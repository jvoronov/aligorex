-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 16, 2014 at 10:39 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wrumwrum`
--

-- --------------------------------------------------------

--
-- Table structure for table `adverts_comments`
--

CREATE TABLE IF NOT EXISTS `adverts_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ads_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `clear_comment` text NOT NULL,
  `original_comment` text NOT NULL,
  `created` datetime NOT NULL,
  `p_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `adverts_comments`
--

INSERT INTO `adverts_comments` (`id`, `ads_id`, `user_id`, `clear_comment`, `original_comment`, `created`, `p_id`) VALUES
(14, 15, 1, '***', 'jawwal1@inbox.lv', '2014-10-01 09:33:19', NULL),
(26, 19, 1, 'vwevew we ew ', 'vwevew we ew ', '2014-10-06 12:35:37', NULL),
(27, 0, 1, 'thrhrthrth', 'thrhrthrth', '2014-10-06 12:46:20', NULL),
(32, 19, 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the ****s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the ****s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop p', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the ****s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the ****s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop p', '2014-10-06 13:00:09', NULL),
(33, 19, 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the ****s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the ****s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop p', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the ****s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the ****s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop p', '2014-10-06 13:10:03', 26),
(34, 20, 1, 'ewfewf', 'ewfewf', '2014-10-08 07:24:50', NULL),
(35, 20, 11, 'rgergreg', 'rgergreg', '2014-10-08 12:54:39', NULL),
(36, 27, 1, 'eregggggg', 'eregggggg', '2014-10-10 14:36:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE IF NOT EXISTS `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `order` int(10) NOT NULL,
  `img` varchar(255) NOT NULL,
  `option_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `status`, `order`, `img`, `option_name`) VALUES
(1, 0, 0, '', 'countries'),
(2, 0, 0, '', 'carrier_countries');

-- --------------------------------------------------------

--
-- Table structure for table `attributes_translate`
--

CREATE TABLE IF NOT EXISTS `attributes_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attributes_translate`
--

INSERT INTO `attributes_translate` (`table_id`, `lang`, `title`) VALUES
(1, 'lv', 'Piegades valstis'),
(1, 'ru', 'Страны доставки'),
(1, 'en', 'Delivery countries'),
(2, 'lv', 'Atrashanas vieta'),
(2, 'ru', 'Местоположение'),
(2, 'en', 'Carrier country');

-- --------------------------------------------------------

--
-- Table structure for table `attributes_values`
--

CREATE TABLE IF NOT EXISTS `attributes_values` (
  `attributes_id` int(10) unsigned NOT NULL,
  `values_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`values_id`),
  KEY `attributes_id` (`attributes_id`),
  KEY `values_id` (`values_id`),
  KEY `attributes_id_2` (`attributes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `attributes_values`
--

INSERT INTO `attributes_values` (`attributes_id`, `values_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 7),
(2, 4),
(2, 5),
(2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `attributes_values_translate`
--

CREATE TABLE IF NOT EXISTS `attributes_values_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attributes_values_translate`
--

INSERT INTO `attributes_values_translate` (`table_id`, `lang`, `title`) VALUES
(1, 'lv', 'Latvija'),
(1, 'ru', 'Латвия'),
(1, 'en', 'Latvia'),
(2, 'lv', 'Lietuva'),
(2, 'ru', 'Литва'),
(2, 'en', 'Lithuania'),
(3, 'lv', 'Igaunija'),
(3, 'ru', 'Эстония'),
(3, 'en', 'Estonia'),
(4, 'lv', 'Latvija'),
(4, 'ru', 'Латвия'),
(4, 'en', 'Latvia'),
(5, 'lv', 'Lietuva'),
(5, 'ru', 'Литва'),
(5, 'en', 'Lithuania'),
(6, 'lv', 'Igaunija'),
(6, 'ru', 'Эстония'),
(6, 'en', 'Estonia'),
(7, 'lv', 'Starptautiskais piegādātais'),
(7, 'ru', 'Международный перевозчик'),
(7, 'en', 'International carrier');

-- --------------------------------------------------------

--
-- Table structure for table `bets`
--

CREATE TABLE IF NOT EXISTS `bets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `carrier_id` int(11) unsigned NOT NULL,
  `ads_id` int(10) unsigned NOT NULL,
  `bet` int(10) unsigned NOT NULL,
  `commnet` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `comfirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `carrier_day` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `carrier_id` (`carrier_id`),
  KEY `ads_id` (`ads_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `bets`
--

INSERT INTO `bets` (`id`, `carrier_id`, `ads_id`, `bet`, `commnet`, `created`, `comfirmed`, `carrier_day`) VALUES
(7, 21, 4, 300, NULL, '2014-09-19 12:17:18', 1, NULL),
(8, 11, 4, 150, NULL, '2014-09-19 12:17:59', 0, NULL),
(9, 11, 6, 700, NULL, '2014-09-19 12:19:14', 0, NULL),
(10, 1, 6, 500, NULL, '2014-09-22 11:57:21', 0, NULL),
(12, 1, 4, 500, NULL, '2014-09-23 07:16:43', 0, NULL),
(24, 1, 15, 444, NULL, '2014-10-01 08:14:54', 0, NULL),
(25, 1, 15, 25, NULL, '2014-10-01 08:15:22', 0, '2014-10-30 11:15:00'),
(26, 11, 19, 500, NULL, '2014-10-03 12:15:59', 0, NULL),
(27, 11, 19, 100, NULL, '2014-10-06 07:08:07', 0, '2014-10-15 10:07:00'),
(28, 11, 19, 122, NULL, '2014-10-06 09:40:10', 0, '2014-10-07 12:40:00'),
(29, 11, 19, 77, NULL, '2014-10-06 09:42:03', 0, NULL),
(30, 11, 19, 900, NULL, '2014-10-06 09:43:08', 0, NULL),
(31, 11, 19, 33, NULL, '2014-10-06 09:58:16', 1, NULL),
(32, 1, 19, 34, NULL, '2014-10-06 13:09:18', 0, NULL),
(33, 1, 20, 500, NULL, '2014-10-08 07:25:02', 0, NULL),
(34, 1, 20, 122, NULL, '2014-10-08 07:25:38', 0, '2014-10-06 12:40:00'),
(35, 1, 23, 500, NULL, '2014-10-10 13:48:07', 0, NULL),
(36, 1, 27, 100, NULL, '2014-10-10 14:36:42', 1, NULL),
(37, 1, 6, 600, NULL, '2014-10-14 11:31:34', 0, NULL),
(38, 21, 6, 300, NULL, '2014-10-14 13:53:06', 0, NULL),
(39, 11, 15, 20, NULL, '2014-10-14 14:24:49', 0, NULL),
(40, 11, 15, 19, NULL, '2014-10-14 14:25:17', 0, NULL),
(41, 11, 15, 18, NULL, '2014-10-14 14:25:23', 0, NULL),
(48, 1, 7, 500, NULL, '2014-10-15 14:03:19', 1, NULL),
(49, 1, 28, 500, NULL, '2014-10-15 14:47:42', 1, NULL),
(50, 1, 8, 500, NULL, '2014-10-15 16:57:27', 0, NULL),
(51, 1, 26, 200, NULL, '2014-10-15 17:00:52', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `carriers`
--

CREATE TABLE IF NOT EXISTS `carriers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) unsigned NOT NULL,
  `is_private` tinyint(1) unsigned NOT NULL,
  `about` text NOT NULL,
  `countries` text NOT NULL,
  `categories` text NOT NULL,
  `carrier_country` text NOT NULL,
  `img` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `carriers`
--

INSERT INTO `carriers` (`id`, `users_id`, `is_private`, `about`, `countries`, `categories`, `carrier_country`, `img`) VALUES
(5, 11, 1, 'wdqwdqwdqwd', '["1","2","3"]', '["1","2","3"]', '"5"', ''),
(6, 1, 1, 'wefewfwef', '["1","2","3","7"]', '["5","4","6","7","1","2","3","8"]', '["4","5","6"]', '');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_id` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  `order` int(4) unsigned NOT NULL,
  `rout_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `img` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `p_id`, `url`, `status`, `deleted`, `order`, `rout_id`, `module_id`, `img`) VALUES
(1, 0, 'Starptautiskie', 1, 0, 5, 5, 3, 'ce03d70e7083d9c1f64185802dacf835.png'),
(2, 0, 'Būvmateriāli', 1, 0, 6, 6, 3, '2987a232a1b061dea6e8b6570760a442.png'),
(3, 0, 'transportlidzekli', 1, 0, 7, 7, 3, 'a577097ca75448d259b41dc28c4adb47.png'),
(4, 0, 'dzivnieki', 1, 0, 2, 8, 3, 'b0c9edf87b4a43468110f22a400e485e.png'),
(5, 0, 'Pasazieri', 1, 0, 1, 13, 3, 'd30cf525f28874f8aee9314d92897b50.png'),
(6, 0, 'mebeles/tehnika', 1, 0, 3, 14, 3, '97774480417c39dcbe82504438a66748.png'),
(7, 0, 'negabarits', 1, 0, 4, 15, 3, '075762fe1e109b7d00c42b610b2a6a9f.png'),
(8, 0, 'citi', 1, 0, 8, 16, 3, '8b0439e6994379c445c16731af12be47.png');

-- --------------------------------------------------------

--
-- Table structure for table `categories_translate`
--

CREATE TABLE IF NOT EXISTS `categories_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories_translate`
--

INSERT INTO `categories_translate` (`table_id`, `lang`, `title`) VALUES
(1, 'lv', 'Starptautiskie'),
(1, 'ru', 'Международные'),
(1, 'en', 'International'),
(2, 'lv', 'Būvmateriāli'),
(2, 'ru', 'Стройматериалы'),
(2, 'en', 'Building materials'),
(3, 'lv', 'Transportlīdzekļi'),
(3, 'ru', 'Транспортные средства'),
(3, 'en', 'Vehicles'),
(4, 'lv', 'Dzīvnieki'),
(4, 'ru', 'Животные'),
(4, 'en', 'Animals'),
(5, 'lv', 'Pasažieri'),
(5, 'ru', 'Пассажиры'),
(5, 'en', 'Passengers'),
(6, 'lv', 'Mēbeles/Tehnika'),
(6, 'ru', 'Мебель/Техника'),
(6, 'en', 'Furniture/Appliances'),
(7, 'lv', 'Negabarīts'),
(7, 'ru', 'Негабарит'),
(7, 'en', 'Outsized'),
(8, 'lv', 'Citi'),
(8, 'ru', 'Другое'),
(8, 'en', 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('679df44d37333bfc6c8b41d4df0c2dee', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36', 1413382743, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:15:"admin@admin.com";s:8:"username";s:13:"administrator";s:5:"email";s:15:"admin@admin.com";s:7:"user_id";s:1:"1";s:14:"old_last_login";s:10:"1413382552";}'),
('9cae778fc9c13cd95b51983b307e6556', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36', 1413435359, 'a:6:{s:9:"user_data";s:0:"";s:8:"identity";s:15:"admin@admin.com";s:8:"username";s:13:"administrator";s:5:"email";s:15:"admin@admin.com";s:7:"user_id";s:1:"1";s:14:"old_last_login";s:10:"1413382833";}');

-- --------------------------------------------------------

--
-- Table structure for table `commissions`
--

CREATE TABLE IF NOT EXISTS `commissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `price` int(10) unsigned NOT NULL,
  `percents` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `commissions`
--

INSERT INTO `commissions` (`id`, `price`, `percents`) VALUES
(2, 307, 3),
(3, 502, 8),
(4, 10000, 15),
(6, 111, 2),
(7, 5000, 12),
(8, 3000, 9);

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE IF NOT EXISTS `complaints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ads_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `description` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `new` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `complaints`
--

INSERT INTO `complaints` (`id`, `ads_id`, `user_id`, `type_id`, `description`, `created`, `new`) VALUES
(15, 7, 1, 1, 'картошка в мешках', '2014-09-29 09:05:41', NULL),
(16, 7, 1, 2, 'ADMIN', '2014-09-29 09:05:45', NULL),
(17, 8, 1, 1, '2 дивана', '2014-09-30 09:17:22', NULL),
(18, 15, 1, 1, 'шпроты в масле', '2014-10-01 07:47:28', 1),
(19, 19, 11, 2, 'carrier company', '2014-10-06 11:00:14', 1),
(20, 19, 1, 2, 'mega carriers company', '2014-10-06 12:35:13', 1),
(21, 19, 1, 1, 'eeeeeeeeee', '2014-10-06 13:46:19', 1),
(22, 20, 1, 1, 'wdqqwdqd', '2014-10-08 10:29:57', 1),
(23, 20, 11, 1, 'wdqqwdqd', '2014-10-08 12:54:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `complaints_types`
--

CREATE TABLE IF NOT EXISTS `complaints_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `complaints_types`
--

INSERT INTO `complaints_types` (`id`, `type`, `description`) VALUES
(1, 'advert', 'Sūdzība uz sludinājumu '),
(2, 'comment', 'Sūdzība uz komentāru'),
(3, 'bet', 'Sūdzība uz likmi');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `key`) VALUES
(1, 'user_registration'),
(2, 'user_forgot_password'),
(3, 'user_new_mail'),
(4, 'user_send_rating_invitation'),
(5, 'user_new_advert'),
(6, 'user_new_bet'),
(7, 'carrier_bet_comfirm'),
(8, 'user_carrier_ask_question'),
(9, 'user_carrier_reject_order'),
(10, 'admin_carrier_reject_order');

-- --------------------------------------------------------

--
-- Table structure for table `emails_translate`
--

CREATE TABLE IF NOT EXISTS `emails_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `description` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `options` text NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emails_translate`
--

INSERT INTO `emails_translate` (`table_id`, `lang`, `description`, `content`, `title`, `options`) VALUES
(1, 'ru', 'письмо активации аккаунта', '<p>Привет )&nbsp;[USER_EMAIL]</p>\r\n\r\n<p>твой пароль [USER_PASSWORD]</p>\r\n\r\n<p>перейдите по ссылке для активации аккаунта [ACTIVATION_LINK]</p>\r\n\r\n<p><strong><span style="background-color:#FFFF00">спасибо</span></strong></p>\r\n', 'активация аккаунта на нашем супер сайте', '<p><b>[USER_EMAIL]</b> - почтовый адрес пользователя</p>\r\n<p><b>[USER_PASSWORD]</b> - пароль пользователя</p>\r\n<p><b>[ACTIVATION_LINK]</b> - ссылка активации</p>'),
(1, 'lv', 'lietotaja aktivizesanas vestule', '<p>sveiks&nbsp;[USER_EMAIL]</p>\r\n\r\n<p>tava parole&nbsp;[USER_PASSWORD]</p>\r\n\r\n<p>jusu aktivacijas links [ACTIVATION_LINK]</p>\r\n', 'velstule', '<p><b>[USER_EMAIL]</b> - lietotaja epasts</p>\r\n<p><b>[USER_PASSWORD]</b> - lietotaja parole</p>\r\n<p><b>[ACTIVATION_LINK]</b> - aktivacijas links</p>'),
(2, 'ru', 'письмо: забыл пароль', '<p>привет&nbsp;<strong>[USER_EMAIL]</strong>,</p>\r\n\r\n<p>забыл пароль? поменяй:&nbsp;<strong>[FORGOT_LINK]</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Смена пароля', '<p><b>[USER_EMAIL]</b> - почтовый адрес пользователя</p>\n<p><b>[FORGOT_LINK]</b> - ссылка на смену пароля</p>'),
(2, 'lv', 'velstule: aizmirsu paroli', '<p>sveiki&nbsp;<strong>[USER_EMAIL]</strong>,</p>\r\n\r\n<p>aizmirsi paroli? nomaini:&nbsp;<strong>[FORGOT_LINK]</strong></p>\r\n', 'paroles maina', '<p><b>[USER_EMAIL]</b> - lietotaja epasts</p>\r\n<p><b>[FORGOT_LINK]</b> - nomainit paroli links</p>'),
(3, 'ru', 'Смена почтового адреса', '<p>вот твой код [CODE]</p>\r\n', 'Смена почты', '<p><b>[CODE]</b> - код для смены почтового адреса</p>'),
(3, 'lv', 'E-pasta mainīšana', '<p>tavs aktivacijas kods [CODE]</p>\r\n', 'epasta mainisana', '<p><b>[CODE]</b> - aktivizācijas kods</p>'),
(1, 'en', 'user activation', '<p>Hi)&nbsp;[USER_EMAIL]</p>\r\n\r\n<p>your password [USER_PASSWORD]</p>\r\n\r\n<p>activate your accaunt [ACTIVATION_LINK]</p>\r\n\r\n<p><strong><span style="background-color:rgb(255, 255, 0)">thanks</span></strong></p>\r\n', 'user activation', '<p><b>[USER_EMAIL]</b> - usermail</p>\r\n<p><b>[USER_PASSWORD]</b> - user password</p>\r\n<p><b>[ACTIVATION_LINK]</b> - activation link</p>'),
(2, 'en', 'change password', '<p>hi&nbsp;<strong>[USER_EMAIL]</strong>,</p>\r\n\r\n<p>Forgot password? change it:&nbsp;<strong>[FORGOT_LINK]</strong></p>\r\n', 'change mail', '<p><b>[USER_EMAIL]</b> - usermail</p>\r\n<p><b>[FORGOT_LINK]</b> - forgot link</p>'),
(3, 'en', 'user change mail', '<p>your code [CODE]</p>\r\n', 'email change', '<p><b>[CODE]</b> - activation code</p>'),
(4, 'ru', 'Приглашение на оценку перевозчика', '<p>Ви подавали объявление [ADS_TITLE]. пожалуйста оцените &nbsp;[CARRIER_COMPANY]. по ссылке &nbsp;[ACCESS_LINK]</p>\n', 'оцените перевозчика', '<p><b>[ADS_TITLE]</b> - название объявления</p>\r\n<p><b>[ACCESS_LINK]</b> - ссылка доступа к объявлению</p>\r\n<p><b>[CARRIER_COMPANY]</b> - название компании перевозчика</p>'),
(4, 'lv', 'Vēstule uz pārvadātāja novērtējumu', '<p>Sveiks, jusu sludinajums [ADS_TITLE]. ludzu novertiet&nbsp;&nbsp;[CARRIER_COMPANY]. jusu links&nbsp;&nbsp;[ACCESS_LINK]</p>\r\n', 'Ludzu novertiet parvadataju', '<p><b>[ADS_TITLE]</b> - Sludinājuma virsraksts</p>\n<p><b>[ACCESS_LINK]</b> - links, pieeja sludinājumam</p>\n<p><b>[CARRIER_COMPANY]</b> - Pārvadātāja uzņēmuma nosaukums</p>'),
(4, 'en', 'rating invitation', '<p>Your advert [ADS_TITLE]. rate please &nbsp;[CARRIER_COMPANY]. your link [ACCESS_LINK]</p>\r\n', 'please rate carrier', '<p><b>[ADS_TITLE]</b> - advert title</p>\n<p><b>[ACCESS_LINK]</b> - advert access link</p>\n<p><b>[CARRIER_COMPANY]</b> - carrier company name</p>'),
(5, 'ru', 'После подачи объявления', '<p>Вы добавили новое объявление [ADS_TITLE], ссылка на управление [ACCESS_LINK]</p>\r\n', 'Вы добавили новое объявление', '<p><b>[ADS_TITLE]</b> - название объявления</p>\r\n<p><b>[ACCESS_LINK]</b> - ссылка доступа к объявлению</p>\r\n'),
(5, 'lv', 'Pēc jauna sludinājuma pievienošanas', '<p>jus pievinojat jauno sludinajumu [ADS_TITLE], jusu parvaldes links [ACCESS_LINK]</p>\r\n', 'jus pievinojat jauno sludinajumu', '<p><b>[ADS_TITLE]</b> - Sludinājuma virsraksts</p>\r\n<p><b>[ACCESS_LINK]</b> - links, pieeja sludinājumam</p>'),
(5, 'en', 'After new advert', '<p>your advert [ADS_TITLE], access link [ACCESS_LINK]</p>\r\n', 'new advert', '<p><b>[ADS_TITLE]</b> - advert title</p>\n<p><b>[ACCESS_LINK]</b> - advert access link</p>'),
(6, 'ru', 'Заказчику при новой ставке', '<p>Ваше объявление [ADS_TITLE]. Перевозчик [CARRIER_COMPANY] предложил [BET] евро за услугу. ссылка на объявление [ACCESS_LINK]</p>\r\n', 'Вам новая ставка', '<p><b>[ADS_TITLE]</b> - название объявления</p>\n<p><b>[ACCESS_LINK]</b> - ссылка доступа к объявлению</p>\n<p><b>[CARRIER_COMPANY]</b> - название компании перевозчика</p>\n<p><b>[BET]</b> - ставка</p>'),
(6, 'lv', 'Pasūtītājām pēc jaunas likmes', '<p>Jusu sludinajums [ADS_TITLE]. Parvedejs [CARRIER_COMPANY] piedavaja jauno likmi [BET]. sludinajuma links [ACCESS_LINK]</p>\r\n', 'Jums jauna likme', '<p><b>[ADS_TITLE]</b> - Sludinājuma virsraksts</p>\r\n<p><b>[ACCESS_LINK]</b> - links, pieeja sludinājumam</p>\r\n<p><b>[CARRIER_COMPANY]</b> - Pārvadātāja uzņēmuma nosaukums</p>\r\n<p><b>[BET]</b> - likme</p>'),
(6, 'en', 'Mail to customer, after new carrier bet', '<p>your advert [ADS_TITLE]. Carrier company [CARRIER_COMPANY] bet [BET] . your link [ACCESS_LINK]</p>\r\n', 'New bet from carrier', '<p><b>[ADS_TITLE]</b> - advert title</p>\n<p><b>[ACCESS_LINK]</b> - advert access link</p>\n<p><b>[CARRIER_COMPANY]</b> - carrier company name</p>\n<p><b>[BET]</b> - carrier bet</p>'),
(7, 'ru', 'Заказчик подтвердил ставку (письмо перевозчику)', '<p>Ваша ставка [BET] на объявление [ADS_TITLE] подтверждена. Данные заказчика можете получить в личном кабинете. ссылка на объявление [ADS_LINK].</p>\r\n', 'Ваша ставка подтверждена', '<p><b>[ADS_TITLE]</b> - название объявления</p>\r\n<p><b>[ADS_LINK]</b> - ссылка на объявлению</p>\r\n<p><b>[BET]</b> - ставка</p>'),
(7, 'lv', 'Pasūtītājs apstiprināja likmi (vēstule pārvadātājām)', '<p>Jusu likme [BET] uz sludinajumu [ADS_TITLE] ir apstiprinata. Varat sanem pasutitaja datus iejot saite ar savu akkaunu. links uz sludinajumu [ADS_LINK].</p>\r\n', 'Jusu likme ir apstiprinata', '<p><b>[ADS_TITLE]</b> - Sludinājuma virsraksts</p>\n<p><b>[ADS_LINK]</b> - Sludinājuma links/p>\n<p><b>[BET]</b> - likme</p>'),
(7, 'en', 'Customer comfirm bet (mail to carrier)', '<p>Your bet [BET] advert title [ADS_TITLE] comfirmed. Customer contact data in your accaunt. advert link [ADS_LINK].</p>\r\n', 'Your bet comfirmed', '<p><b>[ADS_TITLE]</b> - advert title/p>\r\n<p><b>[ADS_LINK]</b> - advert link/p>\r\n<p><b>[BET]</b> - carrier bet</p>'),
(8, 'ru', 'Перевозчик задал вопрос (почта заказчику)', '<p>По объявлению [ADS_TITLE]. Вам задали вопрос [CARRIER_COMPANY] : [QUESTION].ссылка на объявление [ACCESS_LINK]</p>\r\n', 'Новый вопрос от перевозчика', '<p><b>[ADS_TITLE]</b> - название объявления</p>\r\n<p><b>[ACCESS_LINK]</b> -ссылка доступа к объявлению</p>\r\n<p><b>[CARRIER_COMPANY]</b> - название компании перевозчика</p>\r\n<p><b>[QUESTION]</b> - вопрос от перевозчика</p>'),
(8, 'lv', 'Pārvadātājs uzdeva jautājumu (vestule pasūtitājām)', '<p>Jusu sludinajums [ADS_TITLE]. Jums uzdeva jautamumu [CARRIER_COMPANY] : [QUESTION]. links uz sludinajumu [ACCESS_LINK]</p>\r\n', 'Jums jauns jautajums no parvedataja', '<p><b>[ADS_TITLE]</b> - Sludinājuma virsraksts</p>\r\n<p><b>[ACCESS_LINK]</b> -links, pieeja sludinājumam</p>\r\n<p><b>[CARRIER_COMPANY]</b> - Pārvadātāja uzņēmuma nosaukums</p>\r\n<p><b>[QUESTION]</b> - jautājums no pārvādāja</p>'),
(8, 'en', 'Carrier question (mail to customer)', '<p>your advert [ADS_TITLE]. Carrier asks [CARRIER_COMPANY] : [QUESTION] . adverts link [ACCESS_LINK]</p>\r\n', 'You have new question from carrier', '<p><b>[ADS_TITLE]</b> - advert title</p>\r\n<p><b>[ACCESS_LINK]</b> -advert access link</p>\r\n<p><b>[CARRIER_COMPANY]</b> - carrier company name</p>\r\n<p><b>[QUESTION]</b> - carrier question</p>'),
(9, 'ru', 'перевозчик отказался от заказа (письмо заказчику)', '<p>Перевозчик отказался от заказа</p>\r\n\r\n<p>[ADS_TITLE] ссылка:&nbsp;[ADS_LINK]&nbsp;</p>\r\n', 'Перевозчик отказался от заказа', '<p><b>[ADS_LINK]</b> - ссылка на объявление</p>\r\n<p><b>[ADS_TILTE]</b> - название объявления</p>'),
(9, 'lv', 'Pārdevējs atteicās no pasūtījuma (vēstulē pasūtītājam )', '<p>Pārdevējs atteicās no pasūtījuma.</p>\r\n\r\n<p>[ADS_TITLE] links:&nbsp;[ADS_LINK]&nbsp;</p>\r\n', 'Pārdevējs atteicās no pasūtījuma', '<p><b>[ADS_TITLE]</b> - Sludinājuma virsraksts</p>\n<p><b>[ADS_LINK]</b> - Sludinājuma links</p>'),
(9, 'en', 'Carrier reject order (mail to customer)', '<p>Перевозчик отказался от заказа</p>\r\n\r\n<p>[ADS_TITLE] ссылка:&nbsp;[ADS_LINK]&nbsp;</p>\r\n', 'Перевозчик отказался от заказа', '<p><b>[ADS_TITLE]</b> - advert title/p>\n<p><b>[ADS_LINK]</b> - advert link</p>'),
(10, 'ru', 'перевозчик отказался от заказа (письмо администратору)', '<p>[ADS_TITLE]</p>\r\n\r\n<p>[ADS_LINK]</p>\r\n\r\n<p>[CARRIER_COMPANY]</p>\r\n\r\n<p>[CARRIER_DESCRIPTION]</p>\r\n', 'Pārdevējs atteicās no pasūtījuma', '<p><b>[ADS_LINK]</b> - ссылка на объявление</p>\r\n<p><b>[ADS_TILTE]</b> - название объявления</p>\r\n<p><b>[CARRIER_COMPANY]</b> - название компании</p>\r\n<p><b>[CARRIER_DESCRIPTION]</b> - причина</p>'),
(10, 'lv', 'Pārdevējs atteicās no pasūtījuma (vēstulē administratoram)', '<p>[ADS_TITLE]</p>\r\n\r\n<p>[ADS_LINK]</p>\r\n\r\n<p>[CARRIER_COMPANY]</p>\r\n\r\n<p>[CARRIER_DESCRIPTION]</p>\r\n', 'Pārdevējs atteicās no pasūtījuma', '<p><b>[ADS_TITLE]</b> - Sludinājuma virsraksts</p>\r\n<p><b>[ADS_LINK]</b> - Sludinājuma links</p>\r\n<p><b>[CARRIER_COMPANY]</b> - firmas nosaukums</p>\r\n<p><b>[CARRIER_DESCRIPTION]</b> - iemesls</p>\r\n'),
(10, 'en', 'Carrier reject order (mail to administrator)', '<p>[ADS_TITLE]</p>\r\n\r\n<p>[ADS_LINK]</p>\r\n\r\n<p>[CARRIER_COMPANY]</p>\r\n\r\n<p>[CARRIER_DESCRIPTION]</p>\r\n', 'Pārdevējs atteicās no pasūtījuma', '<p><b>[ADS_TITLE]</b> - advert title/p>\r\n<p><b>[ADS_LINK]</b> - advert link</p>\r\n<p><b>[CARRIER_COMPANY]</b> - company name</p>\r\n<p><b>[CARRIER_DESCRIPTION]</b> - description</p>');

-- --------------------------------------------------------

--
-- Table structure for table `favorite_adverts`
--

CREATE TABLE IF NOT EXISTS `favorite_adverts` (
  `ads_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  KEY `ads_id` (`ads_id`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `ads_id_2` (`ads_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `favorite_adverts`
--

INSERT INTO `favorite_adverts` (`ads_id`, `user_id`) VALUES
(6, 1),
(6, 11),
(8, 1),
(9, 1),
(15, 1),
(16, 1),
(17, 1),
(17, 11),
(17, 20),
(18, 1),
(18, 11),
(20, 1),
(20, 20),
(21, 1),
(22, 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrātors'),
(2, 'members', 'Lietotājs '),
(3, 'carriers', 'Parvedejs');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) NOT NULL,
  `description` varchar(20) NOT NULL,
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `lang`, `description`, `default`, `status`) VALUES
(1, 'ru', 'Русский', 0, 1),
(2, 'lv', 'Latviešu', 2, 1),
(6, 'en', 'English', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`) VALUES
(1, 'top_menu'),
(2, 'footer_menu');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `categories` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `title`, `categories`) VALUES
(1, 'pages', 'Lappuses', 1),
(2, 'blog', 'Blogs', 1),
(3, 'ads', 'Sludinājumi ', 1),
(4, 'templates', 'Veidnes', 0),
(5, 'categories', 'Kategorijas', 0);

-- --------------------------------------------------------

--
-- Table structure for table `navigation`
--

CREATE TABLE IF NOT EXISTS `navigation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_id` int(10) unsigned NOT NULL,
  `rout_id` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `menus_id` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `navigation`
--

INSERT INTO `navigation` (`id`, `p_id`, `rout_id`, `status`, `deleted`, `created`, `modified`, `order`, `menus_id`) VALUES
(1, 0, 11, 1, 0, '2014-10-10 08:47:40', '0000-00-00 00:00:00', 2, 1),
(2, 0, 12, 1, 0, '2014-10-10 08:48:10', '0000-00-00 00:00:00', 1, 1),
(3, 0, 17, 1, 0, '2014-10-13 06:12:16', '0000-00-00 00:00:00', 1, 2),
(4, 0, 18, 1, 0, '2014-10-13 06:13:14', '0000-00-00 00:00:00', 3, 2),
(5, 0, 19, 1, 0, '2014-10-13 06:13:57', '0000-00-00 00:00:00', 4, 2),
(6, 0, 20, 1, 0, '2014-10-13 06:14:22', '0000-00-00 00:00:00', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `navigation_translate`
--

CREATE TABLE IF NOT EXISTS `navigation_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  KEY `table_id` (`table_id`,`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `navigation_translate`
--

INSERT INTO `navigation_translate` (`table_id`, `lang`, `title`) VALUES
(1, 'lv', 'Lietotajiem'),
(1, 'ru', 'Пользователю'),
(1, 'en', 'For member'),
(2, 'lv', 'Parvedatajam'),
(2, 'ru', 'Перевозчику'),
(2, 'en', 'For carriers'),
(3, 'lv', 'Par mums'),
(3, 'ru', 'О нас'),
(3, 'en', 'About'),
(4, 'lv', 'Lietošanas noteikumi'),
(4, 'ru', 'Правила пользования порталом'),
(4, 'en', 'Terms of Use'),
(5, 'lv', 'Datu drošība'),
(5, 'ru', 'Безопасность данных'),
(5, 'en', 'Privacy policy'),
(6, 'lv', 'Apmaksa un piegade'),
(6, 'ru', 'Оплата и доставка'),
(6, 'en', 'Payment and delivery');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned DEFAULT NULL,
  `carrier_id` int(11) unsigned NOT NULL,
  `ads_id` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `rejection` tinyint(3) unsigned DEFAULT '0',
  `description` text,
  `bet` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `carrier_id` (`carrier_id`),
  KEY `products_id` (`ads_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `carrier_id`, `ads_id`, `status`, `created`, `rejection`, `description`, `bet`) VALUES
(1, NULL, 21, 4, 1, '2014-09-30 13:41:11', 0, NULL, 0),
(2, NULL, 1, 7, 0, '2014-09-30 13:57:27', 1, 'slomalas mashina', 500),
(7, NULL, 11, 19, 0, '2014-10-06 13:52:43', 0, NULL, 0),
(8, NULL, 1, 7, 0, '2014-10-15 12:08:29', 1, 'egwewgewg', 20),
(9, NULL, 11, 7, 0, '2014-10-15 12:22:57', 1, 'gregregerg', 30),
(10, NULL, 1, 7, 0, '2014-10-15 12:25:52', 1, '', 100),
(11, NULL, 1, 7, 0, '2014-10-15 12:39:38', 1, '', 100),
(12, NULL, 1, 7, 0, '2014-10-15 13:51:05', 1, 'efwefwe', 20),
(13, NULL, 1, 7, 1, '2014-10-15 14:03:23', 0, NULL, 500),
(14, NULL, 1, 28, 1, '2014-10-15 14:47:44', 0, NULL, 500),
(15, NULL, 1, 27, 1, '2014-10-15 14:48:56', 0, NULL, 100);

-- --------------------------------------------------------

--
-- Table structure for table `orders_products`
--

CREATE TABLE IF NOT EXISTS `orders_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(10) unsigned NOT NULL,
  `products_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` double(10,2) NOT NULL,
  `qty` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_id` (`orders_id`,`products_id`),
  KEY `products_id` (`products_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `rout_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  KEY `module_id_2` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `url`, `created`, `modified`, `deleted`, `status`, `rout_id`, `module_id`) VALUES
(1, 'priemushestva-registracii-polzovatelej-lv', '2014-10-10 08:34:54', '0000-00-00 00:00:00', 0, 1, 9, 1),
(2, 'priemushestva-registracii-perevozchikov-lv', '2014-10-10 08:35:36', '0000-00-00 00:00:00', 0, 1, 10, 1),
(3, 'Lietotajiem', '2014-10-10 08:45:42', '0000-00-00 00:00:00', 0, 1, 11, 1),
(4, 'Parvedejam', '2014-10-10 08:46:22', '0000-00-00 00:00:00', 0, 1, 12, 1),
(5, 'par-mums', '2014-10-13 06:02:51', '0000-00-00 00:00:00', 0, 1, 17, 1),
(6, 'lietosanas-noteikumi', '2014-10-13 06:04:25', '0000-00-00 00:00:00', 0, 1, 18, 1),
(7, 'datu-drosiba', '2014-10-13 06:07:26', '0000-00-00 00:00:00', 0, 1, 19, 1),
(8, 'apmaksa-un-piegade', '2014-10-13 06:08:29', '0000-00-00 00:00:00', 0, 1, 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages_categories`
--

CREATE TABLE IF NOT EXISTS `pages_categories` (
  `pages_id` int(10) unsigned NOT NULL,
  `categories_id` int(10) unsigned NOT NULL,
  KEY `pages_id` (`pages_id`,`categories_id`),
  KEY `categories_id` (`categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pages_translate`
--

CREATE TABLE IF NOT EXISTS `pages_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `short_content` text NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages_translate`
--

INSERT INTO `pages_translate` (`table_id`, `lang`, `title`, `content`, `short_content`) VALUES
(1, 'lv', 'Приемущества регистрации пользователей lv', '<p>Приемущества регистрации пользователей lvПриемущества регистрации пользователей lvПриемущества регистрации пользователей lvПриемущества регистрации пользователей lv</p>\r\n', ''),
(1, 'ru', 'Приемущества регистрации пользователей', '<p>Примущества регистрации пользователей&nbsp;Примущества регистрации пользователей</p>\r\n', ''),
(1, 'en', 'Приемущества регистрации пользователей en', '<p>Приемущества регистрации пользователей enПриемущества регистрации пользователей enПриемущества регистрации пользователей enПриемущества регистрации пользователей en</p>\r\n', ''),
(2, 'lv', 'Приемущества регистрации перевозчиков lv', '<p>Приемущества регистрации перевозчиков lvПриемущества регистрации перевозчиков lvПриемущества регистрации перевозчиков lvПриемущества регистрации перевозчиков lv</p>\r\n', ''),
(2, 'ru', 'Приемущества регистрации перевозчиков', '<p>Приемущества регистрации перевозчиковПриемущества регистрации перевозчиковПриемущества регистрации перевозчиковПриемущества регистрации перевозчиковПриемущества регистрации перевозчиковПриемущества регистрации перевозчиков</p>\r\n', ''),
(2, 'en', 'Приемущества регистрации перевозчиков en', '<p>Приемущества регистрации перевозчиков enПриемущества регистрации перевозчиков enПриемущества регистрации перевозчиков enПриемущества регистрации перевозчиков enПриемущества регистрации перевозчиков enПриемущества регистрации перевозчиков enПриемущества регистрации перевозчиков enПриемущества регистрации перевозчиков en</p>\r\n', ''),
(3, 'lv', 'Lietotajiem', '<p>Lietotajiem&nbsp;LietotajiemLietotajiemLietotajiemLietotajiemLietotajiem</p>\r\n', ''),
(3, 'ru', 'Пользователям', '<p>ПользователямПользователямПользователямПользователямПользователямПользователям</p>\r\n', ''),
(3, 'en', 'For customers', '<p>For customersFor customersFor customersFor customersFor customersFor customersFor customersFor customers</p>\r\n', ''),
(4, 'lv', 'Parvedejam', '<p>ParvedejamParvedejamParvedejamParvedejam&nbsp;ParvedejamParvedejamParvedejam&nbsp;Parvedejam</p>\r\n', ''),
(4, 'ru', 'Перевозчику', '<p>Перевозчику&nbsp;Перевозчику&nbsp;Перевозчику&nbsp;Перевозчику&nbsp;Перевозчику&nbsp;Перевозчику&nbsp;</p>\r\n', ''),
(4, 'en', 'For carriers', '<p>For carriersFor carriersFor carriersFor carriersFor carriers</p>\r\n', ''),
(5, 'lv', 'Par mums', '<p>Par mumsPar mumsPar mumsPar mumsPar mumsPar mums</p>\r\n', ''),
(5, 'ru', 'О нас', '<p>О насО насО насО насО насО насО насО нас</p>\r\n', ''),
(5, 'en', 'About', '<p><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">AboutAbout &nbsp;&nbsp;</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">AboutAbout &nbsp;&nbsp;</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">About</span><span style="font-size:13.3333339691162px">About &nbsp;&nbsp;</span></p>\r\n', ''),
(6, 'lv', 'Lietošanas noteikumi', '<p>Lieto&scaron;anas noteikumiLieto&scaron;anas noteikumiLieto&scaron;anas noteikumiLieto&scaron;anas noteikumiLieto&scaron;anas noteikumi</p>\r\n', ''),
(6, 'ru', 'Условия использования сайта', '<p>Условия использования сайтаУсловия использования сайтаУсловия использования сайта</p>\r\n', ''),
(6, 'en', 'Terms of Use', '<p>Terms of UseTerms of UseTerms of Use</p>\r\n', ''),
(7, 'lv', 'Datu drošība', '<p>Datu dro&scaron;ībaDatu dro&scaron;ībaDatu dro&scaron;ībaDatu dro&scaron;ībaDatu dro&scaron;ībaDatu dro&scaron;ībaDatu dro&scaron;ība</p>\r\n', ''),
(7, 'ru', 'Безопасность данных', '<p>Безопасность данныхБезопасность данныхБезопасность данныхБезопасность данных</p>\r\n', ''),
(7, 'en', 'Privacy policy', '<p>Privacy policyPrivacy policyPrivacy policyPrivacy policyPrivacy policyPrivacy policy</p>\r\n', ''),
(8, 'lv', 'Apmaksa un piegade', '<p>Apmaksa un piegadeApmaksa un piegadeApmaksa un piegadeApmaksa un piegadeApmaksa un piegadeApmaksa un piegade</p>\r\n', ''),
(8, 'ru', 'Оплата и доставка', '<p>Оплата и доставкаОплата и доставкаОплата и доставкаОплата и доставкаОплата и доставкаОплата и доставкаОплата и доставка</p>\r\n', ''),
(8, 'en', 'Payment and delivery', '<p>Payment and deliveryPayment and deliveryPayment and deliveryPayment and deliveryPayment and deliveryPayment and deliveryPayment and deliveryPayment and delivery</p>\r\n', '');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `delivery_start` varchar(255) NOT NULL,
  `delivery_end` varchar(255) NOT NULL,
  `category` int(10) unsigned NOT NULL,
  `ads_title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `loader` tinyint(1) unsigned DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `car_day` datetime DEFAULT NULL,
  `distance` varchar(20) DEFAULT NULL,
  `weight` varchar(20) DEFAULT NULL,
  `di_width` varchar(20) DEFAULT NULL,
  `di_length` varchar(20) DEFAULT NULL,
  `di_height` varchar(20) DEFAULT NULL,
  `coordinate_strart` varchar(255) NOT NULL,
  `coordinate_end` varchar(255) NOT NULL,
  `users_id` int(10) unsigned DEFAULT NULL,
  `code` varchar(40) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `term` tinyint(1) unsigned NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `loader2` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `status`, `deleted`, `created`, `modified`, `delivery_start`, `delivery_end`, `category`, `ads_title`, `description`, `loader`, `email`, `phone`, `car_day`, `distance`, `weight`, `di_width`, `di_length`, `di_height`, `coordinate_strart`, `coordinate_end`, `users_id`, `code`, `lang`, `term`, `customer_name`, `loader2`) VALUES
(4, 0, 0, '2014-09-18 10:36:06', '0000-00-00 00:00:00', 'Rīga, Rīgas pilsēta, Latvijas Republika', 'Liepāja, Liepājas pilsēta, Latvijas Republika', 3, 'старю секцию', 'старю секцию за очень дешево', 0, 'carrier@carrier.com', '37128292293', NULL, '217', '100', NULL, '3000', '2000', '["56.9496487","24.10518639999998"]', '["56.5056345","21.011905800000022"]', 11, '373999a7c358a510ed8da675fd2f1ed690b2e412', 'ru', 1, '', 0),
(6, 1, 0, '2014-09-18 12:48:40', '0000-00-00 00:00:00', 'Dzintaru iela, Ventspils, Ventspils pilsēta, Latvijas Republika ', 'Ribadeo, España', 1, 'eeeeeeeeee', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop p', 1, 'test@admin.com', '37128292297', '2014-09-26 15:47:00', '3685', '10', '100', '500', '78', '["57.4035327","21.571667899999966"]', '["43.5336794","-7.040348900000026"]', 11, '0089f996d57dc617e66064db6a450b0c212fd1fc', 'lv', 4, '', 0),
(7, 0, 0, '2014-09-30 12:10:14', '2014-09-30 07:05:15', 'Rugāji, Rugāju novads, Latvijas Republika', 'Minska, Baltkrievija', 1, 'картошка в мешках', 'помогите крестьянам, перевезти картошку', 1, 'carrier@carrier.com', '37128292294', '2014-10-02 14:00:00', '446', '10000', '', '50', '', '["57.004375","27.131727899999987"]', '["53.9","27.566667000000052"]', 11, '0417ae0976e5073d58f841ab602744e4ed1635b6', 'en', 4, '', 0),
(8, 1, 0, '2014-09-30 07:07:22', '0000-00-00 00:00:00', 'Rīga, Rīgas pilsēta, Latvijas Republika', 'Salaspils, Salaspils novads, Latvijas Republika', 2, '2 дивана', 'переезжаю из Риги', 1, 'admin@admin.com', '37127777777', '2014-10-02 10:07:00', '21', NULL, NULL, NULL, NULL, '["56.9496487","24.10518639999998"]', '["56.8564288","24.35227180000004"]', 1, '9bca7b96ba1274319799f173d61e605a4e816751', 'ru', 3, '', 0),
(9, 1, 0, '2014-10-01 06:39:06', '0000-00-00 00:00:00', 'Brīvības iela, Rīga, Rīgas pilsēta, Latvijas Republika', 'Rugāji, Rugāju novads, Latvijas Republika', 2, 'ничего не перевозим', 'просто нечего делать', 0, 'admin@admin.com', '37166666666', '2014-10-17 09:38:00', '219', NULL, NULL, NULL, NULL, '["56.9659619","24.14408400000002"]', '["57.004375","27.131727899999987"]', 1, '0dba89f6987f49056a62c8ef138f0c5b6e2167df', 'lv', 4, '', 0),
(15, 1, 0, '2014-10-01 07:03:17', '0000-00-00 00:00:00', 'Klaipėda, Klaipėdos apskritis, Lietuvos Respublika', 'Tallinn, Tallinna linn, Eesti Vabariik', 3, 'шпроты в масле', 'очень вкусные', 1, 'sprotes@klaipeda.com', '37022222222', '2014-10-31 10:02:00', '614', '10000', NULL, NULL, NULL, '["55.70329479999999","21.14427939999996"]', '["59.43696079999999","24.75357459999998"]', 1, 'f1be0c31e5b74f6e507f61a145436551d9898f49', 'ru', 3, '', 0),
(16, 1, 0, '2014-10-03 10:41:22', '0000-00-00 00:00:00', 'Rīga, Rīgas pilsēta, Latvijas Republika', 'Liepāja, Liepājas pilsēta, Latvijas Republika', 1, 'wqdqwd', 'fewfwef', 1, 'carrier@carrier.com', '+(371)6666666', '2014-10-15 13:37:00', '217', '2', '3', '3', '4', '["56.9496487","24.10518639999998"]', '["56.5056345","21.011905800000022"]', NULL, 'b594c95bad2240c4581e6724a9cb3098494e1ecd', 'ru', 2, '', 1),
(17, 1, 0, '2014-10-03 10:43:12', '0000-00-00 00:00:00', 'Tallinas iela, Rīga, Rīgas pilsēta, Latvijas Republika', 'Pērnavas iela, Rīga, Rīgas pilsēta, Latvijas Republika', 3, 'qwdqwd', '', NULL, 'carrier@carrier.com', '+(371)6666666', NULL, '1', NULL, NULL, NULL, NULL, '["56.9596411","24.14261209999995"]', '["56.9573736","24.152651099999957"]', NULL, 'e43e77ce36c39fba884298846d9bde920ee937b6', 'ru', 1, 'Super admin', NULL),
(18, 1, 0, '2014-10-03 11:17:53', '0000-00-00 00:00:00', 'liepaja wqdq w', 'Jelgava, Jelgavas pilsēta, Latvijas Republika', 3, 'eeeeeeeeeewd qweeeeeeeeeewd qweeeeeeeeeewd qweeeeeeeeeewd qw ', '', NULL, 'carrier@carrier.com', '+(371)6666666', NULL, '181', NULL, NULL, NULL, NULL, '["56.5056345","21.011905800000022"]', '["56.6511091","23.721354099999985"]', NULL, '668a35a87ce37ffef0bceb35d7bb68dc5dd4e5d8', 'ru', 1, 'Super admin', NULL),
(19, 0, 0, '2014-10-03 11:20:17', '0000-00-00 00:00:00', 'Maskavas iela, Rīga, Rīgas pilsēta, Latvijas Republika', 'Valmiera, Valmieras pilsēta, Latvijas Republika', 2, 'eeeeeeeeeewdqweeeeeeeeeewdqweeeeeeeeeewd  qweeeeeeeeeewd qweeeeeeeeeewd qweeeeeeeeeewd qw ', '', NULL, 'carrier@carrier.com', '+(371)6666666', '2014-10-30 14:19:00', '112', NULL, NULL, NULL, NULL, '["56.8997008","24.204148099999998"]', '["57.5384659","25.426361799999995"]', NULL, 'bf044b0deff37e60f46d585df34be3a1d0a41af0', 'ru', 4, 'Super admin', NULL),
(20, 1, 0, '2014-10-07 08:07:02', '0000-00-00 00:00:00', '19 Atmodas bulvāris, Liepāja, Liepājas pilsēta, Latvijas Republika', '19 Atmodas iela, Jelgava, Jelgavas pilsēta, Latvijas Republika', 3, 'wdqqwdqd', '', NULL, 'carrier@carrier.com', '28292297222', NULL, '180', NULL, NULL, NULL, NULL, '["56.5557957","21.0065654"]', '["56.6507896","23.69022689999997"]', NULL, 'd81683a8fd7a3efe54ccc0a864b87b410b21e271', 'lv', 1, 'Super admin', NULL),
(21, 1, 0, '2014-10-10 12:53:43', '0000-00-00 00:00:00', 'qwd', 'qwd', 3, 'qwdqwd', '', NULL, 'test@admin.com', '+(371)6666666', NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'dfdaabee608662adeea80d550388e30901abd881', 'ru', 1, 'qwdqwd', NULL),
(22, 1, 0, '2014-10-10 13:22:15', '0000-00-00 00:00:00', 'Rīga, Rīgas pilsēta, Latvijas Republika', 'Lielais prospekts, Ventspils, Ventspils pilsēta, Latvijas Republika', 5, 'sssssssss', '', NULL, 'user2@user.com', '37128292293', NULL, NULL, NULL, '30', NULL, NULL, '', '', NULL, '04a75f28e15cb34364f1ddfc531875b3a3d9e4ec', 'ru', 2, 'Super admin', 1),
(23, 1, 0, '2014-10-10 13:31:46', '0000-00-00 00:00:00', 'Rīgas iela, Ventspils, Ventspils pilsēta, Latvijas Republika', 'Liepāja, Liepājas pilsēta, Latvijas Republika', 8, 'sekciju', 'eeeeeeeeeee adssssssssss  ssssssssss', NULL, 'jawwal1@inbox.lv', '28292297222', '2014-10-29 16:31:00', '119', '550', NULL, NULL, NULL, '["57.39349050000001","21.55522480000002"]', '["56.5056345","21.011905800000022"]', NULL, '230ff77f1e98c153b94d56056068d82fa268a9a3', 'ru', 4, 'qwdqwd', NULL),
(24, 1, 0, '2014-10-10 13:34:00', '0000-00-00 00:00:00', 'Rīgas pilsēta, Latvijas Republika', 'Liepāja, Liepājas pilsēta, Latvijas Republika', 4, 'stul', '', NULL, 'carrier@carrier.com', '28292297222', NULL, '219', NULL, NULL, NULL, NULL, '["56.9690944","24.101013299999977"]', '["56.5056345","21.011905800000022"]', NULL, 'ddd8e717d95085303207e710c762fd0234826469', 'ru', 1, 'qwdqwd', NULL),
(25, 1, 0, '2014-10-10 13:40:01', '0000-00-00 00:00:00', 'dwq', 'qwdqwd', 7, 'eeeeeeeeee', '', 1, 'carrier@carrier.com', '+(371)6666666', '2014-10-15 16:39:00', NULL, NULL, NULL, NULL, NULL, '["43.4375556","-79.71458569999999"]', '', NULL, '7c250d0cc849c28bbbb8898cfb20889e483dd0e2', 'ru', 3, 'Super admin', 1),
(26, 1, 0, '2014-10-10 13:45:13', '0000-00-00 00:00:00', 'Valmiera, Valmieras pilsēta, Latvijas Republika', 'Sigulda, Siguldas novads, Latvijas Republika', 6, 'televizor', '', NULL, 'admin@admin.com', '37128292294', NULL, '63', '20', NULL, NULL, NULL, '["57.5384659","25.426361799999995"]', '["57.1495626","24.860332500000027"]', 1, '9dcfe3425655779994ef45add75712600364d782', 'ru', 1, 'vladimir', NULL),
(27, 0, 0, '2014-10-10 14:20:02', '0000-00-00 00:00:00', 'Limbaži, Limbažu novads, Latvijas Republika', 'Daugavpils, Daugavpils pilsēta, Latvijas Republika', 4, 'черепаху', '', NULL, 'admin@admin.com', '+(371)6666666', '2014-10-20 20:00:00', '267', '1', NULL, NULL, NULL, '["57.5129829","24.72199599999999"]', '["55.874736","26.536178999999947"]', 1, '1e14777110a18b4dfe18ce6ef8239634e08d6a18', 'lv', 4, 'Super admin', NULL),
(28, 0, 0, '2014-10-13 13:35:12', '0000-00-00 00:00:00', 'Rīga, Rīgas pilsēta, Latvijas Republika', 'Lietuva', 5, 'wqdwqd', '', NULL, 'admin@admin.com', '+(371)6666666', NULL, '233', NULL, NULL, NULL, NULL, '["56.9496487","24.10518639999998"]', '["55.169438","23.88127499999996"]', 1, '0d929c13154d401c68462035259c3cfa84447f37', 'lv', 1, 'wwwww', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products_attributes_values`
--

CREATE TABLE IF NOT EXISTS `products_attributes_values` (
  `products_id` int(10) unsigned NOT NULL,
  `attributes_id` int(10) unsigned NOT NULL,
  `values_id` int(10) unsigned NOT NULL,
  KEY `products_id` (`products_id`),
  KEY `attributes_id` (`attributes_id`),
  KEY `values_id` (`values_id`),
  KEY `products_id_2` (`products_id`,`attributes_id`,`values_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products_img`
--

CREATE TABLE IF NOT EXISTS `products_img` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `img` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `order` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `products_img`
--

INSERT INTO `products_img` (`id`, `product_id`, `img`, `title`, `thumbnail`, `order`) VALUES
(1, 7, '9f02f627f38f6becaf4a3b1453babf13.png', NULL, NULL, NULL),
(2, 7, '3da01ac58bfaaa600ca7c43d2c586f13.png', NULL, NULL, NULL),
(3, 19, '3e350ea09eda3467a02f907643e122a6.jpg', NULL, NULL, NULL),
(4, 19, 'ecb910e9a2ecaaec8076d40c7b07cd10.jpg', NULL, NULL, NULL),
(5, 19, 'ecb910e9a2ecaaec8076d40c7b07cd10.jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products_translate`
--

CREATE TABLE IF NOT EXISTS `products_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `product_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  KEY `product_id` (`product_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `raitings`
--

CREATE TABLE IF NOT EXISTS `raitings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `raitings`
--

INSERT INTO `raitings` (`id`, `status`) VALUES
(55, 1),
(56, 1),
(57, 1),
(58, 1);

-- --------------------------------------------------------

--
-- Table structure for table `raitings_carriers`
--

CREATE TABLE IF NOT EXISTS `raitings_carriers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `carrier_id` int(11) unsigned NOT NULL,
  `raiting_id` int(10) unsigned NOT NULL,
  `ads_id` int(10) unsigned NOT NULL,
  `value` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `raitings_carriers`
--

INSERT INTO `raitings_carriers` (`id`, `carrier_id`, `raiting_id`, `ads_id`, `value`) VALUES
(19, 1, 55, 7, 3),
(20, 1, 56, 7, 4),
(21, 1, 55, 28, 5),
(22, 1, 56, 28, 0),
(23, 1, 57, 28, 3),
(24, 1, 55, 27, 5),
(25, 1, 56, 27, 3),
(26, 1, 57, 27, 3);

-- --------------------------------------------------------

--
-- Table structure for table `raitings_comments`
--

CREATE TABLE IF NOT EXISTS `raitings_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ads_id` int(10) unsigned NOT NULL,
  `carrier_id` int(11) unsigned NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `raitings_translate`
--

CREATE TABLE IF NOT EXISTS `raitings_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `raitings_translate`
--

INSERT INTO `raitings_translate` (`table_id`, `lang`, `title`, `description`) VALUES
(55, 'lv', 'precizitāte', 'precizitāte precizitāte precizitāte'),
(55, 'ru', 'аккуратность', 'аккуратность аккуратность аккуратность'),
(55, 'en', 'accuracy', 'accuracyaccuracyaccuracy accuracy'),
(56, 'lv', 'Punktualitate', 'cik punktuals ir parvedejs'),
(56, 'ru', 'Пунктуальность', 'насколько пунктуален перевозчик'),
(56, 'en', 'punctuality', 'wqd wqdqwd qw d q'),
(57, 'lv', 'test rating', 'test rating'),
(57, 'ru', 'test rating', 'test rating'),
(57, 'en', 'test rating', 'test rating'),
(58, 'lv', 'Tests 2', 'averageaverageaverageaverage'),
(58, 'ru', 'Tests 2', 'Tests 2Tests 2Tests 2'),
(58, 'en', 'Tests 2', 'Tests 2Tests 2Tests 2Tests 2');

-- --------------------------------------------------------

--
-- Table structure for table `routs`
--

CREATE TABLE IF NOT EXISTS `routs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_id` int(10) unsigned NOT NULL,
  `rout` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `routs`
--

INSERT INTO `routs` (`id`, `p_id`, `rout`, `title`) VALUES
(1, 0, 'pages/index', 'Страницы (главная)'),
(2, 0, 'blog/index', 'Блог (главная)'),
(3, 0, 'template', 'Все Шаблоны'),
(4, 0, 'categories', 'Категории'),
(5, 4, 'Starptautiskie', 'Starptautiskie (Sludinājumi )'),
(6, 4, 'Būvmateriāli', 'Būvmateriāli (Sludinājumi )'),
(7, 4, 'transportlidzekli', 'Transportlīdzekļi (Sludinājumi )'),
(8, 4, 'dzivnieki', 'Dzīvnieki (Sludinājumi )'),
(9, 1, 'pages/show/priemushestva-registracii-polzovatelej-lv', 'Приемущества регистрации пользователей lv'),
(10, 1, 'pages/show/priemushestva-registracii-perevozchikov-lv', 'Приемущества регистрации перевозчиков lv'),
(11, 1, 'pages/show/Lietotajiem', 'Lietotajiem'),
(12, 1, 'pages/show/Parvedejam', 'Parvedejam'),
(13, 4, 'Pasazieri', 'Pasažieri (Sludinājumi )'),
(14, 4, 'mebeles/tehnika', 'Mēbeles/Tehnika (Sludinājumi )'),
(15, 4, 'negabarits', 'Negabarīts (Sludinājumi )'),
(16, 4, 'citi', 'Citi (Sludinājumi )'),
(17, 1, 'pages/show/par-mums', 'Par mums'),
(18, 1, 'pages/show/lietosanas-noteikumi', 'Lietošanas noteikumi'),
(19, 1, 'pages/show/datu-drosiba', 'Datu drošība'),
(20, 1, 'pages/show/apmaksa-un-piegade', 'Apmaksa un piegade');

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE IF NOT EXISTS `seo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `modules_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `modules_id`, `item_id`) VALUES
(1, 5, 1),
(2, 5, 2),
(3, 5, 3),
(4, 5, 4),
(5, 1, 1),
(6, 1, 2),
(7, 1, 3),
(8, 1, 4),
(9, 5, 5),
(10, 5, 6),
(11, 5, 7),
(12, 5, 8),
(13, 1, 5),
(14, 1, 6),
(15, 1, 7),
(16, 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `seo_translate`
--

CREATE TABLE IF NOT EXISTS `seo_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` text NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seo_translate`
--

INSERT INTO `seo_translate` (`table_id`, `lang`, `title`, `description`, `keywords`) VALUES
(1, 'lv', '', '', ''),
(1, 'ru', '', '', ''),
(1, 'en', '', '', ''),
(2, 'lv', '', '', ''),
(2, 'ru', '', '', ''),
(2, 'en', '', '', ''),
(3, 'lv', '', '', ''),
(3, 'ru', '', '', ''),
(3, 'en', '', '', ''),
(4, 'lv', '', '', ''),
(4, 'ru', '', '', ''),
(4, 'en', '', '', ''),
(5, 'lv', '', '', ''),
(5, 'ru', '', '', ''),
(5, 'en', '', '', ''),
(6, 'lv', '', '', ''),
(6, 'ru', '', '', ''),
(6, 'en', '', '', ''),
(7, 'lv', '', '', ''),
(7, 'ru', '', '', ''),
(7, 'en', '', '', ''),
(8, 'lv', '', '', ''),
(8, 'ru', '', '', ''),
(8, 'en', '', '', ''),
(9, 'lv', '', '', ''),
(9, 'ru', '', '', ''),
(9, 'en', '', '', ''),
(10, 'lv', '', '', ''),
(10, 'ru', '', '', ''),
(10, 'en', '', '', ''),
(11, 'lv', '', '', ''),
(11, 'ru', '', '', ''),
(11, 'en', '', '', ''),
(12, 'lv', '', '', ''),
(12, 'ru', '', '', ''),
(12, 'en', '', '', ''),
(13, 'lv', '', '', ''),
(13, 'ru', '', '', ''),
(13, 'en', '', '', ''),
(14, 'lv', '', '', ''),
(14, 'ru', '', '', ''),
(14, 'en', '', '', ''),
(15, 'lv', '', '', ''),
(15, 'ru', '', '', ''),
(15, 'en', '', '', ''),
(16, 'lv', '', '', ''),
(16, 'ru', '', '', ''),
(16, 'en', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `autoload` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `autoload`) VALUES
(1, 'site_main', 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings_translate`
--

CREATE TABLE IF NOT EXISTS `settings_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `value` text NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings_translate`
--

INSERT INTO `settings_translate` (`table_id`, `lang`, `value`) VALUES
(1, 'ru', '{"site_title":"WrumWrum.com","site_email":"info@wrumwrum.com","site_phone":"555555","site_adress":"\\u0415\\u043b\\u0433\\u0430\\u0432\\u0430","site_description":"\\u041d\\u0430\\u0448 \\u0441\\u0430\\u0439\\u0442 ","site_keywords":"\\u041d\\u0430\\u0448 \\u0441\\u0430\\u0439\\u0442,\\u041d\\u0430\\u0448 \\u0441\\u0430\\u0439\\u0442 ,\\u041d\\u0430\\u0448 \\u0441\\u0430\\u0439\\u0442 ,\\u041d\\u0430\\u0448 \\u0441\\u0430\\u0439\\u0442 "}'),
(1, 'lv', '{"site_title":"WrumWrum.com","site_email":"info@wrumwrum.com","site_phone":"555555","site_adress":"Jelgava","site_description":"Musu saite","site_keywords":"Musu saite,Musu saite, Musu saite, Musu saite"}'),
(1, 'en', '{"site_title":"WrumWrum.com","site_email":"info@wrumwrum.com","site_phone":"555555","site_adress":"Jelgava","site_description":"Musu saite","site_keywords":"Musu saite,Musu saite, Musu saite, Musu saite"}');

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `rout_id` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `translates`
--

CREATE TABLE IF NOT EXISTS `translates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1206 ;

--
-- Dumping data for table `translates`
--

INSERT INTO `translates` (`id`, `key`) VALUES
(1198, 'additional_info'),
(929, 'add_new_ads_title'),
(930, 'add_new_answer_title'),
(931, 'add_new_time'),
(942, 'add_new_what'),
(932, 'add_new_where'),
(995, 'ads_add_bet_btn'),
(1002, 'ads_add_commnet'),
(1006, 'ads_add_commnet_btn'),
(1003, 'ads_add_commnet_title'),
(1005, 'ads_add_commnet_title_place'),
(1024, 'ads_ads_report'),
(1007, 'ads_all_bets'),
(1045, 'ads_all_filter_btn'),
(1043, 'ads_all_filter_cat'),
(1044, 'ads_all_filter_direction'),
(1042, 'ads_all_title'),
(1012, 'ads_bets_bet'),
(1010, 'ads_bets_carrier_day'),
(1013, 'ads_bets_comfirm_btn'),
(1014, 'ads_bets_comfirm_carrier'),
(1011, 'ads_bets_comfirm_title'),
(1008, 'ads_bets_company'),
(1009, 'ads_bets_created'),
(996, 'ads_bet_no_logged'),
(991, 'ads_carrier_add_bet'),
(992, 'ads_carrier_add_day'),
(990, 'ads_carrier_bet_place'),
(989, 'ads_carrier_bet_title'),
(993, 'ads_carrier_profit'),
(1000, 'ads_carrier_selected'),
(959, 'ads_choose_data'),
(948, 'ads_cm'),
(1016, 'ads_comments_title'),
(1022, 'ads_comment_anwer_btn'),
(1020, 'ads_comment_customer_anwer'),
(1018, 'ads_comment_delete'),
(1001, 'ads_comment_no_logged'),
(1017, 'ads_comment_report'),
(1019, 'ads_comment_report_customer'),
(1021, 'ads_comment_report_customer_place'),
(964, 'ads_contacts'),
(965, 'ads_customer_name'),
(966, 'ads_customer_name_place'),
(1025, 'ads_delete_btn'),
(962, 'ads_description'),
(963, 'ads_description_place'),
(951, 'ads_di_height'),
(950, 'ads_di_length'),
(949, 'ads_di_width'),
(947, 'ads_kg'),
(952, 'ads_loader_services'),
(953, 'ads_loader_services2'),
(999, 'ads_login'),
(969, 'ads_mail'),
(970, 'ads_mail_place'),
(960, 'ads_more_data'),
(1015, 'ads_no_bets'),
(1023, 'ads_no_comments'),
(1004, 'ads_no_contact_data'),
(997, 'ads_or'),
(967, 'ads_phone'),
(968, 'ads_phone_place'),
(961, 'ads_photos_title'),
(1029, 'ads_rate_btn'),
(1027, 'ads_rate_comment'),
(1028, 'ads_rate_comment_place'),
(1026, 'ads_rate_title'),
(998, 'ads_register'),
(972, 'ads_rules_link'),
(971, 'ads_rules_title'),
(979, 'ads_show_added'),
(976, 'ads_show_from'),
(983, 'ads_show_loader'),
(986, 'ads_show_loader2'),
(988, 'ads_show_loader2_no'),
(987, 'ads_show_loader2_yes'),
(985, 'ads_show_loader_no'),
(984, 'ads_show_loader_yes'),
(980, 'ads_show_status'),
(982, 'ads_show_status_close'),
(981, 'ads_show_status_open'),
(978, 'ads_show_what'),
(975, 'ads_show_where'),
(977, 'ads_show_where_del'),
(994, 'ads_site_profit'),
(954, 'ads_term'),
(956, 'ads_term_fast'),
(955, 'ads_term_no'),
(958, 'ads_term_till'),
(957, 'ads_term_yes'),
(944, 'ads_title'),
(945, 'ads_title_place'),
(946, 'ads_weight'),
(1181, 'backend_assign'),
(1156, 'backend_auth_groups'),
(1155, 'backend_auth_mail'),
(1151, 'backend_auth_new_group'),
(1150, 'backend_auth_new_user'),
(1161, 'backend_auth_user_active'),
(1162, 'backend_auth_user_inactive'),
(1154, 'backend_auth_user_manag'),
(1182, 'backend_btn_create'),
(1166, 'backend_btn_deactivate'),
(1179, 'backend_btn_save'),
(1152, 'backend_btn_search'),
(1174, 'backend_created'),
(1143, 'backend_dash_active'),
(1145, 'backend_dash_ads_stats'),
(1147, 'backend_dash_closed'),
(1148, 'backend_dash_deleted'),
(1144, 'backend_dash_inactive'),
(1146, 'backend_dash_open'),
(1140, 'backend_dash_stats'),
(1142, 'backend_dash_total'),
(1141, 'backend_dash_users_stats'),
(1173, 'backend_display'),
(1177, 'backend_display_drop'),
(1178, 'backend_display_drop_no'),
(1176, 'backend_display_or_not'),
(1175, 'backend_editing'),
(1125, 'backend_menu_ads'),
(1130, 'backend_menu_blog'),
(1127, 'backend_menu_categories'),
(1134, 'backend_menu_commission'),
(1135, 'backend_menu_complaints'),
(1123, 'backend_menu_dash'),
(1131, 'backend_menu_emails'),
(1133, 'backend_menu_filters'),
(1138, 'backend_menu_languages'),
(1126, 'backend_menu_navigation'),
(1128, 'backend_menu_pages'),
(1132, 'backend_menu_ratings'),
(1137, 'backend_menu_settings'),
(1139, 'backend_menu_site_settings'),
(1129, 'backend_menu_templates'),
(1136, 'backend_menu_translations'),
(1124, 'backend_menu_users'),
(1163, 'backend_msg_deactivate_user'),
(1159, 'backend_msg_delete_record'),
(1160, 'backend_msg_nothing_found'),
(1170, 'backend_nav_add_menu_item'),
(1168, 'backend_nav_add_nav_btn'),
(1180, 'backend_nav_menu_item'),
(1167, 'backend_nav_title'),
(1169, 'backend_nav_unique_name'),
(1165, 'backend_no'),
(1158, 'backend_options'),
(1171, 'backend_order'),
(1149, 'backend_overview'),
(1153, 'backend_search_place'),
(1157, 'backend_status'),
(1172, 'backend_title'),
(1164, 'backend_yes'),
(973, 'btn_ads_add'),
(1084, 'btn_change'),
(940, 'btn_close'),
(1085, 'btn_comfirm'),
(1082, 'btn_save'),
(1083, 'btn_update'),
(1196, 'carrier_company_name'),
(1195, 'carrier_info'),
(1201, 'carrier_rat'),
(1199, 'carrier_ratings'),
(1070, 'change_new_mail_title'),
(1074, 'code'),
(1073, 'code_comfirm'),
(1075, 'code_pace'),
(1188, 'comfirmed_date'),
(1078, 'customer_accaunt'),
(1080, 'customer_ads'),
(1077, 'customer_dashboard'),
(939, 'customer_direction'),
(1079, 'customer_logout'),
(1197, 'delivery_categories'),
(937, 'delivery_distance'),
(935, 'delivery_end'),
(936, 'delivery_end_place'),
(938, 'delivery_km'),
(933, 'delivery_start'),
(934, 'delivery_start_place'),
(1071, 'enter_new_mail'),
(1072, 'enter_new_mail_place'),
(1183, 'filter_old_label'),
(1184, 'first_palce'),
(1106, 'forgot_password'),
(1108, 'forgot_passw_title'),
(1122, 'index_direction'),
(1120, 'index_invite'),
(1121, 'index_invite_btn'),
(1119, 'index_next_btn'),
(974, 'msg_ads_added'),
(1038, 'msg_ads_deleted'),
(1031, 'msg_bet_added'),
(1032, 'msg_carrier_comfirmed'),
(1035, 'msg_comment_added'),
(1039, 'msg_comment_deleted'),
(1036, 'msg_complaint_added'),
(1037, 'msg_complaint_warning'),
(1033, 'msg_cwrong_access_code'),
(1076, 'msg_mail_failed'),
(1111, 'msg_mail_not_send'),
(1068, 'msg_mail_updated_success'),
(1067, 'msg_new_mail_updated'),
(1034, 'msg_rate_success'),
(1112, 'msg_register_success'),
(1187, 'msg_reject_order'),
(1081, 'msg_succes_updated'),
(1030, 'msg_wrong_validation'),
(1102, 'no'),
(1190, 'orders_done'),
(1191, 'orders_new'),
(1186, 'other_palce'),
(1040, 'pagination_first_link'),
(1041, 'pagination_last_link'),
(1203, 'rating_avg_title'),
(1204, 'rating_avg_tooltip'),
(1205, 'rating_no_values'),
(1200, 'rating_title'),
(1202, 'rating_values_cnt'),
(1113, 'register_carrier'),
(1114, 'register_member'),
(1118, 'register_name'),
(1115, 'register_psw'),
(1116, 'register_repsw'),
(1117, 'register_repsw_place'),
(1192, 'reject_issue_title'),
(1193, 'reject_label'),
(1194, 'reject_submit_btn'),
(1189, 'reject_title'),
(1105, 'remember_label'),
(1110, 'reset_psw_change_btn'),
(1185, 'second_palce'),
(943, 'select_category'),
(1109, 'send_btn'),
(941, 'show_map'),
(1103, 'site_login_email'),
(1104, 'site_login_password'),
(1098, 'table_ads_status_title'),
(1096, 'table_ads_title'),
(1099, 'table_bets_created_title'),
(1097, 'table_comfirmed_title'),
(1100, 'table_view_ads_title'),
(1094, 'top_menu_carrier'),
(1093, 'top_menu_member'),
(1092, 'top_menu_register'),
(1090, 'user_adverts_href'),
(1087, 'user_bets_href'),
(1065, 'user_carrier_country'),
(1051, 'user_contact_info_title'),
(1054, 'user_contact_phone'),
(1055, 'user_contact_phone_place'),
(1052, 'user_contact_username'),
(1053, 'user_contact_username_place'),
(1091, 'user_favorites_href'),
(1066, 'user_menu_carrier_change_mail'),
(1064, 'user_menu_carrier_settings'),
(1047, 'user_new_psw'),
(1048, 'user_new_psw_place'),
(1088, 'user_orders_href'),
(1060, 'user_profile_about'),
(1061, 'user_profile_about_place'),
(1062, 'user_profile_categories_title'),
(1063, 'user_profile_choose_category'),
(1086, 'user_profile_href'),
(1095, 'user_profile_img'),
(1056, 'user_profile_info_title'),
(1057, 'user_profile_is_private_title'),
(1059, 'user_profile_not_private'),
(1058, 'user_profile_private'),
(1046, 'user_psw_change_title'),
(1089, 'user_rating_href'),
(1049, 'user_renew_psw'),
(1050, 'user_renew_psw_place'),
(1107, 'website_enter_title'),
(1069, 'wrong_mail_code'),
(1101, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `translates_translate`
--

CREATE TABLE IF NOT EXISTS `translates_translate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3614 ;

--
-- Dumping data for table `translates_translate`
--

INSERT INTO `translates_translate` (`id`, `table_id`, `lang`, `value`) VALUES
(2783, 929, 'en', 'Добавить новое объявление'),
(2784, 929, 'lv', 'Iesnegt jaunu pieprasījumu'),
(2785, 929, 'ru', 'Добавить новое объявление'),
(2786, 930, 'en', 'Пожалуйста ответьте на следующие вопросы, это займет не больше'),
(2787, 930, 'lv', 'Lūdzu atbildiet uz sekojošiem jautājumiem, tas aizņems nevairak par <span class="text-wrum">1</span> min.'),
(2788, 930, 'ru', 'Пожалуйста ответьте на следующие вопросы, это займет не больше'),
(2789, 931, 'en', 'минуты.'),
(2790, 931, 'lv', 'min.'),
(2791, 931, 'ru', 'минуты.'),
(2792, 932, 'en', 'Куда?'),
(2793, 932, 'lv', '1. Virziens'),
(2794, 932, 'ru', 'Куда?'),
(2795, 933, 'en', 'Точка отправления'),
(2796, 933, 'lv', 'Sākumpunkts'),
(2797, 933, 'ru', 'Точка отправления'),
(2798, 934, 'en', 'Введите точку отправления'),
(2799, 934, 'lv', 'Ievadiet sākumpunkts'),
(2800, 934, 'ru', 'Введите точку отправления'),
(2801, 935, 'en', 'Точка назначения'),
(2802, 935, 'lv', 'Galamērķis'),
(2803, 935, 'ru', 'Точка назначения'),
(2804, 936, 'en', 'Введите точку назначения'),
(2805, 936, 'lv', 'Ievadiet galamērķis'),
(2806, 936, 'ru', 'Введите точку назначения'),
(2807, 937, 'en', 'Примерное расстояние:'),
(2808, 937, 'lv', 'attālums:'),
(2809, 937, 'ru', 'Примерное расстояние:'),
(2810, 938, 'en', 'км.'),
(2811, 938, 'lv', 'km.'),
(2812, 938, 'ru', 'км.'),
(2813, 939, 'en', 'Ваш выбранный маршрут'),
(2814, 939, 'lv', 'Jūsu maršruts'),
(2815, 939, 'ru', 'Ваш выбранный маршрут'),
(2816, 940, 'en', 'Закрыть'),
(2817, 940, 'lv', 'Aizvērt'),
(2818, 940, 'ru', 'Закрыть'),
(2819, 941, 'en', 'Посмотреть на карте'),
(2820, 941, 'lv', 'Skatīties uz karti'),
(2821, 941, 'ru', 'Посмотреть на карте'),
(2822, 942, 'en', 'Что?'),
(2823, 942, 'lv', '2. Apraksts'),
(2824, 942, 'ru', 'Что?'),
(2825, 943, 'en', 'Выберите категорию'),
(2826, 943, 'lv', 'Izvēlieties kategoriju'),
(2827, 943, 'ru', 'Выберите категорию'),
(2828, 944, 'en', 'Что перевозим?'),
(2829, 944, 'lv', 'Ko pārvdām?'),
(2830, 944, 'ru', 'Что перевозим?'),
(2831, 945, 'en', 'напр. секцию'),
(2832, 945, 'lv', 'piemierām ledusskapis'),
(2833, 945, 'ru', 'напр. секцию'),
(2834, 946, 'en', 'Вес'),
(2835, 946, 'lv', 'Svars'),
(2836, 946, 'ru', 'Вес'),
(2837, 947, 'en', '(кг)'),
(2838, 947, 'lv', '(kg)'),
(2839, 947, 'ru', '(кг)'),
(2840, 948, 'en', '(см)'),
(2841, 948, 'lv', '(cm)'),
(2842, 948, 'ru', '(см)'),
(2843, 949, 'en', 'Ширина'),
(2844, 949, 'lv', 'Platums'),
(2845, 949, 'ru', 'Ширина'),
(2846, 950, 'en', 'Длина'),
(2847, 950, 'lv', 'Garums '),
(2848, 950, 'ru', 'Длина'),
(2849, 951, 'en', 'Высота'),
(2850, 951, 'lv', 'Augstums'),
(2851, 951, 'ru', 'Высота'),
(2852, 952, 'en', 'Услуги грузчика?'),
(2853, 952, 'lv', 'Nepieciešams krāvējs'),
(2854, 952, 'ru', 'Услуги грузчика?'),
(2855, 953, 'en', 'Выгрузка?'),
(2856, 953, 'lv', 'Nepieciešamā izkraušana'),
(2857, 953, 'ru', 'Выгрузка?'),
(2858, 954, 'en', 'Срок'),
(2859, 954, 'lv', 'Termiņš'),
(2860, 954, 'ru', 'Срок'),
(2861, 955, 'en', 'Не определен'),
(2862, 955, 'lv', 'Nenoteikts'),
(2863, 955, 'ru', 'Не определен'),
(2864, 956, 'en', 'Как можно быстрее'),
(2865, 956, 'lv', 'Atrāk'),
(2866, 956, 'ru', 'Как можно быстрее'),
(2867, 957, 'en', 'Определен'),
(2868, 957, 'lv', 'Noteikts'),
(2869, 957, 'ru', 'Определен'),
(2870, 958, 'en', 'Нужно до'),
(2871, 958, 'lv', 'Līdz'),
(2872, 958, 'ru', 'Нужно до'),
(2873, 959, 'en', 'Выберите дату'),
(2874, 959, 'lv', 'Datums'),
(2875, 959, 'ru', 'Выберите дату'),
(2876, 960, 'en', 'Подробнее'),
(2877, 960, 'lv', '3. Sīkāk'),
(2878, 960, 'ru', 'Подробнее'),
(2879, 961, 'en', 'Фотографии - формат: (jpeg|png), максимальный размер: 5мб'),
(2880, 961, 'lv', 'Attēli - formats: (jpeg|png), max izmērs: 5mb'),
(2881, 961, 'ru', 'Фотографии - формат: (jpeg|png), максимальный размер: 5мб'),
(2882, 962, 'en', 'Описание'),
(2883, 962, 'lv', 'Apraksts'),
(2884, 962, 'ru', 'Описание'),
(2885, 963, 'en', 'введите описание (максимум 500 символов)'),
(2886, 963, 'lv', '(max. 500 simboli)'),
(2887, 963, 'ru', 'введите описание (максимум 500 символов)'),
(2888, 964, 'en', 'Контакты'),
(2889, 964, 'lv', 'Jūsu kontakti'),
(2890, 964, 'ru', 'Контакты'),
(2891, 965, 'en', 'Имя, Фамилия'),
(2892, 965, 'lv', 'Vārds, uzvārds'),
(2893, 965, 'ru', 'Имя, Фамилия'),
(2894, 966, 'en', 'введите имя, фамилию'),
(2895, 966, 'lv', 'Ievadiet vārdu un uzvārdu'),
(2896, 966, 'ru', 'введите имя, фамилию'),
(2897, 967, 'en', 'Телефон'),
(2898, 967, 'lv', 'Tālrunis'),
(2899, 967, 'ru', 'Телефон'),
(2900, 968, 'en', 'введите номер телефона'),
(2901, 968, 'lv', 'tālrunis'),
(2902, 968, 'ru', 'введите номер телефона'),
(2903, 969, 'en', 'Электронный адрес'),
(2904, 969, 'lv', 'E-pasts'),
(2905, 969, 'ru', 'Электронный адрес'),
(2906, 970, 'en', 'введите электронный адрес'),
(2907, 970, 'lv', 'e-pasts'),
(2908, 970, 'ru', 'введите электронный адрес'),
(2909, 971, 'en', 'Ознакомился и согласен'),
(2910, 971, 'lv', 'Piekritu'),
(2911, 971, 'ru', 'Ознакомился и согласен'),
(2912, 972, 'en', 'с правилами пользования'),
(2913, 972, 'lv', 'с правилами пользования'),
(2914, 972, 'ru', 'с правилами пользования'),
(2915, 973, 'en', 'Разместить объявление'),
(2916, 973, 'lv', 'Разместить объявление'),
(2917, 973, 'ru', 'Разместить объявление'),
(2918, 974, 'en', 'Объявление успешно добавлено, на Ваш электронный адрес выслано письмо'),
(2919, 974, 'lv', 'Объявление успешно добавлено, на Ваш электронный адрес выслано письмо'),
(2920, 974, 'ru', 'Объявление успешно добавлено, на Ваш электронный адрес выслано письмо'),
(2921, 975, 'en', 'Куда перевезти'),
(2922, 975, 'lv', 'Куда перевезти'),
(2923, 975, 'ru', 'Куда перевезти'),
(2924, 976, 'en', 'Откуда:'),
(2925, 976, 'lv', 'Откуда:'),
(2926, 976, 'ru', 'Откуда:'),
(2927, 977, 'en', 'Куда:'),
(2928, 977, 'lv', 'Куда:'),
(2929, 977, 'ru', 'Куда:'),
(2930, 978, 'en', 'Что перевезти'),
(2931, 978, 'lv', 'Что перевезти'),
(2932, 978, 'ru', 'Что перевезти'),
(2933, 979, 'en', 'Добавлено:'),
(2934, 979, 'lv', 'Добавлено:'),
(2935, 979, 'ru', 'Добавлено:'),
(2936, 980, 'en', 'Статус:'),
(2937, 980, 'lv', 'Статус:'),
(2938, 980, 'ru', 'Статус:'),
(2939, 981, 'en', 'открыто'),
(2940, 981, 'lv', 'открыто'),
(2941, 981, 'ru', 'открыто'),
(2942, 982, 'en', 'перевозчик выбран'),
(2943, 982, 'lv', 'перевозчик выбран'),
(2944, 982, 'ru', 'перевозчик выбран'),
(2945, 983, 'en', 'Грузчик:'),
(2946, 983, 'lv', 'Грузчик:'),
(2947, 983, 'ru', 'Грузчик:'),
(2948, 984, 'en', 'нужен'),
(2949, 984, 'lv', 'нужен'),
(2950, 984, 'ru', 'нужен'),
(2951, 985, 'en', 'не нужен'),
(2952, 985, 'lv', 'не нужен'),
(2953, 985, 'ru', 'не нужен'),
(2954, 986, 'en', 'Выгрузка:'),
(2955, 986, 'lv', 'Выгрузка:'),
(2956, 986, 'ru', 'Выгрузка:'),
(2957, 987, 'en', 'нужна'),
(2958, 987, 'lv', 'нужна'),
(2959, 987, 'ru', 'нужна'),
(2960, 988, 'en', 'не нужна'),
(2961, 988, 'lv', 'не нужна'),
(2962, 988, 'ru', 'не нужна'),
(2963, 989, 'en', 'Ваша ставка'),
(2964, 989, 'lv', 'Ваша ставка'),
(2965, 989, 'ru', 'Ваша ставка'),
(2966, 990, 'en', 'введите ставку'),
(2967, 990, 'lv', 'введите ставку'),
(2968, 990, 'ru', 'введите ставку'),
(2969, 991, 'en', 'Сделать предложение'),
(2970, 991, 'lv', 'Сделать предложение'),
(2971, 991, 'ru', 'Сделать предложение'),
(2972, 992, 'en', 'Предложите свою дату'),
(2973, 992, 'lv', 'Предложите свою дату'),
(2974, 992, 'ru', 'Предложите свою дату'),
(2975, 993, 'en', 'Ваш доход'),
(2976, 993, 'lv', 'Ваш доход'),
(2977, 993, 'ru', 'Ваш доход'),
(2978, 994, 'en', 'Комиссия'),
(2979, 994, 'lv', 'Комиссия'),
(2980, 994, 'ru', 'Комиссия'),
(2981, 995, 'en', 'Добавить ставку'),
(2982, 995, 'lv', 'Добавить ставку'),
(2983, 995, 'ru', 'Добавить ставку'),
(2984, 996, 'en', 'Чтобы сделать предложение нужно'),
(2985, 996, 'lv', 'Чтобы сделать предложение нужно'),
(2986, 996, 'ru', 'Чтобы сделать предложение нужно'),
(2987, 997, 'en', 'или'),
(2988, 997, 'lv', 'или'),
(2989, 997, 'ru', 'или'),
(2990, 998, 'en', 'зарегистрироваться'),
(2991, 998, 'lv', 'зарегистрироваться'),
(2992, 998, 'ru', 'Зарегистрироваться'),
(2993, 999, 'en', 'войти'),
(2994, 999, 'lv', 'войти'),
(2995, 999, 'ru', 'Войти'),
(2996, 1000, 'en', 'Перевозчик уже выбран'),
(2997, 1000, 'lv', 'Перевозчик уже выбран'),
(2998, 1000, 'ru', 'Перевозчик уже выбран'),
(2999, 1001, 'en', 'Чтобы задать вопрос нужно'),
(3000, 1001, 'lv', 'Чтобы задать вопрос нужно'),
(3001, 1001, 'ru', 'Чтобы задать вопрос нужно'),
(3002, 1002, 'en', 'Задать вопрос'),
(3003, 1002, 'lv', 'Задать вопрос'),
(3004, 1002, 'ru', 'Задать вопрос'),
(3005, 1003, 'en', 'Ваш вопрос'),
(3006, 1003, 'lv', 'Ваш вопрос'),
(3007, 1003, 'ru', 'Ваш вопрос'),
(3008, 1004, 'en', 'Запрещено писать контактные данные'),
(3009, 1004, 'lv', 'Запрещено писать контактные данные'),
(3010, 1004, 'ru', 'Запрещено писать контактные данные'),
(3011, 1005, 'en', 'Введите вопрос (максимум 500 символов)'),
(3012, 1005, 'lv', 'Введите вопрос (максимум 500 символов)'),
(3013, 1005, 'ru', 'Введите вопрос (максимум 500 символов)'),
(3014, 1006, 'en', 'Задать вопрос'),
(3015, 1006, 'lv', 'Задать вопрос'),
(3016, 1006, 'ru', 'Задать вопрос'),
(3017, 1007, 'en', 'Предложения'),
(3018, 1007, 'lv', 'Pieprasījumi'),
(3019, 1007, 'ru', 'Предложения'),
(3020, 1008, 'en', 'Компания'),
(3021, 1008, 'lv', 'Компания'),
(3022, 1008, 'ru', 'Компания'),
(3023, 1009, 'en', 'Создано'),
(3024, 1009, 'lv', 'Создано'),
(3025, 1009, 'ru', 'Создано'),
(3026, 1010, 'en', 'Дата перевозчика'),
(3027, 1010, 'lv', 'Дата перевозчика'),
(3028, 1010, 'ru', 'Дата перевозчика'),
(3029, 1011, 'en', 'Подтверждение'),
(3030, 1011, 'lv', 'Подтверждение'),
(3031, 1011, 'ru', 'Подтверждение'),
(3032, 1012, 'en', 'Ставка'),
(3033, 1012, 'lv', 'Ставка'),
(3034, 1012, 'ru', 'Ставка'),
(3035, 1013, 'en', 'Принять'),
(3036, 1013, 'lv', 'Принять'),
(3037, 1013, 'ru', 'Принять'),
(3038, 1014, 'en', 'выбран'),
(3039, 1014, 'lv', 'выбран'),
(3040, 1014, 'ru', 'выбран'),
(3041, 1015, 'en', 'Предложений нет, станте первым!!!'),
(3042, 1015, 'lv', 'Предложений нет, станте первым!!!'),
(3043, 1015, 'ru', 'Предложений нет, станте первым!!!'),
(3044, 1016, 'en', 'Вопросы и ответы'),
(3045, 1016, 'lv', 'Вопросы и ответы'),
(3046, 1016, 'ru', 'Вопросы и ответы'),
(3047, 1017, 'en', 'Пожаловаться на Комментарий'),
(3048, 1017, 'lv', 'Пожаловаться на Комментарий'),
(3049, 1017, 'ru', 'Пожаловаться на Комментарий'),
(3050, 1018, 'en', 'Удалить комментарий'),
(3051, 1018, 'lv', 'Удалить комментарий'),
(3052, 1018, 'ru', 'Удалить комментарий'),
(3053, 1019, 'en', 'Заказчик'),
(3054, 1019, 'lv', 'Заказчик'),
(3055, 1019, 'ru', 'Заказчик'),
(3056, 1020, 'en', 'Ваш ответ'),
(3057, 1020, 'lv', 'Ваш ответ'),
(3058, 1020, 'ru', 'Ваш ответ'),
(3059, 1021, 'en', 'Введите ответ (максимум 500 символов)'),
(3060, 1021, 'lv', 'Введите ответ (максимум 500 символов)'),
(3061, 1021, 'ru', 'Введите ответ (максимум 500 символов)'),
(3062, 1022, 'en', 'ответить'),
(3063, 1022, 'lv', 'ответить'),
(3064, 1022, 'ru', 'ответить'),
(3065, 1023, 'en', 'Вопросы не заданы'),
(3066, 1023, 'lv', 'Вопросы не заданы'),
(3067, 1023, 'ru', 'Вопросы не заданы'),
(3068, 1024, 'en', 'Пожаловаться на объявление'),
(3069, 1024, 'lv', 'Пожаловаться на объявление'),
(3070, 1024, 'ru', 'Пожаловаться на объявление'),
(3071, 1025, 'en', 'Удалить объявление'),
(3072, 1025, 'lv', 'Удалить объявление'),
(3073, 1025, 'ru', 'Удалить объявление'),
(3074, 1026, 'en', 'Оцените качество'),
(3075, 1026, 'lv', 'Оцените качество'),
(3076, 1026, 'ru', 'Оцените качество'),
(3077, 1027, 'en', 'Комментарий'),
(3078, 1027, 'lv', 'Комментарий'),
(3079, 1027, 'ru', 'Комментарий'),
(3080, 1028, 'en', 'По желанию оставте комментарий'),
(3081, 1028, 'lv', 'По желанию оставте комментарий'),
(3082, 1028, 'ru', 'По желанию оставте комментарий'),
(3083, 1029, 'en', 'Оценить'),
(3084, 1029, 'lv', 'Оценить'),
(3085, 1029, 'ru', 'Оценить'),
(3086, 1030, 'en', 'Validation fails'),
(3087, 1030, 'lv', 'Nepareizi aizpilditi laukumi'),
(3088, 1030, 'ru', 'Ошибки в валидации данных'),
(3089, 1031, 'en', 'Ставка добавлена'),
(3090, 1031, 'lv', 'Ставка добавлена'),
(3091, 1031, 'ru', 'Ставка добавлена'),
(3092, 1032, 'en', 'Вы выбрали перевозчика, он с Вами свяжется'),
(3093, 1032, 'lv', 'Вы выбрали перевозчика, он с Вами свяжется'),
(3094, 1032, 'ru', 'Вы выбрали перевозчика, он с Вами свяжется'),
(3095, 1033, 'en', 'Произошла ошибка'),
(3096, 1033, 'lv', 'Произошла ошибка'),
(3097, 1033, 'ru', 'Произошла ошибка'),
(3098, 1034, 'en', 'Спасибо за оценку перевозчика'),
(3099, 1034, 'lv', 'Спасибо за оценку перевозчика'),
(3100, 1034, 'ru', 'Спасибо за оценку перевозчика'),
(3101, 1035, 'en', 'Комментарий добавлен'),
(3102, 1035, 'lv', 'Комментарий добавлен'),
(3103, 1035, 'ru', 'Комментарий добавлен'),
(3104, 1036, 'en', 'Спасибо жалоба принята'),
(3105, 1036, 'lv', 'Спасибо жалоба принята'),
(3106, 1036, 'ru', 'Спасибо жалоба принята'),
(3107, 1037, 'en', 'Вы уже отправляли жалобу'),
(3108, 1037, 'lv', 'Вы уже отправляли жалобу'),
(3109, 1037, 'ru', 'Вы уже отправляли жалобу'),
(3110, 1038, 'en', 'Объявление удалено'),
(3111, 1038, 'lv', 'Объявление удалено'),
(3112, 1038, 'ru', 'Объявление удалено'),
(3113, 1039, 'en', 'Комментарий удален'),
(3114, 1039, 'lv', 'Комментарий удален'),
(3115, 1039, 'ru', 'Комментарий удален'),
(3116, 1040, 'en', 'на первую'),
(3117, 1040, 'lv', 'на первую'),
(3118, 1040, 'ru', 'на первую'),
(3119, 1041, 'en', 'в конец'),
(3120, 1041, 'lv', 'в конец'),
(3121, 1041, 'ru', 'в конец'),
(3122, 1042, 'en', 'Предложения'),
(3123, 1042, 'lv', 'Jauni pieprasījumi'),
(3124, 1042, 'ru', 'Предложения'),
(3125, 1043, 'en', 'Категории'),
(3126, 1043, 'lv', 'Категории'),
(3127, 1043, 'ru', 'Категории'),
(3128, 1044, 'en', 'Маршрут'),
(3129, 1044, 'lv', 'Маршрут'),
(3130, 1044, 'ru', 'Маршрут'),
(3131, 1045, 'en', 'подобрать'),
(3132, 1045, 'lv', 'подобрать'),
(3133, 1045, 'ru', 'подобрать'),
(3134, 1046, 'en', 'Change password'),
(3135, 1046, 'lv', 'Nomainit paroli'),
(3136, 1046, 'ru', 'Смена пароля'),
(3137, 1047, 'en', 'New password'),
(3138, 1047, 'lv', 'Jauna parole'),
(3139, 1047, 'ru', 'Новый пароль'),
(3140, 1048, 'en', 'Enter new password'),
(3141, 1048, 'lv', 'Ievadiet jauno paroli'),
(3142, 1048, 'ru', 'Введите новый пароль'),
(3143, 1049, 'en', 'Repeat new password'),
(3144, 1049, 'lv', 'Jaunas paroles atkartosana'),
(3145, 1049, 'ru', 'Повтор нового пароля'),
(3146, 1050, 'en', 'Enter repeat new password'),
(3147, 1050, 'lv', 'Ievadiet atkartoti jauno paroli'),
(3148, 1050, 'ru', 'Введите повтор нового пароля'),
(3149, 1051, 'en', 'Contact information'),
(3150, 1051, 'lv', 'Kontaktinformacija'),
(3151, 1051, 'ru', 'Контактная информация'),
(3152, 1052, 'en', 'Username'),
(3153, 1052, 'lv', 'Lietotaja vards'),
(3154, 1052, 'ru', 'Имя пользователя'),
(3155, 1053, 'en', 'Enter username'),
(3156, 1053, 'lv', 'Ievadiet lietotaja vardu'),
(3157, 1053, 'ru', 'Введите Имя пользователя'),
(3158, 1054, 'en', 'Phone number'),
(3159, 1054, 'lv', 'Telefona numurs'),
(3160, 1054, 'ru', 'Номер телефона'),
(3161, 1055, 'en', 'Enter phone number'),
(3162, 1055, 'lv', 'Ievadiet telefona numuru'),
(3163, 1055, 'ru', 'Введите Номер телефона'),
(3164, 1056, 'en', 'Summary information'),
(3165, 1056, 'lv', 'Kopeja informacija'),
(3166, 1056, 'ru', 'Основная информация'),
(3167, 1057, 'en', 'Физическое лицо/Юридическое лицо'),
(3168, 1057, 'lv', 'Fiziska persona/Juridiska persona'),
(3169, 1057, 'ru', 'Физическое лицо/Юридическое лицо'),
(3170, 1058, 'en', 'Физическое лицо'),
(3171, 1058, 'lv', 'Fiziska persona'),
(3172, 1058, 'ru', 'Физическое лицо'),
(3173, 1059, 'en', 'Юридическое лицо'),
(3174, 1059, 'lv', 'Juridiska persona'),
(3175, 1059, 'ru', 'Юридическое лицо'),
(3176, 1060, 'en', 'About'),
(3177, 1060, 'lv', 'Par mani'),
(3178, 1060, 'ru', 'О себе'),
(3179, 1061, 'en', 'Describe few words'),
(3180, 1061, 'lv', 'Uzrakstiet isi par mani'),
(3181, 1061, 'ru', 'Опишите коротко о себе'),
(3182, 1062, 'en', 'Categories'),
(3183, 1062, 'lv', 'Kategorijas'),
(3184, 1062, 'ru', 'Категории'),
(3185, 1063, 'en', 'Select delivery categories'),
(3186, 1063, 'lv', 'Izvelies piegades kategoriju'),
(3187, 1063, 'ru', 'Выберите категории доставки'),
(3188, 1064, 'en', 'Carrier settings'),
(3189, 1064, 'lv', 'Parvedeja iestatijumi'),
(3190, 1064, 'ru', 'Настройки перевозчика'),
(3191, 1065, 'en', 'Where are You from'),
(3192, 1065, 'lv', 'Kur Jus dzivojat'),
(3193, 1065, 'ru', 'Выберите страну проживания'),
(3194, 1066, 'en', 'Change mail'),
(3195, 1066, 'lv', 'Nomainit epastu'),
(3196, 1066, 'ru', 'Смена почтового адреса'),
(3197, 1067, 'en', 'Uz jauno epastu aizutita velstule, ar aktivacijas kodu'),
(3198, 1067, 'lv', 'Uz jauno epastu aizutita velstule, ar aktivacijas kodu'),
(3199, 1067, 'ru', 'На новый почтовый адрес выслано письмо с кодом активации'),
(3200, 1068, 'en', 'Email success changed'),
(3201, 1068, 'lv', 'Epasts veiksmigi nomainits'),
(3202, 1068, 'ru', 'Почтовый адрес успешно сменен'),
(3203, 1069, 'en', 'Wrong code'),
(3204, 1069, 'lv', 'kods ir nepareizs'),
(3205, 1069, 'ru', 'Код не верен'),
(3206, 1070, 'en', 'Change new mail'),
(3207, 1070, 'lv', 'Epasta nomainisana'),
(3208, 1070, 'ru', 'Смена почтового адреса'),
(3209, 1071, 'en', 'New mail'),
(3210, 1071, 'lv', 'jauna pasta adrese'),
(3211, 1071, 'ru', 'Новый почтовый адрес'),
(3212, 1072, 'en', 'Enter new mail'),
(3213, 1072, 'lv', 'ВIevadiet jauno pasta adresi'),
(3214, 1072, 'ru', 'Введите новый почтовый адрес'),
(3215, 1073, 'en', 'Code comfirmation'),
(3216, 1073, 'lv', 'Koda apstiprinasana'),
(3217, 1073, 'ru', 'Подтверждение кода'),
(3218, 1074, 'en', 'Code'),
(3219, 1074, 'lv', 'Kods'),
(3220, 1074, 'ru', 'Код'),
(3221, 1075, 'en', 'Enret code'),
(3222, 1075, 'lv', 'Ievadiet kodu'),
(3223, 1075, 'ru', 'Введите код'),
(3224, 1076, 'en', 'Mail server error, please try again later'),
(3225, 1076, 'lv', 'Servera kluda, meginiet velak'),
(3226, 1076, 'ru', 'Ошибка при отправке почты, попробуйте позже'),
(3227, 1077, 'en', 'Dashboard'),
(3228, 1077, 'lv', 'Pilns apskats'),
(3229, 1077, 'ru', 'Обзор информации'),
(3230, 1078, 'en', 'Accaunt'),
(3231, 1078, 'lv', 'Akkaunts'),
(3232, 1078, 'ru', 'Аккаунт'),
(3233, 1079, 'en', 'Log Out'),
(3234, 1079, 'lv', 'Iziet'),
(3235, 1079, 'ru', 'Выйти'),
(3236, 1080, 'en', 'Ads'),
(3237, 1080, 'lv', 'Sludinajumi'),
(3238, 1080, 'ru', 'Объявления'),
(3239, 1081, 'en', 'Success updated'),
(3240, 1081, 'lv', 'Veigsmigi atjaunots'),
(3241, 1081, 'ru', 'Успешно обновлено'),
(3242, 1082, 'en', 'Save'),
(3243, 1082, 'lv', 'Saglabat'),
(3244, 1082, 'ru', 'Сохранить'),
(3245, 1083, 'en', 'Update'),
(3246, 1083, 'lv', 'Atjaunot'),
(3247, 1083, 'ru', 'Обновить'),
(3248, 1084, 'en', 'Change'),
(3249, 1084, 'lv', 'Nomainit'),
(3250, 1084, 'ru', 'Сменить'),
(3251, 1085, 'en', 'Comfirm'),
(3252, 1085, 'lv', 'Apstiprinat'),
(3253, 1085, 'ru', 'Подтвердить'),
(3254, 1086, 'en', 'Профиль'),
(3255, 1086, 'lv', 'Профиль'),
(3256, 1086, 'ru', 'Профиль'),
(3257, 1087, 'en', 'Мои ставки'),
(3258, 1087, 'lv', 'Мои ставки'),
(3259, 1087, 'ru', 'Мои ставки'),
(3260, 1088, 'en', 'Мои заказы'),
(3261, 1088, 'lv', 'Мои заказы'),
(3262, 1088, 'ru', 'Мои заказы'),
(3263, 1089, 'en', 'Мой рейтинг'),
(3264, 1089, 'lv', 'Мой рейтинг'),
(3265, 1089, 'ru', 'Мой рейтинг'),
(3266, 1090, 'en', 'Мои объявления'),
(3267, 1090, 'lv', 'Мои объявления'),
(3268, 1090, 'ru', 'Мои объявления'),
(3269, 1091, 'en', 'Избранное'),
(3270, 1091, 'lv', 'Избранное'),
(3271, 1091, 'ru', 'Избранное'),
(3272, 1092, 'en', 'Регистрация'),
(3273, 1092, 'lv', 'Регистрация'),
(3274, 1092, 'ru', 'Регистрация'),
(3275, 1093, 'en', 'Пользователь'),
(3276, 1093, 'lv', 'Пользователь'),
(3277, 1093, 'ru', 'Пользователь'),
(3278, 1094, 'en', 'Перевозчик'),
(3279, 1094, 'lv', 'Перевозчик'),
(3280, 1094, 'ru', 'Перевозчик'),
(3281, 1095, 'en', 'Изображение'),
(3282, 1095, 'lv', 'Изображение'),
(3283, 1095, 'ru', 'Изображение'),
(3284, 1096, 'en', 'Название объявления'),
(3285, 1096, 'lv', 'Название объявления'),
(3286, 1096, 'ru', 'Название объявления'),
(3287, 1097, 'en', 'Подтверждено'),
(3288, 1097, 'lv', 'Подтверждено'),
(3289, 1097, 'ru', 'Подтверждено'),
(3290, 1098, 'en', 'Статус объявления'),
(3291, 1098, 'lv', 'Статус объявления'),
(3292, 1098, 'ru', 'Статус объявления'),
(3293, 1099, 'en', 'Дата ставки'),
(3294, 1099, 'lv', 'Дата ставки'),
(3295, 1099, 'ru', 'Дата ставки'),
(3296, 1100, 'en', 'Просмотр'),
(3297, 1100, 'lv', 'Просмотр'),
(3298, 1100, 'ru', 'Просмотр'),
(3299, 1101, 'en', 'да'),
(3300, 1101, 'lv', 'да'),
(3301, 1101, 'ru', 'да'),
(3302, 1102, 'en', 'нет'),
(3303, 1102, 'lv', 'нет'),
(3304, 1102, 'ru', 'нет'),
(3305, 1103, 'en', 'Введите почтовый адрес'),
(3306, 1103, 'lv', 'Введите почтовый адрес'),
(3307, 1103, 'ru', 'Введите почтовый адрес'),
(3308, 1104, 'en', 'Введите пароль'),
(3309, 1104, 'lv', 'Введите пароль'),
(3310, 1104, 'ru', 'Введите пароль'),
(3311, 1105, 'en', 'запомнить меня'),
(3312, 1105, 'lv', 'запомнить меня'),
(3313, 1105, 'ru', 'запомнить меня'),
(3314, 1106, 'en', 'Забыли пароль?'),
(3315, 1106, 'lv', 'Забыли пароль?'),
(3316, 1106, 'ru', 'Забыли пароль?'),
(3317, 1107, 'en', 'Вход на сайт'),
(3318, 1107, 'lv', 'Вход на сайт'),
(3319, 1107, 'ru', 'Вход на сайт'),
(3320, 1108, 'en', 'Восстановление пароля'),
(3321, 1108, 'lv', 'Восстановление пароля'),
(3322, 1108, 'ru', 'Восстановление пароля'),
(3323, 1109, 'en', 'Отправить'),
(3324, 1109, 'lv', 'Отправить'),
(3325, 1109, 'ru', 'Отправить'),
(3326, 1110, 'en', 'Изменить'),
(3327, 1110, 'lv', 'Изменить'),
(3328, 1110, 'ru', 'Изменить'),
(3329, 1111, 'en', 'Проблемы при отправке почты, попробуйте позже'),
(3330, 1111, 'lv', 'Проблемы при отправке почты, попробуйте позже'),
(3331, 1111, 'ru', 'Проблемы при отправке почты, попробуйте позже'),
(3332, 1112, 'en', 'Спасибо за регистрацию, на Ваш почтовый адрес выслано письмо с активацией'),
(3333, 1112, 'lv', 'Спасибо за регистрацию, на Ваш почтовый адрес выслано письмо с активацией'),
(3334, 1112, 'ru', 'Спасибо за регистрацию, на Ваш почтовый адрес выслано письмо с активацией'),
(3335, 1113, 'en', 'перевозчика'),
(3336, 1113, 'lv', 'перевозчика'),
(3337, 1113, 'ru', 'перевозчика'),
(3338, 1114, 'en', 'пользователя'),
(3339, 1114, 'lv', 'пользователя'),
(3340, 1114, 'ru', 'пользователя'),
(3341, 1115, 'en', 'Пароль'),
(3342, 1115, 'lv', 'Пароль'),
(3343, 1115, 'ru', 'Пароль'),
(3344, 1116, 'en', 'Повтор пароля'),
(3345, 1116, 'lv', 'Повтор пароля'),
(3346, 1116, 'ru', 'Повтор пароля'),
(3347, 1117, 'en', 'Введите повтор пароля'),
(3348, 1117, 'lv', 'Введите повтор пароля'),
(3349, 1117, 'ru', 'Введите повтор пароля'),
(3350, 1118, 'en', 'Имя'),
(3351, 1118, 'lv', 'Имя'),
(3352, 1118, 'ru', 'Имя'),
(3353, 1119, 'en', 'Продолжить'),
(3354, 1119, 'lv', 'Продолжить'),
(3355, 1119, 'ru', 'Продолжить'),
(3356, 1120, 'en', 'Нужна быстрая и дешевая перевозка?'),
(3357, 1120, 'lv', 'Нужна быстрая и дешевая перевозка?'),
(3358, 1120, 'ru', 'Нужна быстрая и дешевая перевозка?'),
(3359, 1121, 'en', 'Подайте объявление'),
(3360, 1121, 'lv', 'Подайте объявление'),
(3361, 1121, 'ru', 'Подайте объявление'),
(3362, 1122, 'lv', 'Маршрут'),
(3363, 1122, 'ru', 'Маршрут'),
(3364, 1122, 'en', 'Маршрут'),
(3365, 1123, 'lv', 'Apstakts'),
(3366, 1123, 'ru', 'Главная'),
(3367, 1123, 'en', 'Dashboard'),
(3368, 1124, 'lv', 'Lietotāji'),
(3369, 1124, 'ru', 'Пользователи'),
(3370, 1124, 'en', 'Users'),
(3371, 1125, 'lv', 'Sludinājumi'),
(3372, 1125, 'ru', 'Объявления'),
(3373, 1125, 'en', 'Adverts'),
(3374, 1126, 'lv', 'Navigācija'),
(3375, 1126, 'ru', 'Навигация'),
(3376, 1126, 'en', 'Navigation'),
(3377, 1127, 'lv', 'Kategorijas'),
(3378, 1127, 'ru', 'Категории'),
(3379, 1127, 'en', 'Categories'),
(3380, 1128, 'lv', 'Lappuses'),
(3381, 1128, 'ru', 'Страницы'),
(3382, 1128, 'en', 'Pages'),
(3383, 1129, 'lv', 'Veidnes'),
(3384, 1129, 'ru', 'Шаблоны'),
(3385, 1129, 'en', 'Templates'),
(3386, 1130, 'lv', 'Blogs'),
(3387, 1130, 'ru', 'Блог'),
(3388, 1130, 'en', 'Blog'),
(3389, 1131, 'lv', 'Pasta sūtīšana'),
(3390, 1131, 'ru', 'Рассылка'),
(3391, 1131, 'en', 'Emails'),
(3392, 1132, 'lv', 'Vērtējumi'),
(3393, 1132, 'ru', 'Рейтинги'),
(3394, 1132, 'en', 'Ratings'),
(3395, 1133, 'lv', 'Filtri'),
(3396, 1133, 'ru', 'Фильтры'),
(3397, 1133, 'en', 'Filters'),
(3398, 1134, 'lv', 'Komisijas'),
(3399, 1134, 'ru', 'Комиссии'),
(3400, 1134, 'en', 'Commission'),
(3401, 1135, 'lv', 'Sūdzības'),
(3402, 1135, 'ru', 'Жалобы'),
(3403, 1135, 'en', 'Complaints'),
(3404, 1136, 'lv', 'Tulkojumi'),
(3405, 1136, 'ru', 'Переводы'),
(3406, 1136, 'en', 'Translations'),
(3407, 1137, 'lv', 'Iestatījumi'),
(3408, 1137, 'ru', 'Настройки'),
(3409, 1137, 'en', 'Settings'),
(3410, 1138, 'lv', 'Valodas'),
(3411, 1138, 'ru', 'Языки'),
(3412, 1138, 'en', 'Languages'),
(3413, 1139, 'lv', 'Lapas iestatījumi'),
(3414, 1139, 'ru', 'Настройки сайта'),
(3415, 1139, 'en', 'Site settings'),
(3416, 1140, 'lv', 'Lapas statistika'),
(3417, 1140, 'ru', 'Статитстика сайта'),
(3418, 1140, 'en', 'Site statistics'),
(3419, 1141, 'lv', 'Lietotāju statistika'),
(3420, 1141, 'ru', 'Статистика пользователей'),
(3421, 1141, 'en', 'User statistics'),
(3422, 1142, 'lv', 'Kopā'),
(3423, 1142, 'ru', 'Всего'),
(3424, 1142, 'en', 'Total'),
(3425, 1143, 'lv', 'Aktīvo'),
(3426, 1143, 'ru', 'Активных'),
(3427, 1143, 'en', 'Active'),
(3428, 1144, 'lv', 'Neaktīvo'),
(3429, 1144, 'ru', 'Неактивных'),
(3430, 1144, 'en', 'Inactive'),
(3431, 1145, 'lv', 'Sludinājumi statistika'),
(3432, 1145, 'ru', 'Статистика объявлений'),
(3433, 1145, 'en', 'Adverts statistics'),
(3434, 1146, 'lv', 'Atvērto'),
(3435, 1146, 'ru', 'Открытых'),
(3436, 1146, 'en', 'Open'),
(3437, 1147, 'lv', 'Aizvērto'),
(3438, 1147, 'ru', 'Закрытых'),
(3439, 1147, 'en', 'Closed'),
(3440, 1148, 'lv', 'Izdzēsto'),
(3441, 1148, 'ru', 'Удаленных'),
(3442, 1148, 'en', 'Deleted'),
(3443, 1149, 'lv', 'Pārskats'),
(3444, 1149, 'ru', 'Обзор'),
(3445, 1149, 'en', 'Overview'),
(3446, 1150, 'lv', 'Jauns lietotājs'),
(3447, 1150, 'ru', 'Новый пользователь'),
(3448, 1150, 'en', 'New user'),
(3449, 1151, 'lv', 'Jaunā grupa'),
(3450, 1151, 'ru', 'Новая группа'),
(3451, 1151, 'en', 'New group'),
(3452, 1152, 'lv', 'Meklēt '),
(3453, 1152, 'ru', 'Найти'),
(3454, 1152, 'en', 'Search'),
(3455, 1153, 'lv', 'Meklēšana'),
(3456, 1153, 'ru', 'Поиск'),
(3457, 1153, 'en', 'Searching'),
(3458, 1154, 'lv', 'Lietotāju pārvaldība'),
(3459, 1154, 'ru', 'Управление пользователями'),
(3460, 1154, 'en', 'User management'),
(3461, 1155, 'lv', 'E-pasts'),
(3462, 1155, 'ru', 'Почтовый адрес'),
(3463, 1155, 'en', 'Email'),
(3464, 1156, 'lv', 'Grupas'),
(3465, 1156, 'ru', 'Группы'),
(3466, 1156, 'en', 'Groups'),
(3467, 1157, 'lv', 'Statuss'),
(3468, 1157, 'ru', 'Статус'),
(3469, 1157, 'en', 'Status'),
(3470, 1158, 'lv', 'Opcijas'),
(3471, 1158, 'ru', 'Опции'),
(3472, 1158, 'en', 'Options'),
(3473, 1159, 'lv', 'Vai jūs tiešām vēlaties dzēst ierakstu?'),
(3474, 1159, 'ru', 'Вы действительно хотите удалить запись?'),
(3475, 1159, 'en', 'Are you sure you want to delete the record?'),
(3476, 1160, 'lv', 'Nekas nav atrasts'),
(3477, 1160, 'ru', 'Ничего не найдено'),
(3478, 1160, 'en', 'Nothing found'),
(3479, 1161, 'lv', 'Aktivs'),
(3480, 1161, 'ru', 'Активный'),
(3481, 1161, 'en', 'Active'),
(3482, 1162, 'lv', 'Neaktīvs'),
(3483, 1162, 'ru', 'Неактивный'),
(3484, 1162, 'en', 'Inactive'),
(3485, 1163, 'lv', 'Vai tiešām vēlaties deaktivizēt lietotāju %s ?'),
(3486, 1163, 'ru', 'Вы уверены, что хотите активировать пользователя %s ?'),
(3487, 1163, 'en', 'Are you sure you want to activate the user %s ?'),
(3488, 1164, 'lv', 'Ja'),
(3489, 1164, 'ru', 'Да'),
(3490, 1164, 'en', 'Yes'),
(3491, 1165, 'lv', 'Ne'),
(3492, 1165, 'ru', 'Нет'),
(3493, 1165, 'en', 'No'),
(3494, 1166, 'lv', 'Deaktivizēt'),
(3495, 1166, 'ru', 'Деактивировать'),
(3496, 1166, 'en', 'Deactivate'),
(3497, 1167, 'lv', 'Navigāciju pārvalde'),
(3498, 1167, 'ru', 'Управление навигациями сайта'),
(3499, 1167, 'en', 'Manage site navigation'),
(3500, 1168, 'lv', 'Pievienot navigāciju'),
(3501, 1168, 'ru', 'Добавить навигацию'),
(3502, 1168, 'en', 'Add navigation'),
(3503, 1169, 'lv', 'Unikāls nosaukums'),
(3504, 1169, 'ru', 'Уникальное имя'),
(3505, 1169, 'en', 'Unique name'),
(3506, 1170, 'lv', 'Pievienot izvēlnes punktu'),
(3507, 1170, 'ru', 'Добавить пункт меню'),
(3508, 1170, 'en', 'Add menu item'),
(3509, 1171, 'lv', 'Kārtot'),
(3510, 1171, 'ru', 'Сортировать'),
(3511, 1171, 'en', 'Order'),
(3512, 1172, 'lv', 'Virsraksts'),
(3513, 1172, 'ru', 'Заголовок'),
(3514, 1172, 'en', 'Title'),
(3515, 1173, 'lv', 'Attēlots '),
(3516, 1173, 'ru', 'Отображение'),
(3517, 1173, 'en', 'Display'),
(3518, 1174, 'lv', 'Pievienots '),
(3519, 1174, 'ru', 'Создано'),
(3520, 1174, 'en', 'Created'),
(3521, 1175, 'lv', 'Rediģēšana'),
(3522, 1175, 'ru', 'Редактирование'),
(3523, 1175, 'en', 'Editing'),
(3524, 1176, 'lv', 'Radīt / Neradīt'),
(3525, 1176, 'ru', 'Отображать / Не отображать'),
(3526, 1176, 'en', 'Display / Not display'),
(3527, 1177, 'lv', 'Radīt'),
(3528, 1177, 'ru', 'Отображать'),
(3529, 1177, 'en', 'Display'),
(3530, 1178, 'lv', ' Neradīt'),
(3531, 1178, 'ru', 'Не отоброжать'),
(3532, 1178, 'en', 'Not display'),
(3533, 1179, 'lv', 'Saglabāt'),
(3534, 1179, 'ru', 'Сохранить'),
(3535, 1179, 'en', 'Save'),
(3536, 1180, 'lv', 'Nosaukums '),
(3537, 1180, 'ru', 'Название пункта меню'),
(3538, 1180, 'en', 'Menu item name'),
(3539, 1181, 'lv', 'Piešķirt'),
(3540, 1181, 'ru', 'Связать'),
(3541, 1181, 'en', 'Assign'),
(3542, 1182, 'lv', 'Pievienot'),
(3543, 1182, 'ru', 'Создать'),
(3544, 1182, 'en', 'Create'),
(3545, 1183, 'lv', 'Старые'),
(3546, 1183, 'ru', 'Старые'),
(3547, 1183, 'en', 'Старые'),
(3548, 1184, 'lv', '1 vieta'),
(3549, 1184, 'ru', '1 место'),
(3550, 1184, 'en', '1st place'),
(3551, 1185, 'lv', '2-3 vieta'),
(3552, 1185, 'ru', '2-3 место'),
(3553, 1185, 'en', '2-3 place'),
(3554, 1186, 'lv', ' > 4'),
(3555, 1186, 'ru', ' > 4'),
(3556, 1186, 'en', ' > 4'),
(3557, 1187, 'lv', 'Вы отказались от заказа'),
(3558, 1187, 'ru', 'Вы отказались от заказа'),
(3559, 1187, 'en', 'Вы отказались от заказа'),
(3560, 1188, 'lv', 'Дата подтверждения'),
(3561, 1188, 'ru', 'Дата подтверждения'),
(3562, 1188, 'en', 'Дата подтверждения'),
(3563, 1189, 'lv', 'отказались'),
(3564, 1189, 'ru', 'отказались'),
(3565, 1189, 'en', 'отказались'),
(3566, 1190, 'lv', 'оформлен'),
(3567, 1190, 'ru', 'оформлен'),
(3568, 1190, 'en', 'оформлен'),
(3569, 1191, 'lv', 'новый'),
(3570, 1191, 'ru', 'новый'),
(3571, 1191, 'en', 'новый'),
(3572, 1192, 'lv', 'Причина'),
(3573, 1192, 'ru', 'Причина'),
(3574, 1192, 'en', 'Причина'),
(3575, 1193, 'lv', 'Отказ от заказа'),
(3576, 1193, 'ru', 'Отказ от заказа'),
(3577, 1193, 'en', 'Отказ от заказа'),
(3578, 1194, 'lv', 'Отказаться'),
(3579, 1194, 'ru', 'Отказаться'),
(3580, 1194, 'en', 'Отказаться'),
(3581, 1195, 'lv', 'Информация'),
(3582, 1195, 'ru', 'Информация'),
(3583, 1195, 'en', 'Информация'),
(3584, 1196, 'lv', 'Имя/Название компании'),
(3585, 1196, 'ru', 'Имя/Название компании'),
(3586, 1196, 'en', 'Имя/Название компании'),
(3587, 1197, 'lv', 'Категории перевозок'),
(3588, 1197, 'ru', 'Категории перевозок'),
(3589, 1197, 'en', 'Категории перевозок'),
(3590, 1198, 'lv', 'Дополнительная информация'),
(3591, 1198, 'ru', 'Дополнительная информация'),
(3592, 1198, 'en', 'Дополнительная информация'),
(3593, 1199, 'lv', 'Рейтинги перевозчика'),
(3594, 1199, 'ru', 'Рейтинги перевозчика'),
(3595, 1199, 'en', 'Рейтинги перевозчика'),
(3596, 1200, 'lv', 'Название рейтинга'),
(3597, 1200, 'ru', 'Название рейтинга'),
(3598, 1200, 'en', 'Название рейтинга'),
(3599, 1201, 'lv', 'Рейтинг'),
(3600, 1201, 'ru', 'Рейтинг'),
(3601, 1201, 'en', 'Рейтинг'),
(3602, 1202, 'lv', 'Количество оценок'),
(3603, 1202, 'ru', 'Количество оценок'),
(3604, 1202, 'en', 'Количество оценок'),
(3605, 1203, 'lv', 'Срений рейтинг'),
(3606, 1203, 'ru', 'Срений рейтинг'),
(3607, 1203, 'en', 'Срений рейтинг'),
(3608, 1204, 'lv', 'Ваш средний рейтинг'),
(3609, 1204, 'ru', 'Ваш средний рейтинг'),
(3610, 1204, 'en', 'Ваш средний рейтинг'),
(3611, 1205, 'lv', 'нет оценок'),
(3612, 1205, 'ru', 'нет оценок'),
(3613, 1205, 'en', 'нет оценок');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `lang` varchar(2) NOT NULL,
  `new_mail` varchar(255) DEFAULT NULL,
  `new_mail_code` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `lang`, `new_mail`, `new_mail_code`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$OLIe0Vwg4Y3DRjqauA.m3OYVh6NidsZndbYFvcCr9F6sb1jUPptda', '', 'admin@admin.com', '', NULL, NULL, 'iGjKKTg2XK7pe71rKTU7FO', 1268889823, 1413384835, 1, 'Super admin', 'admin', 'mega carriers company', NULL, 'lv', NULL, NULL),
(11, '127.0.0.1', 'carier', '$2y$08$Dm899SZEYQPJH2AX2U/l4.boM8vlOn0b7IVj81/EG9nMswv7wim92', NULL, 'carrier@carrier.com', NULL, NULL, NULL, NULL, 1410855291, 1413379739, 1, NULL, NULL, 'carrier company', '37128292293', 'lv', NULL, NULL),
(20, '127.0.0.1', 'member@member.com', '$2y$08$MUqKIxLi2cxbEI2bCyj7POW4knVcB9dxs/RaWOzbS51EwZHQSTudm', NULL, 'member@member.com', NULL, NULL, NULL, NULL, 1410940827, 1412858447, 1, NULL, NULL, NULL, NULL, 'ru', NULL, NULL),
(21, '127.0.0.1', 'carrier2@carrier.com', '$2y$08$VG.YVbbErnzltg1EqtKb2.NZz34qumHwRTwIalAc6t4NEJOysUitq', NULL, 'carrier2@carrier.com', NULL, NULL, NULL, NULL, 1411112649, 1413294772, 1, NULL, NULL, 'carrier 2', NULL, 'lv', NULL, NULL),
(22, '127.0.0.1', 'carrier@carrier22.com', '$2y$08$JFm4AbDzNSFicm0kWXg8xO2wyiByH7VpU8IhseId3DtySkUYqYxu.', NULL, 'carrier@carrier22.com', '048d42e4440cd10a2babcbb9c47ca756c84722ab', NULL, NULL, NULL, 1413279651, 1413279651, 0, NULL, NULL, NULL, NULL, 'ru', NULL, NULL),
(23, '127.0.0.1', 'test@admin.com', '$2y$08$NqhnrZezIcmNu7Afxv1ojuXxIloPUzHKVakG.P8bg2bxZ4RFWsdbu', NULL, 'test@admin.com', '6fe5240f8e0e4ec92d2fb7185074f7f50e242861', NULL, NULL, NULL, 1413279678, 1413279678, 0, NULL, NULL, NULL, NULL, 'en', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(64, 1, 1),
(65, 1, 2),
(66, 1, 3),
(51, 11, 2),
(52, 11, 3),
(61, 20, 2),
(62, 21, 2),
(63, 21, 3),
(67, 22, 2),
(68, 23, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attributes_translate`
--
ALTER TABLE `attributes_translate`
  ADD CONSTRAINT `attributes_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attributes_values`
--
ALTER TABLE `attributes_values`
  ADD CONSTRAINT `attributes_values_ibfk_1` FOREIGN KEY (`attributes_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attributes_values_translate`
--
ALTER TABLE `attributes_values_translate`
  ADD CONSTRAINT `attributes_values_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `attributes_values` (`values_id`) ON DELETE CASCADE;

--
-- Constraints for table `carriers`
--
ALTER TABLE `carriers`
  ADD CONSTRAINT `carriers_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories_translate`
--
ALTER TABLE `categories_translate`
  ADD CONSTRAINT `categories_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `emails_translate`
--
ALTER TABLE `emails_translate`
  ADD CONSTRAINT `emails_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `emails` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `favorite_adverts`
--
ALTER TABLE `favorite_adverts`
  ADD CONSTRAINT `favorite_adverts_ibfk_1` FOREIGN KEY (`ads_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `favorite_adverts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `navigation_translate`
--
ALTER TABLE `navigation_translate`
  ADD CONSTRAINT `navigation_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `navigation` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders_products`
--
ALTER TABLE `orders_products`
  ADD CONSTRAINT `orders_products_ibfk_1` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `orders_products_ibfk_2` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`);

--
-- Constraints for table `pages_categories`
--
ALTER TABLE `pages_categories`
  ADD CONSTRAINT `pages_categories_ibfk_1` FOREIGN KEY (`pages_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pages_categories_ibfk_2` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages_translate`
--
ALTER TABLE `pages_translate`
  ADD CONSTRAINT `pages_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products_attributes_values`
--
ALTER TABLE `products_attributes_values`
  ADD CONSTRAINT `products_attributes_values_ibfk_1` FOREIGN KEY (`attributes_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `products_attributes_values_ibfk_3` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `products_translate`
--
ALTER TABLE `products_translate`
  ADD CONSTRAINT `products_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_category`
--
ALTER TABLE `product_category`
  ADD CONSTRAINT `product_category_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `raitings_translate`
--
ALTER TABLE `raitings_translate`
  ADD CONSTRAINT `raitings_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `raitings` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `settings_translate`
--
ALTER TABLE `settings_translate`
  ADD CONSTRAINT `settings_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `settings` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `translates_translate`
--
ALTER TABLE `translates_translate`
  ADD CONSTRAINT `translates_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `translates` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
