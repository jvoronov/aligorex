-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 16 2014 г., 11:54
-- Версия сервера: 5.5.24-log
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `aligorex`
--

-- --------------------------------------------------------

--
-- Структура таблицы `translates`
--

CREATE TABLE IF NOT EXISTS `translates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1216 ;

--
-- Дамп данных таблицы `translates`
--

INSERT INTO `translates` (`id`, `key`) VALUES
(1215, 'b_menu_translations');

-- --------------------------------------------------------

--
-- Структура таблицы `translates_translate`
--

CREATE TABLE IF NOT EXISTS `translates_translate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3643 ;

--
-- Дамп данных таблицы `translates_translate`
--

INSERT INTO `translates_translate` (`id`, `table_id`, `lang`, `value`) VALUES
(3641, 1215, 'ru', 'Переводы'),
(3642, 1215, 'lv', 'Переводы');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `translates_translate`
--
ALTER TABLE `translates_translate`
  ADD CONSTRAINT `translates_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `translates` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
