-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 17 2014 г., 11:10
-- Версия сервера: 5.5.24-log
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `aligorex`
--

-- --------------------------------------------------------

--
-- Структура таблицы `attributes`
--

CREATE TABLE IF NOT EXISTS `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `order` int(10) NOT NULL,
  `img` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `attributes`
--

INSERT INTO `attributes` (`id`, `status`, `order`, `img`) VALUES
(2, 1, 2, ''),
(4, 1, 1, 'b66998535f33b70d348a338132d68d25.png');

-- --------------------------------------------------------

--
-- Структура таблицы `attributes_translate`
--

CREATE TABLE IF NOT EXISTS `attributes_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `attributes_translate`
--

INSERT INTO `attributes_translate` (`table_id`, `lang`, `title`) VALUES
(2, 'ru', 'тестовая'),
(2, 'lv', 'Izstrāde'),
(4, 'ru', 'тестовая 2'),
(4, 'lv', 'tests 2'),
(2, 'en', ''),
(4, 'en', '');

-- --------------------------------------------------------

--
-- Структура таблицы `attributes_values`
--

CREATE TABLE IF NOT EXISTS `attributes_values` (
  `attributes_id` int(10) unsigned NOT NULL,
  `values_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`values_id`),
  KEY `attributes_id` (`attributes_id`),
  KEY `values_id` (`values_id`),
  KEY `attributes_id_2` (`attributes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `attributes_values`
--

INSERT INTO `attributes_values` (`attributes_id`, `values_id`) VALUES
(2, 9),
(2, 10),
(4, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `attributes_values_translate`
--

CREATE TABLE IF NOT EXISTS `attributes_values_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `attributes_values_translate`
--

INSERT INTO `attributes_values_translate` (`table_id`, `lang`, `title`) VALUES
(9, 'ru', '3кг'),
(9, 'lv', '3kg'),
(10, 'ru', '2кг'),
(10, 'lv', '2kg'),
(12, 'ru', 'картошка'),
(12, 'lv', 'kartupeli'),
(9, 'en', ''),
(10, 'en', ''),
(12, 'en', '');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_id` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  `order` int(4) unsigned NOT NULL,
  `rout_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `p_id`, `url`, `status`, `deleted`, `order`, `rout_id`, `module_id`) VALUES
(1, 15, 'razrabotka', 1, 0, 2, 55, 1),
(2, 0, 'novosti', 1, 0, 1, 56, 2),
(3, 15, 'dizajn', 1, 0, 3, 57, 1),
(4, 0, 'nashi-raboti', 1, 0, 2, 58, 2),
(5, 15, 'prodvizhenie', 1, 0, 4, 63, 1),
(7, 0, 'testovaja', 0, 1, 0, 70, 1),
(8, 0, 'cars', 1, 0, 1, 80, 3),
(9, 8, 'legkovie', 1, 0, 2, 81, 3),
(10, 8, 'gruzovie', 1, 0, 3, 82, 3),
(15, 0, 'uslugi', 1, 0, 1, 91, 1),
(18, 0, 'novaja-kategorija2fwef', 1, 0, 0, 131, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `categories_translate`
--

CREATE TABLE IF NOT EXISTS `categories_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories_translate`
--

INSERT INTO `categories_translate` (`table_id`, `lang`, `title`) VALUES
(1, 'ru', 'Разработка'),
(2, 'ru', 'Новости'),
(3, 'ru', 'Дизайн'),
(4, 'ru', 'Наши работы'),
(5, 'ru', 'Продвижение'),
(1, 'lv', 'Izstrāde'),
(2, 'lv', 'Jaunumi'),
(3, 'lv', 'Dizains'),
(4, 'lv', 'Musu darbi'),
(5, 'lv', 'Veicināšana'),
(7, 'ru', 'тестовая'),
(7, 'lv', 'šššoļļļļ'),
(8, 'ru', 'автомобили'),
(8, 'lv', 'autotransports'),
(9, 'ru', 'легковые'),
(9, 'lv', 'vieglie auto'),
(10, 'ru', 'грузовые'),
(10, 'lv', 'kravas auto'),
(15, 'ru', 'Услуги'),
(15, 'lv', 'tests'),
(1, 'en', ''),
(2, 'en', ''),
(3, 'en', ''),
(4, 'en', ''),
(5, 'en', ''),
(7, 'en', ''),
(8, 'en', ''),
(9, 'en', ''),
(10, 'en', ''),
(15, 'en', ''),
(18, 'ru', 'Новая категория2'),
(18, 'lv', 'Jauna kategorija3223');

-- --------------------------------------------------------

--
-- Структура таблицы `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('7b356c60ed446fbf1d772850ad5dece6', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36', 1405595005, 'a:7:{s:9:"user_data";s:0:"";s:8:"identity";s:15:"admin@admin.com";s:8:"username";s:13:"administrator";s:5:"email";s:15:"admin@admin.com";s:7:"user_id";s:1:"1";s:14:"old_last_login";s:10:"1405493085";s:13:"cart_contents";a:3:{s:32:"03d0fe4beae446c0f6fed09d22411e50";a:7:{s:5:"rowid";s:32:"03d0fe4beae446c0f6fed09d22411e50";s:2:"id";s:1:"5";s:3:"qty";s:1:"1";s:5:"price";s:3:".00";s:4:"name";s:1:"p";s:7:"options";a:3:{s:8:"title_ru";s:27:"третий продукт";s:8:"title_lv";s:20:"tresais produktsdddd";s:3:"img";s:3:"img";}s:8:"subtotal";d:0;}s:11:"total_items";i:1;s:10:"cart_total";d:0;}}');

-- --------------------------------------------------------

--
-- Структура таблицы `delivery`
--

CREATE TABLE IF NOT EXISTS `delivery` (
  `id` smallint(2) unsigned NOT NULL AUTO_INCREMENT,
  `price` decimal(10,2) NOT NULL,
  `show_adress` tinyint(1) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `delivery`
--

INSERT INTO `delivery` (`id`, `price`, `show_adress`, `status`) VALUES
(1, '0.00', 0, 1),
(3, '5.00', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `delivery_translate`
--

CREATE TABLE IF NOT EXISTS `delivery_translate` (
  `table_id` smallint(2) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `description` text NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `delivery_translate`
--

INSERT INTO `delivery_translate` (`table_id`, `lang`, `description`) VALUES
(1, 'ru', 'На складе'),
(1, 'lv', 'Noliktavā'),
(3, 'ru', 'По указанному адресу'),
(3, 'lv', 'Pēc noradīta adresa');

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'customers', 'customers');

-- --------------------------------------------------------

--
-- Структура таблицы `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) NOT NULL,
  `description` varchar(20) NOT NULL,
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `languages`
--

INSERT INTO `languages` (`id`, `lang`, `description`, `default`, `status`) VALUES
(1, 'ru', 'Русский', 1, 1),
(2, 'lv', 'Latviešu', 0, 1),
(6, 'en', 'English', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `categories` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `modules`
--

INSERT INTO `modules` (`id`, `name`, `title`, `categories`) VALUES
(1, 'pages', 'Страницы', 1),
(2, 'blog', 'Блог', 1),
(3, 'product', 'Магазин', 1),
(4, 'templates', 'Шаблоны', 0),
(5, 'categories', 'Категории', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `navigation`
--

CREATE TABLE IF NOT EXISTS `navigation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_id` int(10) unsigned NOT NULL,
  `rout_id` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `order` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `navigation`
--

INSERT INTO `navigation` (`id`, `p_id`, `rout_id`, `status`, `deleted`, `created`, `modified`, `order`) VALUES
(18, 0, 83, 0, 0, '2014-05-27 11:03:23', '2014-06-18 08:56:54', 3),
(19, 0, 56, 0, 0, '2014-05-27 11:04:17', '2014-06-18 08:57:02', 4),
(20, 0, 61, 0, 0, '2014-05-27 11:04:52', '2014-06-18 08:57:08', 5),
(21, 0, 58, 0, 0, '2014-05-27 11:05:39', '2014-06-18 08:57:15', 6),
(22, 0, 48, 0, 0, '2014-05-27 11:05:56', '2014-06-18 08:57:21', 7),
(23, 0, 91, 1, 1, '2014-06-02 14:55:16', '2014-06-12 07:37:47', 6),
(24, 0, 94, 1, 0, '2014-06-18 08:43:39', '0000-00-00 00:00:00', 1),
(25, 0, 95, 1, 0, '2014-06-18 08:58:58', '0000-00-00 00:00:00', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `navigation_translate`
--

CREATE TABLE IF NOT EXISTS `navigation_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  KEY `table_id` (`table_id`,`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `navigation_translate`
--

INSERT INTO `navigation_translate` (`table_id`, `lang`, `title`) VALUES
(18, 'ru', 'Компания'),
(19, 'ru', 'Новости'),
(20, 'ru', 'Услуги'),
(21, 'ru', 'Наши работы'),
(22, 'ru', 'Контакты'),
(18, 'lv', 'Kompanija'),
(19, 'lv', 'Jaunumi'),
(20, 'lv', 'Pakalpojumi'),
(21, 'lv', 'Musu darbi'),
(22, 'lv', 'Kontakti'),
(23, 'ru', 'тестовая'),
(23, 'lv', 'Sššīīīīīī'),
(18, 'en', ''),
(19, 'en', ''),
(20, 'en', ''),
(21, 'en', ''),
(22, 'en', ''),
(23, 'en', ''),
(24, 'ru', 'Магазин'),
(24, 'lv', 'Veikals'),
(25, 'ru', 'Корзина'),
(25, 'lv', 'Grozs');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `delivery` text NOT NULL,
  `delivery_date` varchar(100) NOT NULL,
  `delivery_id` smallint(2) NOT NULL,
  `payment_id` smallint(6) NOT NULL,
  `customer_deliver_name` varchar(255) NOT NULL,
  `customer_deliver_phone` varchar(25) NOT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `customer_phone` varchar(25) NOT NULL,
  `customer_street` varchar(255) NOT NULL,
  `customer_city` varchar(100) NOT NULL,
  `customer_house` varchar(15) NOT NULL,
  `customer_corp` varchar(15) NOT NULL,
  `customer_build` varchar(25) NOT NULL,
  `customer_flat` varchar(15) NOT NULL,
  `customer_office` varchar(15) NOT NULL,
  `customer_postcode` varchar(15) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `sum` double(10,2) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `new` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=76 ;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `email`, `name`, `phone`, `delivery`, `delivery_date`, `delivery_id`, `payment_id`, `customer_deliver_name`, `customer_deliver_phone`, `customer_name`, `customer_phone`, `customer_street`, `customer_city`, `customer_house`, `customer_corp`, `customer_build`, `customer_flat`, `customer_office`, `customer_postcode`, `comment`, `created`, `deleted`, `sum`, `status`, `new`) VALUES
(72, 0, 'user@user.com', 'Chuk Norris', '+(371)6666666', '', '', 1, 3, '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-06-25 13:43:21', 0, 0.00, 0, 0),
(73, 0, 'user@user.com', 'corporate magnets', '+(7)0000000', '', '', 3, 3, '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-06-26 13:46:50', 0, 0.00, 0, 1),
(74, 0, 'user@user.com', 'Chuk Norris', '+(371)6666666', '', '', 1, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-06-26 14:13:38', 0, 1050.23, 1, 1),
(75, 0, 'user@user.com', 'Chuk Norris', '+(371)6666666', '', '', 1, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-06-26 14:32:47', 0, 1050.23, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `orders_products`
--

CREATE TABLE IF NOT EXISTS `orders_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(10) unsigned NOT NULL,
  `products_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` double(10,2) NOT NULL,
  `qty` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_id` (`orders_id`,`products_id`),
  KEY `products_id` (`products_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `rout_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  KEY `module_id_2` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `url`, `created`, `modified`, `deleted`, `status`, `rout_id`, `module_id`) VALUES
(1, 'masterskaja-prirodi', '2014-05-28 12:58:59', '2014-06-06 08:49:36', 0, 1, 64, 2),
(3, 'uslugi', '2014-05-28 09:33:58', '2014-07-17 05:49:57', 0, 1, 61, 1),
(5, 'razrabotka-programmnogo-obespechenija', '2014-05-28 13:28:40', '2014-06-12 09:05:05', 0, 1, 65, 1),
(6, 'dizajn-sajtov', '2014-05-28 13:29:31', '0000-00-00 00:00:00', 0, 1, 66, 1),
(7, 'sozdanie-logotipov', '2014-05-28 13:29:47', '0000-00-00 00:00:00', 0, 1, 67, 1),
(8, 'razrabotka-veb-sajtov', '2014-05-28 13:30:05', '2014-06-04 11:13:55', 0, 1, 68, 1),
(10, 'testovaja', '2014-06-06 06:23:59', '2014-06-12 07:35:07', 0, 1, 76, 1),
(13, 'nashi-raboti-test', '2014-06-06 07:20:38', '2014-07-17 09:37:53', 0, 1, 79, 2),
(14, 'kompanija2', '2014-06-12 05:42:17', '2014-07-16 14:27:38', 0, 1, 83, 1),
(51, 'blog-1', '2014-07-17 09:23:11', '2014-07-17 09:34:10', 0, 0, 128, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `pages_categories`
--

CREATE TABLE IF NOT EXISTS `pages_categories` (
  `pages_id` int(10) unsigned NOT NULL,
  `categories_id` int(10) unsigned NOT NULL,
  KEY `pages_id` (`pages_id`,`categories_id`),
  KEY `categories_id` (`categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pages_categories`
--

INSERT INTO `pages_categories` (`pages_id`, `categories_id`) VALUES
(1, 4),
(3, 15),
(5, 1),
(6, 3),
(7, 3),
(8, 5),
(10, 15),
(13, 4),
(51, 2),
(51, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `pages_translate`
--

CREATE TABLE IF NOT EXISTS `pages_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `short_content` text NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pages_translate`
--

INSERT INTO `pages_translate` (`table_id`, `lang`, `title`, `content`, `short_content`) VALUES
(1, 'ru', 'Мастерская природы', '<p>ytjtyjtyj</p>\r\n', '<p><img alt="" src="http://localhost/aligorex/uploads/cke_uploads/2014-06/4cf496bde7a16104adadc33f99e985b4.png" style="height:300px; width:500px" /></p>\r\n'),
(1, 'lv', 'Dabas darbnica', '<p>Dabas darbnica</p>\r\n', '<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="http://localhost/aligorex/uploads/cke_uploads/2014-06/55a4be5c4ed9c5cdde8c71e24d72f342.png" style="height:300px; width:500px" /></p>\r\n'),
(3, 'ru', 'Услуги', '<p><img alt="" src="http://localhost/aligorex/uploads/cke_uploads/2014-05/5664fba1f887a35a589d3200f9c418a2.jpg" style="height:189px; width:267px" />Разработка программного обеспечения</p>\r\n\r\n<p>Мы поможем пройти все этапы разработки программного обеспечения, запустить готовое решение и обеспечить поддержку.</p>\r\n\r\n<div class="circle" style="box-sizing: border-box; margin: 10px; width: 80px; height: 80px; border-top-left-radius: 100%; border-top-right-radius: 100%; border-bottom-right-radius: 100%; border-bottom-left-radius: 100%; border: 8px solid rgb(20, 96, 163); color: black; text-align: center; font-size: 72px; overflow: hidden; line-height: 100px; font-family: ''Open Sans'', sans-serif;">\r\n<h1>1</h1>\r\n</div>\r\n\r\n<div class="circle" style="box-sizing: border-box; margin: 10px; width: 80px; height: 80px; border-top-left-radius: 100%; border-top-right-radius: 100%; border-bottom-right-radius: 100%; border-bottom-left-radius: 100%; border: 8px solid rgb(20, 96, 163); color: black; text-align: center; font-size: 72px; overflow: hidden; line-height: 100px; font-family: ''Open Sans'', sans-serif;">\r\n<h1>2</h1>\r\n</div>\r\n\r\n<div class="circle" style="box-sizing: border-box; margin: 10px; width: 80px; height: 80px; border-top-left-radius: 100%; border-top-right-radius: 100%; border-bottom-right-radius: 100%; border-bottom-left-radius: 100%; border: 8px solid rgb(20, 96, 163); color: black; text-align: center; font-size: 72px; overflow: hidden; line-height: 100px; font-family: ''Open Sans'', sans-serif;">\r\n<h1>3</h1>\r\n</div>\r\n\r\n<div class="circle" style="box-sizing: border-box; margin: 10px; width: 80px; height: 80px; border-top-left-radius: 100%; border-top-right-radius: 100%; border-bottom-right-radius: 100%; border-bottom-left-radius: 100%; border: 8px solid rgb(20, 96, 163); color: black; text-align: center; font-size: 72px; overflow: hidden; line-height: 100px; font-family: ''Open Sans'', sans-serif;">\r\n<h1>4</h1>\r\n</div>\r\n\r\n<div class="circle" style="box-sizing: border-box; margin: 10px; width: 80px; height: 80px; border-top-left-radius: 100%; border-top-right-radius: 100%; border-bottom-right-radius: 100%; border-bottom-left-radius: 100%; border: 8px solid rgb(20, 96, 163); color: black; text-align: center; font-size: 72px; overflow: hidden; line-height: 100px; font-family: ''Open Sans'', sans-serif;">\r\n<h1>5</h1>\r\n</div>\r\n', ''),
(3, 'lv', 'pakalpojumi', '<p>musu pakalpojumi</p>\r\n', ''),
(5, 'ru', 'Разработка программного обеспечения', '<p>Разработка программного обеспечения</p>\r\n', ''),
(5, 'lv', 'progarmnodrosinajums', '<p>progarmnodrosinajums</p>\r\n', ''),
(6, 'ru', 'Дизайн сайтов', 'Дизайн сайтов', ''),
(6, 'lv', 'web saitu dizains', 'web saitu dizains', ''),
(7, 'ru', 'Создание логотипов', 'Создание логотипов', ''),
(7, 'lv', 'logo zimesana', 'logo zimesana', ''),
(8, 'ru', 'разработка веб сайтов', '<p>разработка веб сайтов kontent ewfwefwef</p>\r\n', ''),
(8, 'lv', 'web saitu izstrade', '<p>web saitu izstrade kontent wfw assdfsdf dsf s</p>\r\n', ''),
(10, 'ru', 'тестовая', '<p>тестовая страница</p>\r\n', ''),
(10, 'lv', 'testesana', '<p>test lapaspuse</p>\r\n', ''),
(13, 'ru', 'Наши работы тест', '<p>блог тест</p>\r\n', '<p><img alt="" src="http://localhost/aligorex/uploads/cke_uploads/2014-06/0be99b8ebe6c7565bfd01a2674e5d282.png" style="height:300px; width:500px" /></p>\r\n'),
(13, 'lv', 'musu darbi test', '<p>musu darbi test</p>\r\n', '<p><img alt="" src="http://localhost/aligorex/uploads/cke_uploads/2014-06/5d22791bd6ee613b32385e97abd0a137.png" style="height:300px; width:500px" /></p>\r\n'),
(14, 'ru', 'Компания', '<p>Компания</p>\r\n', ''),
(14, 'lv', 'Kompanija', '<p>kompanija</p>\r\n', ''),
(51, 'ru', 'БЛОГ 1', '<p>БЛОГ 1</p>\r\n', '<p>ytttttttttttth</p>\r\n'),
(51, 'lv', 'blogs 1', '<p>blogs 1</p>\r\n', '<p>blogs 1</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` smallint(2) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `payment`
--

INSERT INTO `payment` (`id`, `status`) VALUES
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `payment_translate`
--

CREATE TABLE IF NOT EXISTS `payment_translate` (
  `table_id` smallint(2) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `description` text NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `payment_translate`
--

INSERT INTO `payment_translate` (`table_id`, `lang`, `description`) VALUES
(2, 'ru', 'PayPal'),
(2, 'lv', 'PayPal'),
(3, 'ru', 'Nordea'),
(3, 'lv', 'Nordea');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `price` double(10,2) NOT NULL,
  `discount` double(10,2) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `url`, `price`, `discount`, `status`, `deleted`, `created`, `modified`) VALUES
(3, 'pervij-produkt', 1000.00, 0.00, 1, 0, '2014-06-09 15:19:33', '2014-06-18 11:00:48'),
(4, 'vtoroj-produkt', 50.23, 0.00, 1, 0, '2014-06-18 10:55:09', '2014-06-18 10:55:13'),
(5, 'tretij-produkt', 0.00, 0.00, 0, 0, '2014-07-17 10:51:10', '2014-07-17 11:01:14');

-- --------------------------------------------------------

--
-- Структура таблицы `products_attributes_values`
--

CREATE TABLE IF NOT EXISTS `products_attributes_values` (
  `products_id` int(10) unsigned NOT NULL,
  `attributes_id` int(10) unsigned NOT NULL,
  `values_id` int(10) unsigned NOT NULL,
  KEY `products_id` (`products_id`),
  KEY `attributes_id` (`attributes_id`),
  KEY `values_id` (`values_id`),
  KEY `products_id_2` (`products_id`,`attributes_id`,`values_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products_attributes_values`
--

INSERT INTO `products_attributes_values` (`products_id`, `attributes_id`, `values_id`) VALUES
(3, 2, 9),
(3, 4, 12),
(4, 2, 9),
(4, 4, 12),
(5, 2, 9);

-- --------------------------------------------------------

--
-- Структура таблицы `products_img`
--

CREATE TABLE IF NOT EXISTS `products_img` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `img` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `order` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `products_img`
--

INSERT INTO `products_img` (`id`, `product_id`, `img`, `title`, `thumbnail`, `order`) VALUES
(4, 3, '76955-project_1.png', '', '', 0),
(5, 4, '16185-project_1.png', '', '', 0),
(6, 5, '2c390-bagazniki.jpg', '', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `products_translate`
--

CREATE TABLE IF NOT EXISTS `products_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products_translate`
--

INSERT INTO `products_translate` (`table_id`, `lang`, `title`, `content`) VALUES
(3, 'ru', 'первый продукт', '<p>первый продукт</p>\r\n'),
(3, 'lv', 'pirmais produkts', '<p>pirmais produkts</p>\r\n'),
(4, 'ru', 'второй продукт', '<p>второй продукт</p>\r\n'),
(4, 'lv', 'otrais produkts', '<p>otrais produkts</p>\r\n'),
(5, 'ru', 'третий продукт', '<p>третий продукт</p>\r\n'),
(5, 'lv', 'tresais produktsdddd', '<p>tresais produkts</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `product_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  KEY `product_id` (`product_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_category`
--

INSERT INTO `product_category` (`product_id`, `category_id`) VALUES
(3, 8),
(3, 9),
(4, 8),
(4, 9),
(4, 10),
(5, 18);

-- --------------------------------------------------------

--
-- Структура таблицы `routs`
--

CREATE TABLE IF NOT EXISTS `routs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `p_id` int(10) unsigned NOT NULL,
  `rout` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=132 ;

--
-- Дамп данных таблицы `routs`
--

INSERT INTO `routs` (`id`, `p_id`, `rout`, `title`) VALUES
(6, 0, 'pages/index', 'Страницы (главная)'),
(12, 0, 'blog/index', 'Блог (главная)'),
(48, 0, 'pages/contacts', 'Шаблон - Контакты'),
(49, 0, 'template', 'Все Шаблоны'),
(53, 49, 'pages/template/company', 'Компания'),
(54, 0, 'categories', 'Категории'),
(55, 54, 'pages/category/razrabotka', 'Разработка (Страницы)'),
(56, 54, 'blog/category/novosti', 'Новости (Блог)'),
(57, 54, 'pages/category/dizajn', 'Дизайн (Страницы)'),
(58, 54, 'blog/category/nashi-raboti', 'Наши работы (Блог)'),
(61, 6, 'pages/page/uslugi', 'Услуги'),
(63, 54, 'pages/category/prodvizhenie', 'Продвижение (Страницы)'),
(64, 12, 'blog/article/masterskaja-prirodi', 'Мастерская природы'),
(65, 6, 'pages/page/razrabotka-programmnogo-obespechenija', 'Разработка программного обеспечения'),
(66, 6, 'pages/page/dizajn-sajtov', 'Дизайн сайтов'),
(67, 6, 'pages/page/sozdanie-logotipov', 'Создание логотипов'),
(68, 6, 'pages/page/razrabotka-veb-sajtov', 'разработка веб сайтов'),
(69, 54, '404', 'тестовая (Страницы)'),
(70, 54, '404', 'тестовая (Страницы)'),
(71, 54, '404', 'qwd (Страницы)'),
(72, 6, '404', 'Компания'),
(73, 6, '404', 'Компания'),
(74, 6, '404', 'Компания'),
(75, 6, '404', 'тестовая'),
(76, 6, 'pages/page/testovaja', 'тестовая'),
(77, 6, '404', 'блог тест'),
(78, 6, '404', 'блог тест'),
(79, 12, 'blog/article/nashi-raboti-test', 'Наши работы тест'),
(80, 54, 'cars', 'автомобили (Магазин)'),
(81, 54, 'legkovie', 'легковые (Магазин)'),
(82, 54, 'gruzovie', 'грузовые (Магазин)'),
(83, 6, 'pages/page/kompanija2', 'Компания'),
(84, 54, '404', 'uslugi (Страницы)'),
(85, 54, '404', 'uslugi2 (Страницы)'),
(86, 54, '404', 'usluga2 (Страницы)'),
(87, 6, '404', 'qwdqwdqwd'),
(88, 6, '404', 'Мастерская природы'),
(89, 6, '404', 'блог тестqwdqwd'),
(90, 54, '404', 'uslugi3 (Страницы)'),
(91, 54, 'pages/category/uslugi', 'Услуги (Страницы)'),
(94, 0, 'shop/index', 'Магазин (Главная)'),
(95, 0, 'cart/index', 'Корзина (Обзор)'),
(125, 6, 'pages/page/wqdqwd', 'wqdqwd'),
(128, 12, '404', 'БЛОГ 1'),
(131, 54, 'novaja-kategorija2fwef', 'Новая категория2 (Магазин)');

-- --------------------------------------------------------

--
-- Структура таблицы `seo`
--

CREATE TABLE IF NOT EXISTS `seo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `modules_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Дамп данных таблицы `seo`
--

INSERT INTO `seo` (`id`, `modules_id`, `item_id`) VALUES
(11, 1, 14),
(12, 1, 23),
(13, 1, 24),
(14, 1, 25),
(15, 1, 3),
(16, 1, 28),
(17, 1, 29),
(18, 1, 38),
(19, 1, 39),
(24, 2, 51),
(25, 2, 13),
(27, 5, 18),
(28, 3, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `seo_translate`
--

CREATE TABLE IF NOT EXISTS `seo_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` text NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `seo_translate`
--

INSERT INTO `seo_translate` (`table_id`, `lang`, `title`, `description`, `keywords`) VALUES
(11, 'ru', 'SEOddddd', 'kkkkkkkkkk', 'SEO2'),
(11, 'lv', 'SEO', 'SEO5', ''),
(12, 'ru', 'тестовая333', 'dddddqwdqwd', 'dddddrthtrh'),
(12, 'lv', 'tests22222', 'tests22222wqdqwd', 'tests22222.ewfw,wefwef,wef,wefwef'),
(13, 'ru', 'Услуги', 'Услуги', ''),
(13, 'lv', 'Izstrāde', 'Izstrāde', 'Izstrādewefwefwe'),
(14, 'ru', 'wefwef', 'wefwef', 'wefwefwef'),
(14, 'lv', '2222', 'fffffffff', 'fewwwwwwwww'),
(15, 'ru', 'super seo', 'wefwefwef', 'dddddddddddddddddd'),
(15, 'lv', 'super seo2', 'super seo333', 'fwefwef'),
(16, 'ru', 'wefwef', 'efffffffffff', 'wefff'),
(16, 'lv', '11111111', 'eeeeeeeee', '222222222'),
(17, 'ru', 'Требуется 2', '', 'Требуется 2'),
(17, 'lv', '', '', ''),
(18, 'ru', 'ffffffffffff', '', ''),
(18, 'lv', '', '', ''),
(19, 'ru', 'Услуги', '', 'vajadzigs 10'),
(19, 'lv', '', '', ''),
(24, 'ru', 'БЛОГ 1', 'iugggggggggggg', 'nnnnnnnnnnn'),
(24, 'lv', 'blogs 1', 'blogs 1', 'blogs 1'),
(25, 'ru', 'Наши работы тест', '', ''),
(25, 'lv', '', '', ''),
(27, 'ru', 'Новая категория', 'Новая категория', '='),
(27, 'lv', 'Jauna kategorija', 'Jauna kategorija', 'Jauna kategorija'),
(28, 'ru', 'третий продукт', 'третий продукт222222', 'третий продуктqwdqwd'),
(28, 'lv', 'tresais produkts', 'tresais produkts', '');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `autoload` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `name`, `autoload`) VALUES
(1, 'site_main', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `settings_translate`
--

CREATE TABLE IF NOT EXISTS `settings_translate` (
  `table_id` int(10) unsigned NOT NULL,
  `lang` varchar(2) NOT NULL,
  `value` text NOT NULL,
  KEY `table_id` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings_translate`
--

INSERT INTO `settings_translate` (`table_id`, `lang`, `value`) VALUES
(1, 'ru', '{"site_title":"\\u041d\\u0430\\u0448 \\u0441\\u0430\\u0439\\u0442","site_email":"admin@admin.com","site_phone":"555555","site_adress":"\\u0415\\u043b\\u0433\\u0430\\u0432\\u0430","site_description":"\\u041d\\u0430\\u0448 \\u0441\\u0430\\u0439\\u0442 ","site_keywords":"\\u041d\\u0430\\u0448 \\u0441\\u0430\\u0439\\u0442,\\u041d\\u0430\\u0448 \\u0441\\u0430\\u0439\\u0442 ,\\u041d\\u0430\\u0448 \\u0441\\u0430\\u0439\\u0442 ,\\u041d\\u0430\\u0448 \\u0441\\u0430\\u0439\\u0442 "}'),
(1, 'lv', '{"site_title":"Musu saite","site_email":"admin@admin.com","site_phone":"555555","site_adress":"Jelgava","site_description":"Musu saite","site_keywords":"Musu saite,Musu saite, Musu saite, Musu saite"}');

-- --------------------------------------------------------

--
-- Структура таблицы `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `rout_id` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `templates`
--

INSERT INTO `templates` (`id`, `title`, `url`, `path`, `rout_id`, `status`) VALUES
(3, 'Компания', 'company', 'frontend/templates/company', 53, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$08$OLIe0Vwg4Y3DRjqauA.m3OYVh6NidsZndbYFvcCr9F6sb1jUPptda', '', 'admin@admin.com', '', NULL, NULL, 'NKcZpFViky3dlTsWIhABAe', 1268889823, 1405573668, 1, 'Super admin', 'admin', 'ADMIN', '0'),
(4, '127.0.0.1', 'test admin qwdqwdqwd', '$2y$08$79kn.WlBX86hkL19.m0V1utXAMG9WyHuMr1pWg0TYbcGGD6jp7PaS', NULL, 'user@user.com', NULL, NULL, NULL, NULL, 1405432040, 1405432040, 1, 'customer', 'customer', 'customer', '+(371)6666666');

-- --------------------------------------------------------

--
-- Структура таблицы `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Дамп данных таблицы `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(33, 1, 1),
(34, 1, 2),
(35, 1, 3),
(30, 4, 2);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `attributes_translate`
--
ALTER TABLE `attributes_translate`
  ADD CONSTRAINT `attributes_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `attributes_values`
--
ALTER TABLE `attributes_values`
  ADD CONSTRAINT `attributes_values_ibfk_1` FOREIGN KEY (`attributes_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `attributes_values_translate`
--
ALTER TABLE `attributes_values_translate`
  ADD CONSTRAINT `attributes_values_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `attributes_values` (`values_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `categories_translate`
--
ALTER TABLE `categories_translate`
  ADD CONSTRAINT `categories_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `delivery_translate`
--
ALTER TABLE `delivery_translate`
  ADD CONSTRAINT `delivery_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `delivery` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `navigation_translate`
--
ALTER TABLE `navigation_translate`
  ADD CONSTRAINT `navigation_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `navigation` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `orders_products`
--
ALTER TABLE `orders_products`
  ADD CONSTRAINT `orders_products_ibfk_1` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `orders_products_ibfk_2` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`);

--
-- Ограничения внешнего ключа таблицы `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`);

--
-- Ограничения внешнего ключа таблицы `pages_categories`
--
ALTER TABLE `pages_categories`
  ADD CONSTRAINT `pages_categories_ibfk_1` FOREIGN KEY (`pages_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pages_categories_ibfk_2` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `pages_translate`
--
ALTER TABLE `pages_translate`
  ADD CONSTRAINT `pages_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `payment_translate`
--
ALTER TABLE `payment_translate`
  ADD CONSTRAINT `payment_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `payment` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `products_attributes_values`
--
ALTER TABLE `products_attributes_values`
  ADD CONSTRAINT `products_attributes_values_ibfk_1` FOREIGN KEY (`attributes_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `products_attributes_values_ibfk_3` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `products_translate`
--
ALTER TABLE `products_translate`
  ADD CONSTRAINT `products_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_category`
--
ALTER TABLE `product_category`
  ADD CONSTRAINT `product_category_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `settings_translate`
--
ALTER TABLE `settings_translate`
  ADD CONSTRAINT `settings_translate_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `settings` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
