            var ru2en = {
                ru_str : "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя !?.,;':\"",
                en_str : ['a','b','v','g','d','e','jo','zh','z','i','j','k','l','m','n','o','p','r','s','t',
                'u','f','h','c','ch','sh','sh','','i','','je','ju',
                'ja','a','b','v','g','d','e','jo','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f',
                'h','c','ch','sh','sh','','i','','je','ju','ja','-','','','','','','','',''],

                translit : function(org_str) {
                    var tmp_str = [];
                    for(var i = 0, l = org_str.length; i < l; i++) {
                        var s = org_str.charAt(i), n = this.ru_str.indexOf(s);
                        if(n >= 0) { tmp_str[tmp_str.length] = this.en_str[n]; }
                        else { tmp_str[tmp_str.length] = s; }
                    }
                    return tmp_str.join("");
                    }
            }
            onload = function() {
                var t = document.getElementById("title");
                var b = document.getElementById("url");
                t.oninput = function() {
                    var s = ru2en.translit(t.value);
                    b.value = s;
                }
            }