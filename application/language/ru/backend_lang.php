<?php
$lang['attr_created']	= "Атрибут успешно создан!";



$lang['section_main']	= 'Главная';
$lang['section_users']	= 'Пользователи';
$lang['section_navigation']	= 'Навигация';
$lang['section_categories']	= 'Категории';
$lang['section_pages']	= 'Страницы';
$lang['section_templates']	= 'Шаблоны';
$lang['section_blog']	= 'Блог';
$lang['section_gallery']	= 'Галерея';
$lang['section_settings']	= 'Настройки';
$lang['section_languages']	= 'Языки';
$lang['section_site_settings']	= 'Настройки сайта';

$lang['blogs']	= ' блога';
$lang['section_site_settings']	= 'Настройки сайта';
$lang['section_site_settings']	= 'Настройки сайта';
$lang['section_site_settings']	= 'Настройки сайта';
$lang['section_site_settings']	= 'Настройки сайта';
$lang['section_site_settings']	= 'Настройки сайта';
$lang['section_site_settings']	= 'Настройки сайта';
$lang['section_site_settings']	= 'Настройки сайта';


$lang['create'] = 'Создать';
$lang['show'] = 'Отображать';
$lang['not_show'] = 'Не отображать';
$lang['shownot'] = 'Отображать / Не отображать';
$lang['url'] = 'Адрес (url)';
$lang['content'] = 'Содержимое';
$lang['short'] = 'Краткое описание';
$lang['title'] = 'Заголовок';
$lang['name'] = 'Название';
$lang['category'] = 'В категорию';
$lang['save'] = 'Сохранить';
$lang['editing'] = 'Редактирование';
$lang['edit'] = 'Редактировать';
$lang['delete'] = 'Удалить';
$lang['del_confirm'] = "'Вы действительно хотите удалить запись?'";
$lang['all_records'] = 'Все записи';
$lang['records'] = 'Записи';
$lang['no_records'] = 'Записи еще не созданы!';
$lang['display'] = 'Отображаение';
$lang['created'] = 'Создана';
$lang['edited'] = 'Редактирована';
$lang['options'] = 'Опции';
$lang['no_records'] = 'Записи еще не созданы!';
$lang['review'] = 'Обзор';
$lang['add'] = 'Добавить';
$lang['add_in'] = 'Добавление в';
$lang['find'] = 'Найти';
$lang['order'] = 'Сортировка';
$lang['order_in'] = 'Сортировка в';
$lang['admin_title'] = 'Управление | Aligorex IT Services (c)';
$lang['admin_cms'] = 'Управление сайтом';
$lang['exit'] = 'Выйти';
$lang['choose'] = 'Выберите';
$lang['code'] = 'Код';
$lang['default'] = 'Основной';
$lang['lang'] = 'Язык';
$lang['stat'] = 'Статус';
$lang['link_to'] = 'Связать';
$lang['keywords'] = 'Ключевые слова';



$lang['gallery_add'] = 'Добавление новой галереи';
$lang['gallery_pic'] = 'Изображение';
$lang['gallery_manage_pic'] = 'Управление изображениями/видео';
$lang['gallery_video_link'] = 'Ссылка на видео';
$lang['gallery_pic_del_confirm'] = "'Вы уверены что хотите удалить изображение?'";
$lang['gallery'] = 'Галерея';


$lang['settings_site_name'] = 'Название сайта';
$lang['settings_email'] = 'Почтовый адрес';
$lang['settings_phone'] = 'Телефон';
$lang['settings_address'] = 'Адрес';
$lang['settings_description'] = 'Описание сайта';
$lang['settings_keywords'] = 'Ключевые слова сайта (разделять запятой)';








?>
