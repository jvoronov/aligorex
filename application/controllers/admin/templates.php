<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Templates extends Backend_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('templates_model','templates',TRUE);
        $this->load->model('routs_model','routs',TRUE);
    }
    
    public function index(){
        $this->config->load('pagination', TRUE);
        $config = array_merge($this->config->item('pagination'), Array(
              'base_url'   => site_url('admin/templates/index/'),
              'total_rows'  => $this->templates->count_by('id'),
        ));
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links(); 
        $this->data['records'] = $this->templates->limit($config['per_page'])
                                               ->offset($this->uri->segment(4))
                                               ->order_by('id', 'desc')
                                               ->find_all();
        $this->load->view($this->tpl,$this->data);
    }
    
    public function create(){
        $this->assets->add(array('js'=>array('js/translit.js')));
        $this->form_validation->set_rules($this->templates->get_validation_rules('insert'));
        $this->form_validation->set_rules('url', 'Адрес шаблона', 'trim|required|min_length[6]|max_length[200]|callback__check_url|xss_clean');
        if ($this->form_validation->run() == TRUE)
	{
            $id = $this->_save_page();
            $this->session->set_flashdata('success', 'Шаблон успешно создан!');
            redirect('/admin/templates/edit/'.$id, 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function edit(){
        $this->templates->record_exist('id', $this->uri->segment(4));
        $this->assets->add(array('js'=>array('js/translit.js')));
        $this->data['records'] = $this->templates->find_by('id', $this->uri->segment(4));
        $this->form_validation->set_rules($this->templates->get_validation_rules('insert'));
        $this->form_validation->set_rules('url', 'Адрес шаблона', 'trim|required|min_length[6]|max_length[200]|callback__check_url_edit|xss_clean');
        if ($this->form_validation->run() == TRUE)
	{
            $this->_save_page('update',$this->uri->segment(4));
            $this->session->set_flashdata('success', 'Запись шаблона успешно отредактирована!');
            redirect(current_url(), 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function delete(){
        $rout_id = $this->templates->select('rout_id')->find($this->uri->segment(4));
        $this->routs->update($rout_id->rout_id, array('rout' => $this->config->item('error404','routs_db')));
        $this->templates->delete($this->uri->segment(4));
        redirect(base_url('admin/templates/index'),'location');
    }
    
    public function _check_url() {
        if ($this->templates->is_unique('url', $this->input->post('url'))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('_check_url', 'Данный адрес шаблона уже занят!');
            return FALSE;
        }
    }
    
    public function _check_url_edit() {
        
        $url = $this->templates->find_by('url', $this->input->post('url'));
        if(!$url) return true;
        
        if($url->id == $this->uri->segment(4))
            return true;
        $this->form_validation->set_message('_check_url_edit', 'Данный адрес шаблона уже занят!');
        return false;
    }
    
    public function _save_page($type = 'insert', $id = 0) {
        $data = $this->templates->prep_data($this->input->post());
        $rout_parent_id = $this->routs->select('id')->find_by('rout', $this->config->item('templates_main','routs_db'));
        if($this->input->post('status') == 0)
             $rout =  $this->config->item('error404','routs_db');
        else
             $rout =  $this->config->item('templates','routs_db').$this->input->post('url');
        if ($type == 'insert') {
            $data['rout_id'] = $this->routs->insert(array(
                    'rout'=>$rout,
                    'p_id'=>$rout_parent_id->id,
                    'title'=>$this->input->post('title')
                 ));
            return $this->templates->insert($data);
        } else {// Update
            $rout_id = $this->templates->select('rout_id')->find($this->uri->segment(4));
            $this->routs->update($rout_id->rout_id, array('rout' => $rout,'title'=>$this->input->post('title')));
            $this->templates->update($id, $data);
        }
    }
}