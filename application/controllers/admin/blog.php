<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends Backend_Controller {

    public $modules_data = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('pages_model', 'pages', TRUE);
        $this->load->model('routs_model', 'routs', TRUE);
        $this->load->model('modules_model', 'modules', TRUE);
        $this->load->model('categories_model', 'categories', TRUE);
        $this->load->library(array('ckeditor', 'seo_lib'));
        $this->config->load('cke', TRUE);
        $this->ckeditor->basePath = base_url() . 'assets/third_party/ckeditor/';
        $this->ckeditor->config = $this->config->item('cke');
        $this->modules_data = $this->modules->find_by('name', $this->router->class);
    }

    public function index() {
        $this->config->load('pagination', TRUE);
        $config = array_merge($this->config->item('pagination'), Array(
            'base_url' => site_url('admin/pages/index/'),
            'total_rows' => $this->pages->count_by(array('deleted' => 0, 'module_id' => $this->modules_data->id)),
        ));
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['records'] = $this->pages->where('deleted', 0)
                ->where('module_id', $this->modules_data->id)
                ->limit($config['per_page'])
                ->offset($this->uri->segment(4))
                ->order_by('ord', 'asc')
                ->find_all_t();
        $this->load->view($this->tpl, $this->data);
    }

    public function create() {
        $this->assets->add(array('js' => array('js/translit.js')));
        $this->pages->set_tranlate_field_rules($this->langs, 'tf[short_content]', 'Краткое описание блога на языке', 'required|min_length[3]');
        $this->form_validation->set_rules($this->pages->get_validation_rules('insert'));
        $this->form_validation->set_rules($this->seo_lib->get_validation_rules());
        $this->form_validation->set_rules('url', 'Адрес блога', 'trim|required|min_length[3]|max_length[200]|callback__check_url|xss_clean');
        $this->data['categories'] = $this->categories->get_nested($this->modules_data->id);
        if ($this->data['categories'])
            $this->form_validation->set_rules('mycheck[]', 'Категории', '');
        if ($this->form_validation->run() == TRUE) {
            $id = $this->_save_page();
            $this->seo_lib->save_seo($id, $this->modules_data->id, $this->input->post('seo'));
            $this->session->set_flashdata('success', 'Блог успешно создан!');
            redirect('/admin/blog/edit/' . $id, 'location');
        }
        $this->load->view($this->tpl, $this->data);
    }

    public function edit() {
        $this->assets->add(array('js' => array('js/translit.js')));
        $this->pages->record_exist('id', $this->uri->segment(4));
        $this->load->model('categories_model', 'categories', TRUE);
        $this->data['records'] = $this->pages->find_by_t('id', $this->uri->segment(4));
        $this->data['tf'] = return_assoc_array($this->pages->find_all_by_t(array('id' => $this->uri->segment(4), 'deleted' => 0)), 'lang');
        $this->data['categories'] = $this->categories->get_nested($this->modules_data->id);
        $this->pages->set_tranlate_field_rules($this->langs, 'tf[short_content]', 'Краткое описание блога на языке', 'required|min_length[3]');
        $this->form_validation->set_rules($this->pages->get_validation_rules('insert'));
        $this->form_validation->set_rules($this->seo_lib->get_validation_rules());
        $this->form_validation->set_rules('url', 'Адрес блога', 'trim|required|min_length[6]|max_length[200]|callback__check_url_edit|xss_clean');
        if ($seo = $this->seo_lib->get_records($this->uri->segment(4), $this->modules_data->id))
            $this->data['seo'] = return_assoc_array($seo, 'seo_lang');
        else
            $this->data['seo'] = null;
        if ($this->data['categories']) {
            $this->form_validation->set_rules('mycheck[]', 'Категории', '');
            $this->load->model('pages_categories_model', 'pages_categories', TRUE);
            $this->data['checked'] = $this->pages_categories->get_categories($this->uri->segment(4));
        }
        if ($this->form_validation->run() == TRUE) {
            $this->seo_lib->save_seo($this->uri->segment(4), $this->modules_data->id, $this->input->post('seo'));
            $this->_save_page('update', $this->uri->segment(4));
            $this->session->set_flashdata('success', 'Блог успешно отредактирован!');
            redirect(current_url(), 'location');
        }
        $this->load->view($this->tpl, $this->data);
    }

    public function delete() {
        $rout_id = $this->pages->select('rout_id')->find($this->uri->segment(4));
        $this->routs->update($rout_id->rout_id, array('rout' => $this->config->item('error404', 'routs_db')));
        $this->pages->delete($this->uri->segment(4));
        redirect(base_url('admin/blog/index'), 'location');
    }

    public function _check_url() {
        if ($this->pages->is_unique('url', $this->input->post('url'))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('_check_url', 'Данный адрес блога уже занят!');
            return FALSE;
        }
    }

    public function _check_url_edit() {

        $url = $this->pages->find_by('url', $this->input->post('url'));
        if (!$url)
            return true;
        if ($url->id == $this->uri->segment(4))
            return true;
        $this->form_validation->set_message('_check_url_edit', 'Данный адрес блога уже занят!');
        return false;
    }
    
    public function sort(){
        $data = null;
        $this->assets->add(array('js'=>array('third_party/jq-ui/js/jquery-ui-1.9.2.custom.min.js','js/jquery.mjs.nestedSortable.js',))); 
        if($this->input->is_ajax_request() && $this->input->post('list')){
            $this->pages->save_order();
        }elseif($this->input->is_ajax_request() && $this->input->post('category_id')){
            $records = $this->pages->pages_in_category();
           // print_r($records);exit;
            print sortable($records);
        }else{
            $data['categories'] = $this->categories->get_nested($this->modules_data->id);
            $this->load->view($this->tpl,$data);
        }
    }

    public function _save_page($type = 'insert', $id = 0) {
        $this->load->model('pages_categories_model', 'pages_categories', TRUE);
        $data = $this->pages->prep_data($this->input->post());
        $categories_data = $this->input->post('mycheck');
        $tf = $this->input->post('tf');
        $rout_title = $tf['title'][$this->langs_db->default_language];
        $rout_parent_id = $this->routs->select('id')->find_by('rout', $this->config->item('blog_main', 'routs_db'));
        if ($this->input->post('status') == 0)
            $rout = $this->config->item('error404', 'routs_db');
        else
            $rout = $this->config->item('blog', 'routs_db') . $this->input->post('url');
        if ($type == 'insert') {
            $data['rout_id'] = $this->routs->insert(array(
                'rout' => $rout,
                'p_id' => $rout_parent_id->id,
                'title' => $rout_title
            ));
            $page_id = $this->pages->insert_translate($data, $tf);
            $this->pages_categories->save_to_category($page_id, $categories_data);
            return $page_id;
        } else {
            $rout_id = $this->pages->select('rout_id')->find($this->uri->segment(4));
            $this->routs->update($rout_id->rout_id, array('rout' => $rout, 'title' => $rout_title));
            $this->pages_categories->save_to_category($this->uri->segment(4), $categories_data);
            $this->pages->update($id, $data);
            $this->pages->update_translate($tf, $id);
        }
    }
}
