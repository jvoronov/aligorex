<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends Backend_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('orders_model','orders',TRUE);
        //$this->output->enable_profiler();
    }
    
    public function index(){
        $this->config->load('pagination', TRUE);
        $this->load->helper(array('date'));
        $config = array_merge($this->config->item('pagination'), Array(
              'base_url'   => site_url('admin/orders/index/'),
              'total_rows'  => $this->orders->count_all(),
        ));
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links(); 
        $this->data['records'] = $this->orders->limit($config['per_page'])
                                               ->offset($this->uri->segment(4))
                                               ->order_by('new', 'desc')
                                               ->find_all_by(array('deleted'=>0));
        $this->load->view($this->tpl,$this->data);
    }
    
    public function details(){
        
        $this->load->view($this->tpl,$this->data);
    }
}