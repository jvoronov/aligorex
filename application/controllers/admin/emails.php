<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Emails extends Backend_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('emails_model','emails',TRUE);
        $this->load->library(array('ckeditor'));
        $this->config->load('cke', TRUE);
        $this->ckeditor->basePath = base_url() . 'assets/third_party/ckeditor/';
        $this->ckeditor->config = $this->config->item('cke');
    }

    public function index() {
        if ($this->emails->record_exist($this->uri->segment(4))) {
            $this->data['main'] = 'backend/emails/edit';
            $this->data['tf'] = return_assoc_array($this->emails->find_all_by_t(array('id' => $this->uri->segment(4))), 'lang');
            if ($this->input->post('submit')) {
                $this->form_validation->set_rules($this->emails->get_validation_rules('insert'));
                if ($this->form_validation->run() == TRUE) {
                    $this->emails->update_translate($this->input->post('tf'), $this->uri->segment(4));
                    $this->session->set_flashdata('success', 'Блог успешно создан!');
                    redirect(current_url(), 'location');
                }
            }
        } else {
            $this->data['tf'] = null;
        }
        $this->emails->find_all_t()?$this->data['tpl'] = $this->emails->find_all_t():$this->data['tpl'] = array();
        $this->load->view($this->tpl, $this->data);
    }

}
