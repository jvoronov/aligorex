<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Delivery extends Backend_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('delivery_model','delivery',TRUE);
    }
    
    public function index(){
        $this->config->load('pagination', TRUE);
        $config = array_merge($this->config->item('pagination'), Array(
              'base_url'   => site_url('admin/delivery/index/'),
              'total_rows'  => $this->delivery->count_all(),
        ));
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links(); 
        $this->data['records'] = $this->delivery->limit($config['per_page'])
                                               ->offset($this->uri->segment(4))
                                               ->order_by('id', 'desc')
                                               ->find_all_t();
        $this->load->view($this->tpl,$this->data);
    }
    
    public function create() {
        $this->delivery->set_tranlate_field_rules($this->langs, 'description', 'Адрес доставки на языке', 'required|min_length[3]|max_length[300]|xss_clean');
        $this->form_validation->set_rules($this->delivery->get_validation_rules('insert'));
        if ($this->form_validation->run() == TRUE)
	{
            $id = $this->_save();
            $this->session->set_flashdata('success', 'Доставка успешно создана!');
            redirect('/admin/delivery/edit/'.$id, 'location');
	}
        $this->load->view($this->tpl, $this->data);
    }

    public function edit(){
        $this->data['records'] = $this->delivery->find_by_t('id', $this->uri->segment(4));
        $this->data['translated_fields'] = return_assoc_array($this->delivery->find_all_by_t( array('id'=>$this->uri->segment(4))),'lang');
        $this->delivery->set_tranlate_field_rules($this->langs, 'description', 'Адрес доставки на языке', 'required|min_length[3]|max_length[300]|xss_clean');
        $this->form_validation->set_rules($this->delivery->get_validation_rules('insert'));
        if ($this->form_validation->run() == TRUE)
	{
            $this->_save('update',$this->uri->segment(4));
            $this->session->set_flashdata('success', 'Доставка успешно отредактирована!');
            redirect(current_url(), 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function delete(){
        $this->delivery->delete($this->uri->segment(4));
        redirect(base_url('admin/delivery/index'),'location');
    }
    
    public function _save($type = 'insert', $id = 0) {
        $data = $this->delivery->prep_data($this->input->post());
        $description_data = $this->input->post('description');
        if ($type == 'insert') {
            $return_id = $this->delivery->insert($data);
            $this->delivery->insert_translate($return_id, $description_data, 'description');
            return $return_id;
        } else {
            $this->delivery->update($id, $data);
            $this->delivery->update_translate($id, $description_data, 'description');
        }
    }

}