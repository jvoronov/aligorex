<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File_upload extends Backend_Controller {
    function __construct()
    {
        parent::__construct();
    }
    
    
    function ckupload()
    {
        $subdir = date("Y-m");
        $dir = './uploads/cke_uploads/'.$subdir.'/';
        
        if(is_dir($dir) == FALSE)
        {
            mkdir($dir);
        }
        $callback = $_GET['CKEditorFuncNum'];
        
        $config['upload_path'] = $dir;
	$config['allowed_types'] = 'gif|jpg|png|jpeg';
	$config['max_size']	= '0';
	$config['max_width']  = '0';
	$config['max_height']  = '0';
        $config['encrypt_name'] = TRUE;

	$this->load->library('upload', $config);
        if( $this->upload->do_upload('upload') )
        {
            $image = $this->upload->data();
            $file_name = $image['file_name'];
            $http_path = base_url().'uploads/cke_uploads/'.$subdir.'/'.$file_name;
            $error = '';
        }
        else
        {
            $error =  $this->upload->display_errors();
            $http_path = '';
        }
        echo "<script type='text/javascript'>// <![CDATA[
        window.parent.CKEDITOR.tools.callFunction(".$callback.",  \"".$http_path."\", \"".$error."\" );
        // ]]></script>";
	;
    }
   
}