<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Languages extends Backend_Controller{
    
    
    public function __construct() {
        parent::__construct();
         $this->load->model('languages_model','languages',TRUE);
    }
    
    public function index(){
        $this->data['records'] = $this->languages->order_by('default','desc')->find_all();
        $this->load->view($this->tpl,$this->data);
    }
    
    public function create(){
       
        $this->form_validation->set_rules($this->languages->get_validation_rules('insert'));
        if ($this->form_validation->run() == TRUE)
	{
            $id = $this->_save();
            $this->session->set_flashdata('success', 'Язык успешно создан!');
            redirect('/admin/languages/edit/'.$id, 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function edit(){
        $this->data['records'] = $this->languages->find_by('id', $this->uri->segment(4));
        $this->form_validation->set_rules($this->languages->get_validation_rules('insert'));
        if ($this->form_validation->run() == TRUE)
	{
            $this->_save('update',$this->uri->segment(4));
            $this->session->set_flashdata('success', 'Язык успешно отредактирован!');
            redirect(current_url(), 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function delete(){
        
    }
    
    public function _save($type = 'insert', $id = 0) {
        $data = $this->languages->prep_data($this->input->post());
        if ($type == 'insert') {
             $this->languages->insert_new_lang_fields($data['lang']);
             return $this->languages->insert($data);
        } else {// Update
            if($data['default']){
                $this->languages->set_default_lang($id);
                $this->languages->update($id, $data);
                 $this->languages->cache_routes();
            }else{
                $this->languages->update($id, $data);
                 $this->languages->cache_routes();
            }
        }
    }
}