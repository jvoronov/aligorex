<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends Backend_Controller {

    protected $_module_id = 5;

    public function __construct() {
        parent::__construct();
        $this->load->model('gallery_model', 'gallery', TRUE);
         $this->load->model('gallery_img_model', 'gallery_img', TRUE);
        $this->load->library(array('seo_lib'));
    }

    public function index() {
        $this->data['records'] = $this->gallery->find_all_t();
        $this->load->view($this->tpl, $this->data);
    }

    public function create() {
        $this->assets->add(array('js' => array('js/translit.js')));
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules($this->seo_lib->get_validation_rules());
            $this->form_validation->set_rules($this->gallery->get_validation_rules('insert'));
            $this->form_validation->set_rules('url', 'Адрес галереи', 'required|min_length[2]|max_length[50]|callback__check_url|xss_clean');
            if (!empty($_FILES['file']['name'])) {
                $this->form_validation->set_rules('file', 'Изображение', 'callback__upload_img|xss_clean');
            }
            if ($this->form_validation->run() == TRUE) {
                $id = $this->_save();
                $this->seo_lib->save_seo($id, $this->_module_id, $this->input->post('seo'));
                $this->session->set_flashdata('success', 'Галерея успешно создана!');
                redirect('admin/gallery/index', 'location');
            }
        }
        $this->load->view($this->tpl, $this->data);
    }

    public function edit($id) {
        $data = return_assoc_array($this->gallery->find_all_by_t(array('id' => $this->uri->segment(4))), 'lang');
        if (!$id || !$data)
            redirect('admin/gallery/index');
        if ($seo = $this->seo_lib->get_records($id, $this->_module_id))
            $this->data['seo'] = return_assoc_array($seo, 'seo_lang');
        else
            $this->data['seo'] = null;
        $this->data['tf'] = $data;
        $this->assets->add(array('js' => array('js/translit.js')));
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules($this->seo_lib->get_validation_rules());
            $this->form_validation->set_rules($this->gallery->get_validation_rules('insert'));
            $this->form_validation->set_rules('url', 'Адрес галереи', 'required|min_length[2]|max_length[50]|callback__check_url_edit|xss_clean');
            if (!empty($_FILES['file']['name'])) {
                $this->form_validation->set_rules('file', 'Изображение', 'callback__upload_img|xss_clean');
            }
            if ($this->form_validation->run() == TRUE) {
                $this->seo_lib->save_seo($this->uri->segment(4), $this->_module_id, $this->input->post('seo'));
                $this->_save('update', $this->uri->segment(4));
                $this->session->set_flashdata('success', 'Галерея успешно отредактирована!');
                redirect(current_url(), 'location');
            }
        }
        $this->load->view($this->tpl, $this->data);
    }

    public function order() {
        $this->data['sortable'] = TRUE;
        $this->assets->add(array('js' => array('third_party/jq-ui/js/jquery-ui-1.9.2.custom.min.js', 'js/jquery.mjs.nestedSortable.js',)));
        $this->load->view($this->tpl, $this->data);
    }

    public function order_ajax() {
        if (isset($_POST['sortable'])) {
            $this->gallery->save_order($_POST['sortable']);
        }
        $this->data['records'] = $this->gallery->get_nested();
        $this->load->view('backend/gallery/order_ajax', $this->data);
    }

    public function delete() {
        $this->load->model('routs_model', 'routs', TRUE);
        $rout_id = $this->gallery->select('rout_id')->find($this->uri->segment(4));
        $this->routs->delete($rout_id->rout_id);
        $this->gallery->delete($this->uri->segment(4));
        $this->session->set_flashdata('error', 'Галерея удалена');
        redirect(base_url('admin/gallery/index'), 'location');
    }
    
    public function edit_img($id) {
        $this->assets->add(array('js' => array('js/ajax_file_upload.js','third_party/jq-ui/js/jquery-ui-1.9.2.custom.min.js', 'js/jquery.mjs.nestedSortable.js',)));
        $data = $this->gallery->select('id,title')->find_by_t('id',$id);
        if(!$id || $id!=$data->id)
             redirect('admin/gallery/index');
        $this->data['records'] = $data;
        $this->load->view($this->tpl, $this->data);
    }

    protected function _save($type = 'insert', $id = 0) {
        $this->load->model('routs_model', 'routs', TRUE);
        $data = $this->gallery->prep_data($this->input->post());
        $tf = $this->input->post('tf');
        $rout_title = $tf['title'][$this->langs_db->default_language];
        $rout_parent_id = $this->routs->select('id')->find_by('rout', $this->config->item('gallery_main', 'routs_db'));
        if ($this->input->post('status') == 0)
            $rout = $this->config->item('error404', 'routs_db');
        else
            $rout = $this->config->item('gallery', 'routs_db') . $this->input->post('url');
        if ($type == 'insert') {
            $data['rout_id'] = $this->routs->insert(array(
                'rout' => $rout,
                'p_id' => $rout_parent_id->id,
                'title' => $rout_title
            ));
            $page_id = $this->gallery->insert_translate($data, $tf);
            return $page_id;
        } else {
            $rout_id = $this->gallery->select('rout_id')->find($this->uri->segment(4));
            $this->routs->update($rout_id->rout_id, array('rout' => $rout, 'title' => $rout_title));
            $this->gallery->update($id, $data);
            $this->gallery->update_translate($tf, $id);
        }
    }

    public function _check_url() {
        if ($this->gallery->is_unique('url', $this->input->post('url'))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('_check_url', 'Данный адрес галереи уже занят!');
            return FALSE;
        }
    }

    public function _upload_img() {
        $dir = 'uploads/categories_img/';
        $config['upload_path'] = $dir;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '0';
        $config['max_height'] = '0';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
        $this->load->library('image_lib', $config);
        if ($this->upload->do_upload('file')) {
            $_POST['gallery_img'] = $this->upload->file_name;
            return TRUE;
        } else {
            $this->form_validation->set_message('_upload_img', $this->upload->display_errors('', ''));
            return FALSE;
        }
    }

    public function _check_url_edit() {

        $url = $this->gallery->find_by('url', $this->input->post('url'));
        if (!$url)
            return true;
        if ($url->id == $this->uri->segment(4))
            return true;
        $this->form_validation->set_message('_check_url_edit', 'Данный адрес галереи уже занят!');
        return false;
    }
    
    public function ajax_upload($gallery_id) {
        $status = "";
        $msg = "";
        $file_element_name = 'userfile';

        if ($status != "error") {
            $config['upload_path'] = './uploads/gallery/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config2['new_image'] = $this->upload->upload_path;
		$config2['maintain_ratio'] = TRUE;
		$config2['width'] = 230;
		$config2['height'] = 120;
                $config2['create_thumb'] = TRUE;
                $config2['thumb_marker'] = '_thumb';
		$this->load->library('image_lib', $config2);
                $this->image_lib->resize();
                $file_id = $this->gallery->save_image($gallery_id,$this->upload->file_name);
                if ($file_id) {
                    $status = "success";
                    $msg = "Изображение успешо загружено";
                } else {
                    unlink($this->upload->full_path);
                    $status = "error";
                    $msg = "Проблемы с сохранением в базу данных";
                }
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }
    
    public function images_control($gallery_id) {
        $this->data['records'] = $this->gallery->get_images($gallery_id);
        $this->load->view('backend/gallery/images_control', $this->data);
    }
    
    public function delete_image($id) {
        $this->gallery->delete_image($id);
    }
    
    public function img_order() {
        $this->gallery->update_image_order($this->input->post('list'));
    }
    
    public function update_status($id,$status) {
        $this->gallery->update_image_status($id,$status);
    }
    
    public function add_link() {
        $this->form_validation->set_rules($this->gallery_img->get_validation_rules('insert'));
        $this->form_validation->set_error_delimiters('','');
        if ($this->form_validation->run() == TRUE) {
            $this->gallery->save_link($this->input->post());
            $msg = 'Ссылка на видео добавлена';$error = 0;
        }else{
            $msg = validation_errors();$error = 1;
        }
        header('Content-Type: application/json');
        echo json_encode(array('msg' => $msg,'error'=>$error));exit;
       
    }
    
    public function _check_youtube() {
        if (youtube_data($this->input->post('img'))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('_check_youtube', 'Не правильная ссылка');
            return FALSE;
        }
    }

}
