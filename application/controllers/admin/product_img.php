<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_img extends Backend_Controller{
    
    public $product;
    
    public function __construct() {
        parent::__construct();
        if((int)$this->uri->segment(4))
            $this->product = $this->uri->segment(4);
        $this->load->model('products_img_model','product_img',TRUE);
    }
    
    public function edit() {
        if ($this->input->post('uload_img')) {
            $this->form_validation->set_rules('title', 'описание изображения', 'trim|min_length[3]|max_length[100]');
            if ($this->form_validation->run() == TRUE) {
                $dir = './uploads/product_img/' . $this->product . '/';
                $thumbnail = $dir . '/thumbnail/';
                $big_thumb = $dir . '/big_thumb/';
                if (!is_dir($dir))
                    mkdir($dir);
                if (!is_dir($thumbnail))
                    mkdir($thumbnail);
                if (!is_dir($big_thumb))
                    mkdir($big_thumb);
                $this->config->load('product_upload_img', TRUE);
                $config = $this->config->item('product_upload_img');
                $config['upload_path'] = $dir;
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('userfile')) {
                    $this->config->load('gallery_thumbnail', TRUE);
                    $config = $this->config->item('gallery_thumbnail');
                    $config['source_image'] = $this->upload->upload_path . $this->upload->file_name;
                    $config['new_image'] = $thumbnail;
                    $this->load->library('image_lib', $config);
                    if ($this->image_lib->resize()) {
                        $config['source_image'] = $this->upload->upload_path . $this->upload->file_name;
                        $config['new_image'] = $this->upload->upload_path . 'big_thumb/' . $this->upload->file_name;
                        $config['width'] = '380';
                        $config['height'] = '380';
                        $config['master_dim'] = 'auto';
                        $this->image_lib->initialize($config);
                        if ($this->image_lib->resize()) {
                            $data = array(
                                'product_id' => $this->product,
                                'title' => $this->input->post('title'),
                                'img' => $dir . 'big_thumb/' . $this->upload->file_name,
                                'thumbnail' => $thumbnail . $this->upload->file_name,
                            );
                            $this->product_img->insert($data);
                            $this->session->set_flashdata('success', 'Изображение добавлено');
                            redirect(current_url(), 'location');
                        } else {
                            $this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
                            redirect(current_url(), 'location');
                        }
                    } else {
                        $this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
                        redirect(current_url(), 'location');
                    }
                } else {
                    $this->session->set_flashdata('errors', $this->upload->display_errors('', ''));
                    redirect(current_url(), 'location');
                }
            }
        }

        if($this->input->post('apply'))
        {
           if($this->input->post('mycheck'))
                $this->product_img->delete_img($this->input->post('mycheck'));
           $this->product_img->update_img_title($this->input->post('title'));
           $this->session->set_flashdata('success', 'Изменения применены');
           redirect(current_url(), 'location');
        }
        $this->data['images'] = $this->product_img->where('product_id', $this->product)->find_all();
        $this->load->view($this->tpl,$this->data);
    }
    
    public function image_crud() {
        $this->load->library('image_CRUD');
        $image_crud = new image_CRUD();

        $image_crud->set_primary_key_field('id');
        $image_crud->set_url_field('img');
        $image_crud->set_title_field('title');
        $image_crud->set_table('products_img');
        $image_crud->set_ordering_field('order');
        $image_crud->set_relation_field('product_id');
        $image_crud->set_image_path('uploads/product_img');

        $this->data['output'] = $image_crud->render();
        $this->load->view('backend/tpl', $this->data);
    }
}