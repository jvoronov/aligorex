<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attributes extends Backend_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('attributes_model','attributes',TRUE);
        $this->load->model('attributes_values_model','attributes_values',TRUE);
    }
    
    public function index(){
        $this->config->load('pagination', TRUE);
        $config = array_merge($this->config->item('pagination'), Array(
              'base_url'   => site_url('admin/attributes/index/'),
              'total_rows'  => $this->attributes->count_all(),
        ));
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links(); 
        $this->data['records'] = $this->attributes->limit($config['per_page'])
                                               ->offset($this->uri->segment(4))
                                               ->order_by('order', 'asc')
                                               ->find_all_t();
        if($this->data['records']){
            foreach ($this->data['records'] as $k => $value) {
                $this->data['records'][$k]->values = $this->attributes_values->find_all_values($value->id);
            }
        }
        $this->load->view($this->tpl,$this->data);
    }
    
    public function attribute_create(){
        $this->attributes->set_tranlate_field_rules($this->langs,'title', 'название атрибута', 'trim|required|min_length[3]|max_length[200]|_check_attribute_title|xss_clean', 'required');
        $this->form_validation->set_rules($this->attributes->get_validation_rules('insert'));
        if ($this->form_validation->run() == TRUE){
           if(!empty($_FILES['file']['name'])){
               $this->_upload_img(); 
           }
           $id = $this->_save_attribute();
           $this->session->set_flashdata('success', 'Атрибут успешно создан!');
           redirect('/admin/attributes/attribute_edit/'.$id, 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function _upload_img() {
            $dir = 'uploads/categories_img/';
            $config['upload_path'] = $dir;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']	= '100';
            $config['max_width']  = '100';
            $config['encrypt_name'] = TRUE;
            
            $this->load->library('upload', $config);
            $this->load->library('image_lib', $config);
            if ($this->upload->do_upload('file')) {
                 $_POST['img'] = $this->upload->file_name;
        } else {
            $this->session->set_flashdata('error', $this->upload->display_errors('', ''));
            redirect(current_url(), 'location');
        }
    }

    public function attribute_edit() {
        $this->attributes->record_exist('id', $this->uri->segment(4));
        $this->data['translated_fields'] = return_assoc_array($this->attributes->find_all_by_t(array('id' => $this->uri->segment(4))), 'lang');
        $this->attributes->set_tranlate_field_rules($this->langs, 'title', 'название атрибута', 'trim|required|min_length[3]|max_length[200]|_check_attribute_title|xss_clean', 'required');
        $this->form_validation->set_rules($this->attributes->get_validation_rules('insert'));

        if ($this->form_validation->run() == TRUE) {
            if(!empty($_FILES['file']['name'])){
               $this->_upload_img(); 
            }
            $this->_save_attribute('update', $this->uri->segment(4));
            $this->session->set_flashdata('success', 'Атрибут успешно отредактирован!');
            redirect(current_url(), 'location');
        }
        $this->data['records'] = $this->attributes->find_by_t('id', $this->uri->segment(4));
        $this->load->view($this->tpl, $this->data);
    }

    public function attribute_delete(){
        $this->attributes->record_exist('id', $this->uri->segment(4));
        $this->attributes->delete($this->uri->segment(4));
        $this->session->set_flashdata('error', 'Значение удалено!');
        redirect($_SERVER['HTTP_REFERER'], 'location');
    }
    
    public function attribute_order(){
        $this->data['sortable'] = TRUE;
        $this->assets->add(array('js'=>array('third_party/jq-ui/js/jquery-ui-1.9.2.custom.min.js','js/jquery.mjs.nestedSortable.js',)));
        $this->load->view($this->tpl,$this->data);
    }
    
    public function attribute_order_ajax() {
        if (isset($_POST['sortable'])) {
            $this->attributes->save_order($_POST['sortable']);
        }
        $this->data['records'] = $this->attributes->as_array()->order_by('order', 'asc')->find_all_t();
        $this->load->view('backend/attributes/attribute_order_ajax', $this->data);
    }

    public function values(){
        $this->attributes->record_exist('id', $this->uri->segment(4));
        $this->data['attribute'] = $this->attributes->find_by_t('id', $this->uri->segment(4));
        $this->data['records'] = $this->attributes_values->find_all_values($this->uri->segment(4));
        $this->attributes_values->set_tranlate_field_rules($this->langs,'title', 'название значения', 'trim|required|min_length[3]|max_length[200]|xss_clean');
        $this->form_validation->set_rules($this->attributes_values->get_validation_rules('insert'));
        if ($this->form_validation->run() == TRUE)
	{
            $this->attributes_values->save_attributes_values($this->uri->segment(4),  $this->input->post('title'));
            $this->session->set_flashdata('success', 'Значение добавлено!');
            redirect(current_url(), 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function values_delete(){
        $this->attributes_values->record_exist('values_id', $this->uri->segment(4));
        $this->attributes_values->delete($this->uri->segment(4));
        $this->session->set_flashdata('error', 'Значение удалено!');
        redirect($_SERVER['HTTP_REFERER'], 'location');
    }
    
    public function values_edit(){
        $this->attributes_values->record_exist('values_id', $this->uri->segment(4));
        $this->attributes_values->set_tranlate_field_rules($this->langs,'title', 'название значения', 'trim|required|min_length[3]|max_length[200]|xss_clean');
        $this->form_validation->set_rules($this->attributes_values->get_validation_rules('insert'));
        if ($this->form_validation->run() == TRUE)
	{
            $this->attributes_values->update_translate($this->uri->segment(4),$this->input->post('title'),'title');
            $this->session->set_flashdata('success', 'Значение обновлено!');
            redirect(current_url(), 'location');
	}
        //var_dump(return_assoc_array($this->attributes_values->find_all_by_t( array('values_id'=>$this->uri->segment(4))),'lang'));exit;
        $this->data['translated_fields'] =  return_assoc_array($this->attributes_values->find_all_by_t( array('values_id'=>$this->uri->segment(4))),'lang');
        $this->data['attribute'] = $this->attributes_values->find_by('values_id', $this->uri->segment(4));
        $this->load->view($this->tpl,$this->data);
    }
    
    public function _check_value_title(){
        $this->load->model('values_model','values',TRUE);
         if ($this->values->is_unique('title', $this->input->post('title'))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('_check_value_title', 'Значение уже существует!');
            return FALSE;
        }
    }
    
    public function _check_attribute_title(){
         if ($this->attributes->is_unique_t('title', $this->input->post('title'))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('_check_attribute_title', 'Атрибут уже существует!');
            return FALSE;
        }
    }
    
     public function _check_value_edit() {
        
        $url = $this->values->find_by('title', $this->input->post('title'));
        if(!$url) return true;
        
        if($url->id == $this->uri->segment(4))
            return true;
        $this->form_validation->set_message('_check_value_edit', 'Данное значение уже существует!');
        return false;
    }
    
    public function _check_attribute_edit() {
        
        $url = $this->attributes->find_by('title', $this->input->post('title'));
        if(!$url) return true;
        
        if($url->id == $this->uri->segment(4))
            return true;
        $this->form_validation->set_message('_check_attribute_edit', 'Данный атрибут уже существует!');
        return false;
    }
    
    public function _save_attribute($type = 'insert', $id = 0){
        $attributes_data = $this->attributes->prep_data($this->input->post());;
        $post_title = $this->input->post('title');
        if ($type == 'insert') {
            $attribute_id = $this->attributes->insert($attributes_data);
            $this->attributes->insert_translate($attribute_id,$post_title,'title');
            return $attribute_id;
        } else {// Update
            $this->attributes->update($id, $attributes_data);
            $this->attributes->update_translate($id, $post_title,'title');
        }
    }
    
    public function attributes_product(){
        $this->load->model('products_model','product',TRUE);
        $this->product->record_exist('id', $this->uri->segment(4));
        $this->load->model('products_attributes_values_model','pav',TRUE);
        $this->assets->add(array('css'=>array('css/bootstrap-multiselect.css'),'js'=>array('js/bootstrap-multiselect.js')));
        if($this->input->post('attribute')){
            $array = array();
             foreach ($this->input->post('attribute') as $key => $value) {
                 foreach ($value as $k =>$v){
                    $array[$key]['attributes_id'] = $k;
                    $array[$key]['values_id'] = $v;
                    $array[$key]['products_id'] = $this->uri->segment(4);
                 }
             }
            $this->pav->insert_all($this->uri->segment(4),$array);
            $this->session->set_flashdata('success', 'Категория успешно отредактирована!');
            redirect(current_url(), 'location');
        }
        $this->data['records'] = $this->attributes->find_all_t();
        foreach ($this->data['records'] as $k => $value) {
            $this->data['records'][$k]->values = $this->attributes_values->find_all_by_t( array('attributes_id'=>$value->id,'lang'=>'ru'));
            //var_dump( $this->data['records'][$k]->values);
        }
        
        $this->data['selected_values'] = $this->pav->get_selected_values($this->uri->segment(4));
        $this->load->view($this->tpl,$this->data);
    }
}