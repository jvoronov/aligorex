<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends Backend_Controller{
    //store array of all module data
    public $modules_data = array();
    
    public function __construct() {
        parent::__construct();
        $this->load->model('categories_model','categories',TRUE);
        $this->load->model('routs_model','routs',TRUE);
        $this->load->model('modules_model','modules',TRUE);
        $this->load->library(array('seo_lib'));
        //array of modules data need in all views and controller 
        $this->modules_data = return_assoc_array($this->modules->find_all_by('categories', 1),'name');
        //$this->output->enable_profiler();
    }
    
    public function index($module = 'pages'){
        //получаем данные модуля
        $this->data['module_data'] = $this->modules_data[$module];
        $this->config->load('pagination', TRUE);
        $config = array_merge($this->config->item('pagination'), Array(
              'base_url'   => site_url('admin/categories/index/'),
              'total_rows'  => $this->categories->count_by(array('deleted'=> 0,'module_id'=>$this->modules_data[$module]->id)),
        ));
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links(); 
        $this->data['records'] = $this->categories->where('deleted', 0)
                                                ->where('module_id', $this->modules_data[$module]->id)
                                                ->limit($config['per_page'])
                                                ->offset($this->uri->segment(4))
                                                ->order_by('order', 'asc')
                                                ->find_all_t();
        $this->load->view($this->tpl,$this->data);
    }
    
    public function create($module = 'pages'){
        //получаем данные модуля
        $this->data['module_data'] = $this->modules_data[$module];
        $this->assets->add(array('js'=>array('js/translit.js')));
        $this->form_validation->set_rules($this->categories->get_validation_rules('insert'));
       // $this->form_validation->set_rules($this->seo_lib->get_validation_rules());
        $this->form_validation->set_rules('url', 'Адрес категории', 'trim|required|min_length[3]|max_length[200]|callback__check_url|xss_clean');
        if ($this->form_validation->run() == TRUE){
            if(!empty($_FILES['file']['name'])){
               $this->_upload_img(); 
            }
            $id = $this->_save_cat($this->data['module_data']);
            $this->seo_lib->save_seo($id,5,$this->input->post('seo'));
            $this->session->set_flashdata('success', 'Категория успешно создана!');
            redirect('/admin/categories/edit/'.$this->data['module_data']->name.'/'.$id, 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function edit($module = 'pages',$cat_id){
        $this->data['module_data'] = $this->modules_data[$module];
        $this->assets->add(array('js'=>array('js/translit.js')));
        $this->categories->record_exist('id', $this->uri->segment(5));
        $this->data['records'] = $this->categories->find_by_t('id', $this->uri->segment(5));
        $this->data['tf'] = return_assoc_array($this->categories->find_all_by_t( array('id'=>$this->uri->segment(5), 'deleted'=>0)),'lang');
        
        $this->data['seo'] =  $this->seo_lib->get_records($this->uri->segment(5),5)? return_assoc_array($this->seo_lib->get_records($this->uri->segment(5),5),'seo_lang'):array();
        
        $this->form_validation->set_rules($this->categories->get_validation_rules('insert'));
        //$this->form_validation->set_rules($this->seo_lib->get_validation_rules());
        $this->form_validation->set_rules('url', 'Адрес категории', 'trim|required|min_length[3]|max_length[200]|callback__check_url_edit|xss_clean');
        if ($this->form_validation->run() == TRUE){
            if(!empty($_FILES['file']['name'])){
               $this->_upload_img(); 
            }
            $this->_save_cat($this->data['module_data'],'update',$this->uri->segment(5));
            $this->session->set_flashdata('success', 'Категория успешно отредактирована!');
            redirect(current_url(), 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function order($module = 'pages'){
        $this->data['module_data'] = $this->modules->find_by('name', $module);
        $this->data['sortable'] = TRUE;
        $this->assets->add(array('js'=>array('third_party/jq-ui/js/jquery-ui-1.9.2.custom.min.js','js/jquery.mjs.nestedSortable.js',))); 
        $this->load->view($this->tpl,$this->data);
    }
    
    public function order_ajax($module_name) {
        if (isset($_POST['sortable'])) {
            $this->categories->save_order($_POST['sortable']);
        }
        $this->data['module_data'] = $this->modules->find_by('name', $module_name);
        $this->data['pages'] = $this->categories->get_nested($this->data['module_data']->id);
        $this->load->view('backend/categories/order_ajax', $this->data);
    }
    
    public function delete(){
        $rout_id = $this->categories->select('rout_id')->find($this->uri->segment(5));
        $this->routs->update($rout_id->rout_id, array('rout' => $this->config->item('error404','routs_db')));
        $this->categories->delete($this->uri->segment(5));
        $this->session->set_flashdata('error', 'Категория удалена!');
        redirect(base_url('admin/categories/index/'.$this->uri->segment(4)),'location');
    }
    
    public function _save_cat($module_data,$type = 'insert', $id = 0) {
        $data = $this->categories->prep_data($this->input->post());
        $rout_parent_id = $this->routs->select('id')->find_by('rout', $this->config->item('categories_main','routs_db'));
        if($this->input->post('status') == 0)
             $rout =  $this->config->item('error404','routs_db');
        else
             $rout =  $this->config->item($module_data->name.'_categories','routs_db').$this->input->post('url');
        $tf = $this->input->post('tf');
        $rout_title = $tf['title'][$this->site_lang].' ('.$module_data->title.')';
        if ($type == 'insert') {
            $data['rout_id'] = $this->routs->insert(array(
                    'rout'=>$rout,
                    'p_id'=>$rout_parent_id->id,
                    'title'=>$rout_title
                 ));
            $page_id = $this->categories->insert_translate($data,$tf);
            return $page_id;
        } else {// Update
            $rout_id = $this->categories->select('rout_id')->find($id);//id ?
            $this->routs->update($rout_id->rout_id, array('rout' => $rout,'title'=>$rout_title));
            $this->categories->update($id, $data);
            $this->categories->update_translate($tf,$id);
             $this->seo_lib->save_seo($this->uri->segment(5),5,$this->input->post('seo'));
        }
    }
    
     public function _check_url() {
        if ($this->categories->is_unique('url', $this->input->post('url'))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('_check_url', 'Данный адрес категории уже занят!');
            return FALSE;
        }
    }
    
     public function _check_url_edit() {
        
        $url = $this->categories->find_by('url', $this->input->post('url'));
        if(!$url) return true;
        
        if($url->id == $this->uri->segment(5))
            return true;
        $this->form_validation->set_message('_check_url_edit', 'Данный адрес категории уже занят!');
        return false;
    }
    
    public function product_category(){
        $this->load->model('product_category_model','product_category',TRUE);
        if( $this->input->post('add')){
            $this->product_category->save_product_category($this->uri->segment(4),$this->input->post('mycheck'));
            $this->session->set_flashdata('success', 'Категория успешно отредактирована!');
            redirect(current_url(), 'location');
        }
        $module = 'product';
        $this->data['checked'] = $this->product_category->get_product_categories($this->uri->segment(4));
        $this->data['records'] = $this->categories->get_nested($this->modules_data[$module]->id);
        $this->load->view($this->tpl,$this->data);
    }
    
    public function _upload_img() {
            $dir = 'uploads/categories_img/';
            $config['upload_path'] = $dir;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']	= '0';
            $config['max_width']  = '0';
            $config['encrypt_name'] = TRUE;
            
            $this->load->library('upload', $config);
            $this->load->library('image_lib', $config);
            if ($this->upload->do_upload('file')) {
                 $_POST['img'] = $this->upload->file_name;
        } else {
            $this->session->set_flashdata('error', $this->upload->display_errors('', ''));
            redirect(current_url(), 'location');
        }
    }
}