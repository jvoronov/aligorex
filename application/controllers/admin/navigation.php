<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Navigation extends Backend_Controller{
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('navigation_model','navigation',TRUE);
        $this->load->model('routs_model','routs',TRUE);
        //$this->output->enable_profiler();
    }
    
    public function menus() {
        if($this->input->post('submit') && $this->input->post('name')){
            $this->navigation->new_menu($this->input->post('name'));
            $this->session->set_flashdata('success', 'Новая навигация создана!');
            redirect(current_url(), 'location');
        }
        $this->data['records'] = $this->navigation->all_menus();
        $this->load->view($this->tpl,$this->data);
    }
    
    public function index($nav_id){
        $this->data['menu'] = $this->navigation->menu_data($nav_id);
        $this->data['records'] = $this->navigation->where('deleted', 0)->where('menus_id', $nav_id)->order_by('order', 'asc')->find_all_t();
        $this->load->view($this->tpl,$this->data);
    }
    
    public function create($nav_id){
        $this->data['menu'] = $this->navigation->menu_data($nav_id);
        $this->data['records'] = $this->routs->where('p_id', 0)->find_all();
        $this->navigation->set_tranlate_field_rules($this->langs,'title','Название страницы на языке','trim|required|min_length[3]|max_length[200]|xss_clean');
        $this->form_validation->set_rules($this->navigation->get_validation_rules('insert'));
        if ($this->form_validation->run() == TRUE)
	{
            $id = $this->_save_nav();
            $this->session->set_flashdata('success', 'Пункт меню успешно создан!');
            redirect('/admin/navigation/index/'.$nav_id, 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function edit($nav_id){
        $this->data['menu'] = $this->navigation->menu_data($nav_id);
        $this->navigation->record_exist('id', $this->uri->segment(4));
        $this->data['translated_fields'] = return_assoc_array($this->navigation->find_all_by_t( array('id'=>$this->uri->segment(4), 'deleted'=>0)),'lang');
        $this->data['records'] = $this->navigation->find_by_t('id',$this->uri->segment(4));
        
        $this->data['parents'] = $this->routs->where('p_id', 0)->find_all();
        $par = array();
        foreach ($this->routs->where('p_id', 0)->find_all() as $v){
            $par[] = $v->id;
        }
        if(!in_array($this->data['records']->rout_id, $par)){
            $parent_id = $this->routs->select('p_id')->find($this->data['records']->rout_id);
            $this->data['childs'] = $this->routs->where('p_id', $parent_id->p_id)->where('rout !=', $this->config->item('error404','routs_db'))->find_all();
        }
        else
            $this->data['childs'] = null;
        $this->navigation->set_tranlate_field_rules($this->langs,'title','Название страницы на языке','trim|required|min_length[3]|max_length[200]|xss_clean');
        $this->form_validation->set_rules($this->navigation->get_validation_rules('insert'));
        if ($this->form_validation->run() == TRUE)
	{
            $this->_save_nav('update',$this->data['records']->id);
            $this->session->set_flashdata('success', 'Пункт меню успешно изменен!');
            redirect(current_url(), 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function delete(){
        $this->navigation->delete($this->uri->segment(4));
        $this->session->set_flashdata('error', 'Страница удалена!');
        redirect(base_url('admin/navigation/index'),'location');
    }
    
    public function order($nav_id){
        $this->data['menu'] = $this->navigation->menu_data($nav_id);
        $this->data['sortable'] = TRUE;
        $this->assets->add(array('js'=>array('third_party/jq-ui/js/jquery-ui-1.9.2.custom.min.js','js/jquery.mjs.nestedSortable.js',))); 
        $this->load->view($this->tpl,$this->data);
    }
    
    public function order_ajax($nav_id) {
        // Save order from ajax call
        if (isset($_POST['sortable'])) {
            $this->navigation->save_order($_POST['sortable']);
        }
        // Fetch all pages
        $this->data['pages'] = $this->navigation->get_nested('backend',$nav_id);
        $this->load->view('backend/navigation/order_ajax', $this->data);
    }

    public function get(){
         echo json_encode($this->routs->where('p_id', $this->input->get('parent_id'))->where('rout !=', $this->config->item('error404','routs_db'))->find_all());
    }
    
    public function _save_nav($type = 'insert', $id = 0) {
        $data = $this->navigation->prep_data($this->input->post());
        $title_data = $this->input->post('title');
        if ($type == 'insert') {
             $id = $this->navigation->insert($data);
             $this->navigation->insert_translate($id, $title_data);
             return $id;
        } else {// Update
            $this->navigation->update($id, $data);
            $this->navigation->update_translate($id, $title_data);
        }
    }
}