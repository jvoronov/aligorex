<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends Backend_Controller{
    public $modules_data = array();
    public function __construct() {
        parent::__construct();
        $this->load->model('products_model','product',TRUE);
        $this->load->model('modules_model','modules',TRUE);
        $this->load->library(array('ckeditor','seo_lib'));
        $this->config->load('cke', TRUE);
        $this->ckeditor->basePath = base_url().'assets/third_party/ckeditor/';
        $this->ckeditor->config = $this->config->item('cke');
        $this->modules_data = $this->modules->find_by('name', $this->router->class);
    }
    
    public function index(){
        $this->config->load('pagination', TRUE);
        $config = array_merge($this->config->item('pagination'), Array(
              'base_url'   => site_url('admin/product/index/'),
              'total_rows'  => $this->product->count_by('deleted', 0),
        ));
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links(); 
        $this->data['records'] = $this->product->where('deleted', 0)
                                               ->limit($config['per_page'])
                                               ->offset($this->uri->segment(4))
                                               ->order_by('id', 'desc')
                                               ->find_all_t();
        $this->load->view($this->tpl,$this->data);
    }
    
    public function create(){
        $this->assets->add(array('js'=>array('js/translit.js')));
        $this->form_validation->set_rules($this->product->get_validation_rules('insert'));
        $this->form_validation->set_rules($this->seo_lib->get_validation_rules());
        $this->form_validation->set_rules('url', 'Адрес товара', 'trim|required|min_length[3]|max_length[200]|callback__check_url|xss_clean');
        if ($this->form_validation->run() == TRUE){
            $id = $this->_save_page();
            $this->seo_lib->save_seo($id,$this->modules_data->id,$this->input->post('seo'));
            $this->session->set_flashdata('success', 'Продукт успешно добавлен!');
            redirect('/admin/product/edit/'.$id, 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function edit(){
        $this->product->record_exist('id', $this->uri->segment(4));
        $this->assets->add(array('js'=>array('js/translit.js')));
        $this->data['records'] = $this->product->find_by_t('id', $this->uri->segment(4));
        $this->data['tf'] = return_assoc_array($this->product->find_all_by_t( array('id'=>$this->uri->segment(4), 'deleted'=>0)),'lang');
        if($seo = $this->seo_lib->get_records($this->uri->segment(4),$this->modules_data->id))
             $this->data['seo'] = return_assoc_array($seo,'seo_lang');
        else
            $this->data['seo'] = null;
        $this->form_validation->set_rules($this->product->get_validation_rules('insert'));
        $this->form_validation->set_rules($this->seo_lib->get_validation_rules());
        $this->form_validation->set_rules('url', 'Адрес товара', 'trim|required|min_length[6]|max_length[200]|callback__check_url_edit|xss_clean');
        if ($this->form_validation->run() == TRUE){
            $this->seo_lib->save_seo($this->uri->segment(4),$this->modules_data->id,$this->input->post('seo'));
            $this->_save_page('update',$this->uri->segment(4));
            $this->session->set_flashdata('success', 'Продукт успешно отредактирован!');
            redirect(current_url(), 'location');
	}
        $this->load->view($this->tpl,$this->data);
    }
    
    public function delete(){
        $this->product->delete($this->uri->segment(4));
        $this->session->set_flashdata('error', 'Продукт удален!');
        redirect(base_url('admin/product/index'),'location');
    }
    
    public function _check_url() {
        if ($this->product->is_unique('url', $this->input->post('url'))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('_check_url', 'Данный адрес товара уже занят!');
            return FALSE;
        }
    }
    
    public function _check_url_edit() {
        $url = $this->product->find_by('url', $this->input->post('url'));
        if(!$url) return true;
        if($url->id == $this->uri->segment(4)) return true;
        $this->form_validation->set_message('_check_url_edit', 'Данный адрес страницы уже занят!');
        return false;
    }
    
    public function _save_page($type = 'insert', $id = 0) {
        $data = $this->product->prep_data($this->input->post());
        $tf = $this->input->post('tf');
        if ($type == 'insert') {
             $page_id = $this->product->insert_translate($data,$tf);
             return $page_id;
        } else {
            $this->product->update($id, $data);
            $this->product->update_translate($tf,$id);
        }
    }
}