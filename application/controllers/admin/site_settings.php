<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_settings extends Backend_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('site_settings_model', 'site_settings_model', TRUE);
    }

    public function index() {
        $this->data['translated_fields'] = $this->settings_lib->get_settings('site_main');
        if($this->input->post('submit')){
            $this->form_validation->set_rules($this->site_settings_model->get_validation_rules('insert'));
            if ($this->form_validation->run() == TRUE) {
                $data = $this->settings_lib->prepare_data($this->input->post());
                $this->settings_lib->update_settings('site_main', $data);
                $this->session->set_flashdata('success', 'Настройки сайта отредакторованы!');
                redirect(current_url(), 'location');
            }
        }
        $this->load->view($this->tpl, $this->data);
    }
}