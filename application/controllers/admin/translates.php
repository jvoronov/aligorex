<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Translates extends Backend_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('translates_model', 'translates', TRUE);
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
    }

    public function index() {
        $per_page = $this->_pagination_init($this->translates->count_all());
        $this->data['records'] = $this->translates->get_all_translates($per_page,$this->uri->rsegment(3));
        $this->load->view($this->tpl, $this->data);
    }

    public function add() {
        if ($this->input->post()) {
            $this->form_validation->set_rules($this->translates->get_validation_rules('insert'));
            if ($this->form_validation->run() == TRUE) {
                $this->_save($this->input->post());
                $this->session->set_flashdata('success', 'Перевод успешно создан');
                redirect('admin/translates', 'location');
            }
        }
        $this->load->view($this->tpl, $this->data);
    }

    public function edit($id) {
        $this->data['records'] = $this->translates->get_single_translate($id);
        if ($this->input->post()) {
            $this->form_validation->set_rules($this->translates->get_validation_rules('insert'));
            if ($this->form_validation->run() == TRUE) {
                $this->_save($this->input->post('tf'), 'update', $id);
                $this->session->set_flashdata('success', 'Перевод успешно отредактирован');
                redirect(current_url(), 'location');
            }
        }
        $this->load->view($this->tpl, $this->data);
    }

    public function delete($id) {
        $this->translates->delete($id);
        $this->session->set_flashdata('error', 'Перевод Удален');
        redirect('admin/translates', 'location');
    }

    public function _save($data, $type = 'insert', $id = 0) {
        if ($type == 'insert') {
            $nt = $this->translates->prep_data($data['nt']);
            $id = $this->translates->insert($nt);
            $tf = $this->translates->prep_data_t($id,$data['tf']);
            $this->translates->insert_t($tf);
        } else {
             $this->translates->update_translate($data, $id);
        }
    }
    
    protected function _pagination_init($count,$per_page = 30) {
        $this->config->load('pagination', TRUE);
        $config = array_merge($this->config->item('pagination'), array(
            'base_url' => site_url('admin/'.$this->router->class.'/'.$this->router->method),
            'total_rows' => $count,
            'per_page' => $per_page,
            //'use_page_numbers' => TRUE,
            'num_links' => 5,
            'first_link' => '<<',
            'last_link' => '>>',
            'uri_segment'=>4
        ));
        $this->pagination->initialize($config);
        return $per_page;
    }
    
    public function vaidate_key($value) {
        //Ключ может сотоять только из латинскик букв и знака подчеркивания
        $valid = ( ! preg_match("/^([a-z0-9_])+$/i", $value)) ?  FALSE : TRUE;
        if($valid){
             return true;
        }else{
             $this->form_validation->set_message('vaidate_key', lang('not_valid_lang_key'));
             return false;
        }
        
    }
    
    public function read_directories($file_to_database = "frontend_lang") {
        $file_to_database = $file_to_database . EXT;
        $insert = array();
        $this->load->helper('file');
        $lang_directory = APPPATH . 'language/';
        $lang_keys = array_keys(get_dir_file_info($lang_directory));
        foreach ($lang_keys as $key => $value) {
            $z = get_dir_file_info($lang_directory . $value);
            foreach ($z as $k => $v) {
                if ($v['name'] == $file_to_database) {
                    include($v['server_path']);
                    foreach ($lang as $k2 => $val) {
                        $insert[$k2][$value] = $val;
                    }
                }
            }
        }
        //dump($insert['user_contact_username_place']);
        $this->translates->files_to_db($insert);
    }

}
