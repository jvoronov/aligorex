<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends Frontend_Controller 
{
    public function __construct(){
        parent::__construct();
    }

    function error_404(){
        $this->output->set_status_header('404');
        $this->data['seo'] = $this->seo_lib->custom_seo('Page not found');
        $this->load->view($this->tpl,$this->data);
    }
}   