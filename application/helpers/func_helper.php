<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('return_assoc_array')) {

    function return_assoc_array(array $data, $assoc_filed_title) {
        $result = array();
        foreach ($data as $v) {
            $result[$v->$assoc_filed_title] = $v;
        }
        return $result;
    }

}

if (!function_exists('prepare_translate_array')) {

    function prepare_translate_array(array $data, $id) {
        $result = array();
        foreach ($data as $key => $value) {
            foreach ($value as $k => $v) {
                $result[$k]['table_id'] = $id;
                $result[$k]['lang'] = $k;
                $result[$k][$key] = $value[$k];
            }
        }
        return $result;
    }

}

function prepare_translate_array2(array $data, $id) {
        $result = array();
        foreach ($data as $key => $value) {
            foreach ($value as $k => $v) {
                $result[$k][$key] = $value[$k];
            }
        }
        return $result;
    }

if (!function_exists('sortable_img')) {

    function sortable_img($records) {
        $out = '<div class="alert alert-warning">Пусто</div>';
        if ($records)
            $out = get_ol_img($records);
        return $out;
    }

}
if (!function_exists('get_ol_img')) {

    function get_ol_img($array, $child = FALSE) {
        $str = '';

        if (count($array)) {
            $str .= '<ol class="sortable">';
            foreach ($array as $item) {
                $str .= '<li id="list_' . $item['id'] . '">';
                $str .= '<div style="width:115px;  margin-left:10px"><img width="100px" src="' . base_url() . $item['img'] . '" /></div>';
                $str .= '</li>' . PHP_EOL;
            }
            $str .= '</ol>' . PHP_EOL;
        }
        return $str;
    }

}
if (!function_exists('sortable')) {

    function sortable($records) {
        $out = '<div class="alert alert-warning">Пусто</div>';
        if ($records)
            $out = get_ol($records);

        return $out;
    }

}
if (!function_exists('get_ol')) {

    function get_ol($array, $child = FALSE) {
        $str = '';
        if (count($array)) {
            $str .= '<ol class="sortable">';
            foreach ($array as $item) {
                if ($item->status == 0)
                    $disabled = "<span style='color:red;float:right'> отключен!</span>";
                else {
                    $disabled = "";
                }
                $str .= '<li id="list_' . $item->id . '">';
                $str .= '<div>' . $item->title . $disabled . '</div>';
                $str .= '</li>' . PHP_EOL;
            }
            $str .= '</ol>' . PHP_EOL;
        }
        return $str;
    }

}

if (!function_exists('search_replace')) {

    function search_replace($search = null,$subject) {
        if($search){
            $replace = '<b class="text-success">'.$search.'</b>';
            $return = str_replace($search, $replace, $subject);
        }else{
            $return = $subject;
        }
        return $return;
    }

}