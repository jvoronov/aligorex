<div class="row" style="margin-top: 100px;">
    <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
        <h1><?php echo lang('forgot_password_heading'); ?></h1>
        <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label); ?></p>
        <?php echo form_open(current_url()); ?>
        <p>
            <label for="email"><?php echo sprintf(lang('forgot_password_email_label'), $identity_label); ?></label> <br />
            <?php echo form_input($email); ?>
        </p>
        <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'), 'class="btn btn-success"'); ?></p>
        <div id="infoMessage"><?php echo $message; ?></div>
        <?php echo form_close(); ?>
    </div>
</div>
