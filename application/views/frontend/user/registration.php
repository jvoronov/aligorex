<div class="row" style="margin-top: 100px;">
    <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
        <?php echo form_open(current_url()); ?>
        <p>
            <?php echo lang('create_user_email_label', 'email'); ?> <br />
            <?php echo form_input($email); ?>
        </p>
        <p>
            <?php echo lang('create_user_password_label', 'password'); ?> <br />
            <?php echo form_input($password); ?>
        </p>
        <p>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm'); ?> <br />
            <?php echo form_input($password_confirm); ?>
        </p>
        <p><?php echo form_submit('submit', lang('submit_registration'), 'class="btn btn-success"'); ?></p>
        <?php echo form_close(); ?>
        <?php if ($message): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
    </div>
</div>