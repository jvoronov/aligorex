<div class="row" style="margin-top: 100px;">
    <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
        <?php echo form_open(current_url(), array('class' => 'form-signin', 'role' => 'form')); ?>
        <h4 class="form-signin-heading"><?php echo lang('site_login')?></h4>
        <?php echo form_input($identity); ?><br>
        <?php echo form_input($password); ?>
        <label class="checkbox">
            <?php echo lang('login_remember_label', 'remember'); ?>
            <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?>
        </label>
        <p><?php echo anchor('user/forgot_password', lang('forgot_password_heading')); ?>?</p>
        <?php echo form_submit('submit', lang('login_submit_btn'), 'class="btn btn-lg btn-primary btn-block"'); ?><br>
        <?php if ($message): ?>
            <?php echo $message; ?>
        <?php endif; ?>
        <?php echo form_close(); ?>
    </div>
</div>