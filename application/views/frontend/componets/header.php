<!DOCTYPE html>
<html lang="<?php echo $this->site_lang;?>">
<head>
    <base href="<?=base_url()?>"> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $seo;?>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400' rel='stylesheet' type='text/css'>
    <?php $this->assets->css();?>
    <?php $this->assets->js();?>
</head>
<body>
    