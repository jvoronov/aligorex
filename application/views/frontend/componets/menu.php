<nav class="navbar navbar-default navbar-fixed-top" role="navigation">          
    <div class="container">
        <div id="logo-scroll">
            <a href="<?php echo site_url('/')?>"><img id="logo" src="<?php echo base_url('assets/frontend/img/aligorex_logotype_it.svg')?>"></a>
        </div>
        <div class="navbar-header">    
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse" >  
            <ul class="nav navbar-nav navbar-right">
                <li><?php echo anchor('user/login', '<i class="glyphicon glyphicon-circle-arrow-right"></i> Login'); ?></li>
                <li><?php echo anchor('user/registration', '<i class="glyphicon glyphicon-plus-sign"></i> Registration'); ?></li>
                <?php if($menu):?>
                    <?php foreach($menu as $item):?>
                        <li><?php echo anchor($item['rout'], $item['title']);?></li>
                    <?php endforeach;?>
                <?php endif;?>
                <?php if($languages):?>
                    <?php foreach($languages as $item):?>
                     <li> <?php echo anchor($this->lang->switch_uri($item->lang),$item->description);?></li>
                    <?php endforeach;?>
                <?php endif;?>
            </ul>
        </div>
    </div><!--container-->
</nav>