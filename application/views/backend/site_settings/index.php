<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <p class="navbar-brand">Настройки сайта</p>
        </div>
    </div>
</nav>
<?php echo form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                 $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active; ?>">
                    <a href="#<?php echo $item->lang; ?>" data-toggle="tab"><?php echo $item->description; ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="tab-content">
            <?php foreach ($translated_fields as $key => $item):
                $key == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                <div class="tab-pane <?php echo $active; ?>" id="<?php echo $key; ?>"><br>
                    <label>Название сайта</label>
                    <?php echo form_input(array('name' => "site_title[{$key}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название', 'value' => $this->input->post('submit') ? set_value("site_title[{$key}]") : $item->site_title)); ?><br>
                    <label>Почтовый адрес</label>
                    <?php echo form_input(array('name' => "site_email[{$key}]", 'class' => 'form-control', 'id' => 'description', 'placeholder' => 'Введите Почтовый адрес', 'value' => $this->input->post('submit') ? set_value("site_email[{$key}]") : $item->site_email)); ?><br>
                    <label>Телефон</label>
                    <?php echo form_input(array('name' => "site_phone[{$key}]", 'class' => 'form-control', 'id' => 'description', 'placeholder' => 'Введите Телефон', 'value' => $this->input->post('submit') ? set_value("site_phone[{$key}]") : $item->site_phone)); ?><br>
                    <label>Адрес</label>
                    <?php echo form_input(array('name' => "site_adress[{$key}]", 'class' => 'form-control', 'id' => 'site_adress', 'placeholder' => 'Введите Адрес', 'value' => $this->input->post('submit') ? set_value("site_adress[{$key}]") : $item->site_adress)); ?><br>
                    <label>Описание сайта</label>
                    <?php echo form_textarea(array('name' => "site_description[{$key}]", 'class' => 'form-control', 'id' => 'description', 'placeholder' => 'Введите Описание', 'value' => $this->input->post('submit') ? set_value("site_description[{$key}]") : $item->site_description)); ?><br>
                    <label>Ключевые слова сайта (разделять запятой)</label>
                    <?php echo form_textarea(array('name' => "site_keywords[{$key}]", 'class' => 'form-control', 'id' => 'keywords', 'placeholder' => 'Введите Ключевые слова сайта', 'value' => $this->input->post('submit') ? set_value("site_keywords[{$key}]") : $item->site_keywords)); ?><br>
                   
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-6">
        <?php echo form_submit(array('name'=>'submit','class'=>'btn btn-success'), 'Сохранить');?>
    </div>
</div>
<?php echo form_close(); ?>