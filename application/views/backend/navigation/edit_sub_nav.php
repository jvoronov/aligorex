<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <p class="navbar-brand"><?php echo lang('editing')?> - <span style="color: green"><?php echo $records->title?></span></p>
        </div>
    </div>
</nav>