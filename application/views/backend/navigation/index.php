<?php $this->load->view('backend/navigation/sub_nav'); ?>
<div class="row">
        <div class="col-lg-12 ">
           <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><?=lang('section_main') ?></h4>
                </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <?php if($records):?>
                            <thead>
                                <tr class="info">
                                    <th class="text-center"><h4><?=lang('title') ?></h4></th>
                                    <th class="text-center"><h4><?=lang('display') ?></h4></th>
                                    <th class="text-center"><h4><?=lang('created') ?></h4></th>
                                    <th class="text-center"><h4><?=lang('edited') ?></h4></th>
                                    <th class="text-center"><h4><?=lang('options') ?></h4></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($records as $record): ?>
                                <tr class="odd gradeX">
                                    <td><strong><?php echo $record->title;?></strong></td>
                                    <td class="text-center">
                                        <?php if($record->status == 0):?>
                                           <button type="button" class="btn btn-danger btn-sm"> <?=lang('now_show') ?> </button>
                                        <?php else:?>
                                            <button type="button" class="btn btn-success btn-sm"> <?=lang('show') ?> </button>
                                        <?php endif;?> 
                                    </td>
                                    <td><?php echo $record->created;?></td>
                                    <td><?php echo $record->modified;?></td>
                                    <td class="text-center">
                                        <a class="btn btn-success btn-sm" href="<?php echo site_url('admin/navigation/edit/'.$record->id);?>" title="<?=lang('edit') ?>">
                                            <i class="fa fa-pencil fa-fw"></i>
                                        </a>
                                        <a class="btn btn-danger btn-sm" href="<?php echo site_url('admin/navigation/delete/'.$record->id);?>" title="<?=lang('delete') ?>" onclick="return confirm(<?php echo lang('del_confirm') ?>)">
                                            <i class="fa fa-trash-o fa-lg"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <?php else: ?>
                                <div class="alert alert-warning">
                                   <?=lang('no_records') ?>
                                </div>
                           <?php endif;?>
                        </table>
                    </div>
            </div>
        </div>
    </div>