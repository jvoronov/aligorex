<nav class="navbar navbar-default">
    <div class="navbar-header">
        <p class="navbar-brand">Управление навигациями сайта</p>
    </div>
</nav>
<?php echo form_open();?>
<div class="row">
    <div class="col-lg-4">
        <?php echo form_input(array('name'=>'name','class'=>'form-control'));?>
    </div>
    <div class="col-lg-2">
        <?php echo form_submit(array('name'=>'submit','class'=>'btn btn-success','value'=>'Add new navigation'));?>
    </div>
</div><hr>
<?php echo form_close();?>
<div class="row">
        <div class="col-lg-12 ">
           <div class="panel panel-default">
                <div class="panel-heading">
                </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <?php if($records):?>
                            <thead>
                                <tr class="info">
                                    <th class="text-center"><h4>Id</h4></th>
                                    <th class="text-center"><h4>Name</h4></th>
                                    <th class="text-center"><h4><?=lang('options') ?></h4></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($records as $record): ?>
                                <tr class="odd gradeX">
                                    <td><strong><?php echo $record->id;?></strong></td>
                                    <td><?php echo $record->name;?></td>
                                    <td class="text-center">
                                        <a class="btn btn-info btn-sm" href="<?php echo site_url('admin/navigation/index/'.$record->id);?>" title="">
                                            <i class="fa fa-plus fa-fw"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <?php else: ?>
                                <div class="alert alert-warning">
                                   <?=lang('no_records') ?>
                                </div>
                           <?php endif;?>
                        </table>
                    </div>
            </div>
        </div>
    </div>