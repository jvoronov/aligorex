<?php $this->load->view('backend/navigation/edit_sub_nav'); ?>
<?=form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-6">
        <label for="title"><?=lang('name')?></label>
        <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->config->item('site_default_lang') ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active;?>"><a href="#<?php echo $item->lang;?>" data-toggle="tab"><?php echo $item->description;?></a></li>
          <?php endforeach;?>
        </ul>
        <div class="tab-content">
             <?php foreach ($translated_fields as $item):
                  $item->lang == $this->config->item('site_default_lang') ? $active = 'active' : $active = '';?>
                <div class="tab-pane <?php echo $active;?>" id="<?php echo $item->lang;?>"><br>
                    <?php echo form_input(array('name' => "title[{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => lang('name'), 'value' => $this->input->post('edit_nav') ? set_value("title[{$item->lang}]") : $item->title)); ?><br>
                   
                </div>
            <?php endforeach;?>
             <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
        </div>
        <label><?=lang('link_to')?></label><br>
        <ul class="cat-list c1" style="width: 150px;">
                <?php foreach($parents as $item):
                    $records->rout_id == $item->id ? $selected = 'selected' : $selected = '';?>
            <li data-cat_id="<?=$item->id?>" class="<?php echo $selected?>"><a href="javascript:;" ><span class="cat_name"><?=$item->title?></span></a></li>
                <?php endforeach;?>    
        </ul>
        <ul class="cat-list c2" style="width: 300px;">
            <?php if(is_array($childs)):
                foreach($childs as $item):
                $records->rout_id == $item->id ? $selected = 'selected' : $selected = '';?>
                    <li data-cat_id="<?=$item->id?>" class="<?php echo $selected?>"><a href="javascript:;" ><span class="cat_name"><?=$item->title?></span></a></li>
                <?php endforeach;?>
            <?php endif;?>  
        </ul><br>
        
        <input type="hidden" name="cid" />
        <input type="hidden" name="category_id" value="<?php echo $records->rout_id?>"/>
        <div style="clear:both"></div>
        <?php echo form_error('category_id', '<div class="alert alert-danger">', '</div>'); ?>
        <label><?=lang('shownot')?></label>
        <?php echo form_dropdown('status', array('0'=>lang('not_show'),'1'=>lang('show')), $this->input->post('edit_page') ? set_value('status') : $records->status , 'class="form-control"');?><br>
        <?php echo form_submit(array('name'=>'edit_nav','class'=>'btn btn-success'), lang('save'));?>
    </div>
</div>
<?= form_close(); ?>
<script type="text/javascript">
    $(document).ready(function(){
        var url = $('base').attr('href');
        
        var clearUl = function(ulid){
            var total_ul = parseInt($('ul.cat-list').length);
            ulid = parseInt(ulid)-1;
            if(ulid==total_ul) return;
            do {
                $('ul.c'+total_ul).empty();
                total_ul-=1;
            }
            while(total_ul > ulid)
        }
        
        $('ul.cat-list').on('click','li', function(){
            var li = $(this);
            var pclass=li.parent().attr('class'); 
            $('input[name="cid"]').val(pclass[pclass.length-1]);
            var cid = parseInt($('input[name="cid"]').val())+1;
            
            
            
            var cat_id = li.data('cat_id');            
            $('input[name="category_id"]').val(cat_id);
            clearUl(cid);
            
            //$(this).addClass('selected');
            
            atr = {
                data:{parent_id:cat_id},
                type:"GET",
                url: url+'admin/navigation/get',
                success:function(data){
                    data = $.parseJSON(data);

                    $.each(data,function(i, item){
                        $('ul.c'+cid).append('<li data-cat_id="'+item.id+'"><a href="javascript:;"  >'+item.title+'</a></li>');
                    });
                    $('.cat-list').find("li").removeClass("selected");
                    li.addClass('selected');
                    
                }
            };
            $.ajax(atr);
            
        });
    });
</script>