<?php $this->load->view('backend/navigation/sub_nav'); ?>
<div class="row">
    <div class="col-lg-12 ">
<section>
	<div id="orderResult"></div>
	<input type="button" id="save" value="Сохранить" class="btn btn-success" />
</section>
</div>
</div>
<script>
$(function() {
    
	$.post('<?php echo site_url('admin/navigation/order_ajax/'.$menu->id); ?>', {}, function(data){
		$('#orderResult').html(data);
                
	});

	$('#save').click(function(){
		oSortable = $('.sortable').nestedSortable('toArray');

		$('#orderResult').slideUp(function(){
			$.post('<?php echo site_url('admin/navigation/order_ajax/'.$menu->id); ?>', { sortable: oSortable }, function(data){
				$('#orderResult').html(data);
				$('#orderResult').slideDown();
			});
		});
		
	});
});
</script>
