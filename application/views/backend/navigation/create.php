<?php $this->load->view('backend/navigation/sub_nav'); ?>
<?php echo form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <label for="title"><?=lang('name')?></label>
        <ul class="nav nav-tabs">
            <?php  foreach ($this->langs as $item):?>
                <?php $active =  $item->lang == $this->site_lang ? 'active' :null;?>
                <li class="<?php echo $active;?>"><a href="#<?php echo $item->lang;?>" data-toggle="tab"><?php echo $item->description;?></a></li>
          <?php endforeach;?>
        </ul>
        <div class="tab-content">
             <?php foreach ($this->langs as $item):?>
                <?php $active =  $item->lang == $this->site_lang ? 'active' :null;?>
                <div class="tab-pane <?php echo $active;?>" id="<?php echo $item->lang;?>"><br>
                    <?php echo form_input(array('name' => "title[{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => lang('section_settings'), 'value' => set_value("title[{$item->lang}]"))); ?><br>
                   
                </div>
            <?php endforeach;?>
             
        </div>
        <label><?=lang('link_to')?></label><br>
        <ul class="cat-list c1" style="width: 150px;">
                <?php foreach($records as $item):?>
                    <li data-cat_id="<?=$item->id?>"><a href="javascript:;"><span class="cat_name"><?=$item->title?></span></a></li>
                <?php endforeach;?>    
        </ul>
        <ul class="cat-list c2" style="width: 300px;"></ul><br>
        
        <input type="hidden" name="cid" />
        <input type="hidden" name="category_id" />
        <div style="clear:both"></div>
        <label><?=lang('shownot')?></label>
        <?php echo form_dropdown('status', array('0'=>lang('not_show'),'1'=>lang('show')), '1' , 'class="form-control"');?><br>
        <?php echo form_hidden('menus_id',$menu->id);?><br>
        <?php echo form_submit(array('name'=>'add_nav','class'=>'btn btn-success'), lang('create'));?>
    </div>
</div>
<?php echo form_close(); ?>
<script type="text/javascript">
    $(document).ready(function(){
        var url = $('base').attr('href');
        
        var clearUl = function(ulid){
            var total_ul = parseInt($('ul.cat-list').length);
            ulid = parseInt(ulid)-1;
            if(ulid==total_ul) return;
            do {
                $('ul.c'+total_ul).empty();
                total_ul-=1;
            }
            while(total_ul > ulid)
        }
        
        $('ul.cat-list').on('click','li', function(){
            var li = $(this);
            var pclass=li.parent().attr('class'); 
            $('input[name="cid"]').val(pclass[pclass.length-1]);
            var cid = parseInt($('input[name="cid"]').val())+1;
            
            
            
            var cat_id = li.data('cat_id');            
            $('input[name="category_id"]').val(cat_id);
            clearUl(cid);
            
            //$(this).addClass('selected');
            
            atr = {
                data:{parent_id:cat_id},
                type:"GET",
                url: url+'admin/navigation/get',
                success:function(data){
                    data = $.parseJSON(data);

                    $.each(data,function(i, item){
                        $('ul.c'+cid).append('<li data-cat_id="'+item.id+'"><a href="javascript:;"  >'+item.title+'</a></li>');
                    });
                    $('.cat-list').find("li").removeClass("selected");
                    li.addClass('selected');
                    
                }
            };
            $.ajax(atr);
            
        });
    });
</script>