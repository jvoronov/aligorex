<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <p class="navbar-brand">Заказы</p>
        </div>
    </div>
</nav>
<div class="row">
        <div class="col-lg-12 ">
           <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Обзор заказов</h4>
                </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <?php if($records):?>
                            <thead>
                                <tr class="info">
                                    <th class="text-center"><h4>N заказа</h4></th>
                                    <th class="text-center"><h4>Имя</h4></th>
                                    <th class="text-center"><h4>Почта</h4></th>
                                    <th class="text-center"><h4>Телефон</h4></th>
                                    <th class="text-center"><h4>Статус</h4></th>
                                    <th class="text-center"><h4>Оформлен</h4></th>
                                    <th class="text-center"><h4>Сумма</h4></th>
                                    <th class="text-center"><h4>Опции</h4></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($records as $record): ?>
                                <tr class="odd gradeX">
                                    <td class="text-center">
                                       <?php if($record->new == 1):?>
                                        <span class="label label-success">New</span> 
                                       <?php endif;?>
                                            <?php echo $record->id;?> 
                                    </td>
                                    <td class="text-center"><?php echo $record->name;?></td>
                                    <td class="text-center"><?php echo $record->email;?></td>
                                    <td class="text-center"><?php echo $record->phone;?></td>
                                    <td class="text-center">
                                        <?php
                                            if($record->status == 0)
                                                echo '<span class="text-danger">Не оплачен</span>';
                                            else
                                                echo '<span class="text-success">Оплачен</span>';
                                        ?>
                                    </td>
                                    <td class="text-center"><?php echo $record->created;?></td>
                                    <td class="text-center"><?php echo $record->sum;?></td>
                                    <td class="text-center">
                                        <a class="btn btn-info btn-sm" href="<?php echo site_url('admin/orders/details/'.$record->id);?>" title="Подробнее">
                                            <i class="fa fa-eye fa-fw"></i>
                                        </a>
                                        <a class="btn btn-danger btn-sm" href="<?php echo site_url('admin/orders/delete/'.$record->id);?>" title="Удалить" onclick="return confirm('Вы действительно хотите удалить запись?')">
                                            <i class="fa fa-trash-o fa-lg"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <?php else: ?>
                                <div class="alert alert-warning">
                                    Заказов нет
                                </div>
                           <?php endif;?>
                        </table>
                    </div>
            </div>
        </div>
    </div>
<div class="row">
    <div class="col-lg-12">
        <?php echo $pagination; ?>
    </div>
</div