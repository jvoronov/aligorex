<?php $this->load->view('backend/payment/sub_nav'); ?>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <p class="navbar-brand">Редактирование вида оплаты - <span style="color: green"><?php echo $records->description?></span></p>
        </div>
    </div>
</nav>
<?=form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active;?>"><a href="#<?php echo $item->lang;?>" data-toggle="tab"><?php echo $item->description;?></a></li>
          <?php endforeach;?>
        </ul>
        <div class="tab-content">
             <?php foreach ($translated_fields as $item):
                  $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                <div class="tab-pane <?php echo $active;?>" id="<?php echo $item->lang;?>"><br>
                    <label for="description">Вид оплаты</label>
                    <?php echo form_input(array('name' => "description[{$item->lang}]", 'class' => 'form-control', 'id' => 'description', 'placeholder' => 'Введите название', 'value' => $this->input->post('edit_page') ? set_value("description[{$item->lang}]") : $item->description)); ?><br>
                </div>
            <?php endforeach;?>
         </div>
        <label>Отображать / Не отображать</label>
        <?php echo form_dropdown('status', array('0'=>'Не отображать','1'=>'Отображать'), $records->status , 'class="form-control"');?><br>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?php echo form_submit(array('name'=>'edit_page','class'=>'btn btn-success'), 'Сохранить изменения');?>
    </div>
</div>
<?=form_close(); ?>