<?php $this->load->view('backend/translates/sub_nav');?>
<nav class="navbar navbar-default">
    <div class="navbar-header">
        <p class="navbar-brand">Редактирование перевода <span class="text-success"><?php echo $records[$this->site_lang]->key;?></span></p>
    </div>
</nav>
<?php echo form_open(current_url()); ?>
<?php foreach ($this->langs as $item): ?>
    <div class="row">
        <div class="col-lg-8">
            <label><?php echo $item->description; ?></label>
            <?php echo form_input(array('name' => "tf[value][{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите перевод', 'value' => set_value("tf[value][{$item->lang}]", isset($records[$item->lang]->value)?$records[$item->lang]->value:null))); ?><br>
            <?php echo form_error("tf[value][{$item->lang}]"); ?>
        </div>
    </div>
<?php endforeach; ?>
<hr>
<div class="row">
    <div class="col-lg-6">
        <?php echo form_submit(array('name' => 'submit', 'class' => 'btn btn-success btn-md'), 'Сохранить'); ?>
        <?php echo form_hidden('nt[key]','qwjfqjwnfjqwnfnwqqwdqwd'); ?><br>
    </div>
</div>
<?php echo form_close();?>