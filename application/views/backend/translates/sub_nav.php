<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <p class="navbar-brand">Переводы</p>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php $this->uri->rsegment(2) == 'index' ? $active = 'class="active"' : $active = '';?>
                <li <?php echo $active?>><?php echo anchor('admin/translates', 'Обзор')?></li>
                <?php $this->uri->rsegment(2) == 'add' ? $active = 'class="active"' : $active = '';?>
                <li <?php echo $active?>><?php echo anchor('admin/translates/add', 'Добавить')?></li>
            </ul>
            <?php echo form_open('admin/'.$this->router->class,array('class'=>'navbar-form navbar-right'))?>
                <div class="form-group">
                     <?php echo form_input(array('name' => "word", 'class' => 'form-control', 'placeholder' => 'Поиск')); ?>
                </div>
                <button type="submit" class="btn btn-default" >Найти</button>
            <?php echo form_close();?>
        </div>
    </div>
</nav>