<?php $this->load->view('backend/translates/sub_nav'); ?>
<?php echo form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-8">
        <label>Уникальный ключ</label>
        <?php echo form_input(array('name' => "nt[key]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите Уникальный ключ', 'value' => set_value("nt[key]"))); ?><br>
        <?php echo form_error('nt[key]'); ?>
    </div>
</div>
<hr>
<?php foreach ($this->langs as $item): ?>
    <div class="row">
        <div class="col-lg-8">
            <label><?php echo $item->description; ?></label>
            <?php echo form_input(array('name' => "tf[value][{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите перевод', 'value' => set_value("tf[value][{$item->lang}]"))); ?><br>
            <?php echo form_error("tf[value][{$item->lang}]"); ?>
        </div>
    </div>
<?php endforeach; ?>
<hr>
<div class="row">
    <div class="col-lg-6">
        <?php echo form_submit(array('name' => 'submit', 'class' => 'btn btn-success btn-md'), 'Добавить'); ?>
    </div>
</div>
<?php echo form_close(); ?>