<?php
$this->load->view('backend/translates/sub_nav');
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Обзор переводов
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Уникальный ключ</th>
                                <th>Перевод</th>
                                <th>Язык</th>
                                <th>Опции</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($records):?>
                                <?php foreach ($records as $value): ?>
                                    <tr>
                                        <td><?php echo $value->key; ?></td>
                                        <td><?php echo search_replace($this->input->post('word') ? $this->input->post('word') : null, $value->value); ?></td>
                                        <td><?php echo $value->lang; ?></td>
                                        <td>
                                            <?php echo anchor('admin/translates/edit/'.$value->id,'<span class="glyphicon glyphicon-pencil"></span>','class="btn btn-xs btn-success"')?>
                                            <?php echo anchor('admin/translates/delete/'.$value->id,'<span class="glyphicon glyphicon-trash"></span>','class="btn btn-xs btn-danger" onclick="return confirm(\'Вы действительно хотите удалить запись?\')"')?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <?php echo $this->pagination->create_links(); ?>
</div>