<?php $this->load->view('backend/templates/edit_sub_nav'); ?>
<?=form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-6">
        <label for="title">Название шаблона</label>
        <?php echo form_input(array('name' => 'title', 'required' => '', 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название страницы', 'value' => $this->input->post('edit_page') ? set_value('title') : $records->title)); ?><br>
        <?php echo form_error('title', '<div class="alert alert-danger">', '</div>'); ?>
        <label for="url">Адрес шаблона (url)</label>
        <?php echo form_input(array('name' => 'url', 'required' => '','class' => 'form-control', 'id' => 'url', 'value' => $this->input->post('edit_page') ? set_value('url') : $records->url)); ?><br>
        <?php echo form_error('url', '<div class="alert alert-danger">', '</div>'); ?>
        <label for="name">Путь до шаблона ( frontend/templates/имя шаблона )</label>
        <?php echo form_input(array('name' => 'path', 'required' => '','class' => 'form-control', 'id' => 'name', 'value' => $this->input->post('edit_page') ? set_value('path') : $records->path)); ?><br>
        <?php echo form_error('path', '<div class="alert alert-danger">', '</div>'); ?>
        <label>Отображать / Не отображать</label>
        <?php echo form_dropdown('status', array('0'=>'Не отображать','1'=>'Отображать'), $this->input->post('edit_page') ? set_value('status') : $records->status , 'class="form-control"');?><br>
        <?php echo form_submit(array('name'=>'edit_page','class'=>'btn btn-success'), 'Сохранить изменения');?>
    </div>
</div>
<?=form_close(); ?>