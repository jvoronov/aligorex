<?php $this->load->view('backend/product/edit_sub_nav'); ?>
    <div class="row">
        <?php echo form_open_multipart(current_url()); ?>
        <div class="col-lg-4">
            <label>Выберите изображение</label>
            <?php echo form_upload(array('name' => 'userfile', 'multiple' => '')); ?>
            <?php echo form_error('userfile', '<div class="alert alert-danger">', '</div>'); ?>
        </div>
        <div class="col-lg-5">
            <label>Описание изображения</label>
            <?php echo form_input(array('name' => 'title', 'id' => 'title', 'class' => 'form-control', 'value' => set_value('title'))); ?><br>
            <?php echo form_error('title', '<div class="alert alert-danger">', '</div>'); ?>
        </div>
        <div class="col-lg-2">
            <br>
            <?php echo form_submit(array('name' => 'uload_img', 'class' => 'btn btn-info'), 'Загрузить изображение'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
<hr>
<?php if ($images): ?>
    <?php echo form_open(current_url()); ?>
    <div class="row">
        <div class="col-lg-4">
            <?php echo form_submit(array('name' => 'apply', 'class' => 'btn btn-success'), 'Сохранить изменения'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <hr>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr class="info">
                            <th class="text-center"><h4>Изображение</h4></th>
                    <th class="text-center"><h4>Описание</h4></th>
                    <th class="text-center"><h4>Удалить</h4></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($images as $image): ?>
                            <tr class="odd gradeX">
                                <td class="text-center">
                                    <img src="<?php echo site_url($image->thumbnail); ?>" alt="" class="img-thumbnail">
                                </td>
                                <td class="text-center">
                                    <textarea rows="2" name="title[<?php echo $image->id; ?>]"><?php echo $image->title; ?></textarea>
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" name="mycheck[]"  value="<?php echo $image->id; ?>" />
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>   
                </table>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>  
<?php endif; ?>
</div>
