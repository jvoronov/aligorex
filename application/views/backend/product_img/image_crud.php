<?php $this->load->view('backend/product/edit_sub_nav'); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo $output->output; ?>
    </div>
</div>
<?php foreach($output->css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($output->js_files as $file): ?>
<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>