<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <p class="navbar-brand">Доставка товара</p>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php $this->uri->segment(3) == 'index' ? $active = 'class="active"' : $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/delivery/index');?>">Обзор</a></li>
                <?php $this->uri->segment(3) == 'create' ? $active = 'class="active"' : $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/delivery/create');?>">Добавить доставку</a></li>
            </ul>
        </div>
    </div>
</nav>