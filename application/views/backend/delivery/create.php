<?php $this->load->view('backend/delivery/sub_nav'); ?>
<?=form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
       <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active;?>"><a href="#<?php echo $item->lang;?>" data-toggle="tab"><?php echo $item->description;?></a></li>
          <?php endforeach;?>
        </ul>
        <div class="tab-content">
             <?php foreach ($this->langs as $item):
                  $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                <div class="tab-pane <?php echo $active;?>" id="<?php echo $item->lang;?>"><br>
                    <label for="description">Адрес доставки</label>
                    <?php echo form_input(array('name' => "description[{$item->lang}]", 'class' => 'form-control', 'id' => 'description', 'placeholder' => 'Введите адрес доставки', 'value' => set_value("description[{$item->lang}]"))); ?><br>
                </div>
            <?php  endforeach;?>
        </div>
        <label for="price">Цена доставки</label>
        <?php echo form_input(array('name' => "price", 'class' => 'form-control', 'id' => 'price', 'placeholder' => 'Введите цену доставки', 'value' => set_value("price"))); ?><br>
        <label>Отображать / Не отображать</label>
        <?php echo form_dropdown('status', array('0'=>'Не отображать','1'=>'Отображать'), '0' , 'class="form-control"');?><br>
        <label>Форма адреса доставки клиента</label>
        <?php echo form_dropdown('show_adress', array('0'=>'Не отображать','1'=>'Отображать'), '1' , 'class="form-control"');?><br>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-6">
        <?php echo form_submit(array('name'=>'submit','class'=>'btn btn-success'), 'Добавить');?>
    </div>
</div>
<hr>
<?= form_close(); ?>