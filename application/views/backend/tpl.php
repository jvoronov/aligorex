<?php $this->load->view('backend/componets/header'); ?>
<div class="wrapper">
    <?php $this->load->view('backend/componets/top_nav'); ?>
    <?php $this->load->view('backend/componets/left_nav'); ?>
</div>
<div id="page-wrapper">
    <?php $this->load->view('backend/componets/breadcrumps');?>
    <?php $this->load->view('backend/componets/msg'); ?>
    <?php $this->load->view($this->data['main']); ?>
</div>
</body>
</html>
