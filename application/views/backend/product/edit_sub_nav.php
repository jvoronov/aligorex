<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <p class="navbar-brand">Редактирование товара</p>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php $this->uri->segment(2) == 'product' ? $active = 'class="active"' : $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/product/edit/'.$this->uri->segment(4));?>">Информация</a></li>
                <?php $this->uri->segment(3) == 'image_crud' ? $active = 'class="active"' : $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/product_img/image_crud/'.$this->uri->segment(4));?>">Изображения</a></li>
                <?php $this->uri->segment(3) == 'product_category' ? $active = 'class="active"' : $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/categories/product_category/'.$this->uri->segment(4));?>">Категории</a></li>
                <?php $this->uri->segment(3) == 'attributes_product' ? $active = 'class="active"' : $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/attributes/attributes_product/'.$this->uri->segment(4));?>">Фильтры</a></li>
            </ul>
        </div>
    </div>
</nav>