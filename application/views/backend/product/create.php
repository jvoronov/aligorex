<?php $this->load->view('backend/product/sub_nav'); ?>
<?php echo form_open(current_url());?>
    <div class="row">
        <div class="col-lg-12">
            <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active;?>">
                    <a href="#<?php echo $item->lang;?>" data-toggle="tab">
                        <?php echo $item->description;?>
                    </a>
                </li>
            <?php endforeach;?>
            </ul>
            <div class="tab-content">
                 <?php foreach ($this->langs as $item):
                    $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                    <div class="tab-pane <?php echo $active;?>" id="<?php echo $item->lang;?>"><br>
                        <label>Название товара</label>
                        <?php echo form_input(array('name' => "tf[title][{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название', 'value' => set_value("tf[title][{$item->lang}]"))); ?><br>
                        <label>Описание товара</label>
                        <?php echo $this->ckeditor->editor("tf[content][{$item->lang}]", html_entity_decode(set_value("tf[content][{$item->lang}]"))); ?><br>
                        <?php  $data['lang'] = $item->lang; $this->load->view('backend/seo/create',$data); ?>
                    </div>
                <?php  endforeach;?>
            </div>
        </div>
      </div>
      <div class="row">
          <div class="col-lg-6">
               <label>Адрес товара (url)</label>
            <?php echo form_input(array('name'=>'url','class'=>'form-control','id'=>'url','value'=>set_value('url')));?><br>
            <label>Отображать / Не отображать</label>
            <?php echo form_dropdown('status', array('disabled'=>'Не отображать','enabled'=>'Отображать'), 1 , 'class="form-control"');?><br>
          </div>
        <div class="col-lg-6">
            <label>Цена</label>
            <div class="form-group input-group">
                <span class="input-group-addon">
                    <i class="fa fa-eur"></i>
                </span>
                <?php echo form_input(array('name'=>'price','class'=>'form-control','placeholder'=>'Введите цену товара ( 0 - по договоренности)','value'=>set_value('price')));?>
                <span class="input-group-addon">EUR</span>
            </div>
            <label>Количество</label>
            <div class="form-group input-group">
                <span class="input-group-addon">
                    <i class="fa fa-list-ol"></i>
                </span>
                <?php echo form_input(array('name'=>'quantity','class'=>'form-control','placeholder'=>'Введите количество товара','disabled'=>'disabled','value'=>set_value('quantity')));?>
                <span class="input-group-addon">шт.</span>
            </div>
            <label>Скидка</label>
            <div class="form-group input-group">
                <span class="input-group-addon">
                    <i class="fa fa-gift"></i>
                </span>
                <?php echo form_input(array('name'=>'discount','id'=>'discount','class'=>'form-control','placeholder'=>'Введите скидку на товар в рублях (не обязательно)','disabled'=>'disabled','value'=>set_value('discount')));?>
                <span class="input-group-addon">руб.</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <hr>
            <?php echo form_submit(array('name'=>'submit','class'=>'btn btn-success'), 'Добавить товар');?>
        </div> 
    </div><hr>
   <?php echo form_close();?>