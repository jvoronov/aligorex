<?php $this->load->view('backend/categories/sub_nav'); ?>
<div class="row">
    <div class="col-lg-12 text-center">
        <h4>Сортировка категорий - <span style="color: green"><?php echo $module_data->title?></span></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 ">
<section>
	<div id="orderResult"></div>
	<input type="button" id="save" value="Сохранить" class="btn btn-success" />
</section>
</div>
</div>
<script>
$(function() {
    
	$.post('<?php echo site_url('admin/categories/order_ajax/'.$module_data->name); ?>', {}, function(data){
		$('#orderResult').html(data);
                
	});

	$('#save').click(function(){
		oSortable = $('.sortable').nestedSortable('toArray');

		$('#orderResult').slideUp(function(){
			$.post('<?php echo site_url('admin/categories/order_ajax/'.$module_data->name); ?>', { sortable: oSortable }, function(data){
				$('#orderResult').html(data);
				$('#orderResult').slideDown();
			});
		});
		
	});
});
</script>
