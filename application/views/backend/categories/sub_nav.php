<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <p class="navbar-brand">Категории</p>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php $this->uri->segment(3) == 'index' ? $active = 'class="active"' : $active = '';?>
                <li class="dropdown">
                   <a id="drop1" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Обзор<b class="caret"></b></a>
                   <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                       <?php foreach ($this->modules_data as $item):?>
                       <li role="presentation"><a role="menuitem" tabindex="-1" href="admin/categories/index/<?php echo $item->name;?>"><?php echo $item->title;?></a></li>
                       <?php endforeach;?>
                  </ul>
                </li>
                <li class="dropdown">
                   <a id="drop2" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Добавить категорию<b class="caret"></b></a>
                   <ul class="dropdown-menu" role="menu" aria-labelledby="drop2">
                       <?php foreach ($this->modules_data as $item):?>
                       <li role="presentation"><a role="menuitem" tabindex="-1" href="admin/categories/create/<?php echo $item->name;?>"><?php echo $item->title;?></a></li>
                       <?php endforeach;?>
                  </ul>
                </li>
                <li class="dropdown">
                   <a id="drop3" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Сортировать категории<b class="caret"></b></a>
                   <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                       <?php foreach ($this->modules_data as $item):?>
                       <li role="presentation"><a role="menuitem" tabindex="-1" href="admin/categories/order/<?php echo $item->name;?>"><?php echo $item->title;?></a></li>
                       <?php endforeach;?>
                  </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-right" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Поиск" disabled>
                </div>
                <button type="submit" class="btn btn-default" disabled>Найти</button>
            </form>
        </div>
    </div>
</nav>