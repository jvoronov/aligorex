<?php $this->load->view('backend/categories/sub_nav'); ?>
<div class="row">
    <div class="col-lg-12 text-center">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                 <p class="navbar-brand">Редактирование категории - <span style="color: green"><?php echo $records->title?></span> (<?php echo $module_data->title?>)</p>
            </div>
        </nav>
    </div>
</div>
<?php echo form_open_multipart(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs">
           <?php foreach ($this->langs as $item):?>
                <?php $active = $item->lang == $this->site_lang ? 'active' : null;?>
                <li class="<?php echo $active;?>">
                    <a href="#<?php echo $item->lang;?>" data-toggle="tab">
                        <?php echo $item->description;?>
                    </a>
                </li>
          <?php endforeach;?>
        </ul>
        <div class="tab-content">
             <?php foreach ($this->langs as $item):?>
                <?php $active = $item->lang == $this->site_lang ? 'active' : null;?>
                <div class="tab-pane <?php echo $active;?>" id="<?php echo $item->lang;?>"><br>
                    <label for="title">Название категории</label>
                   <?php echo form_input(array('name' => "tf[title][{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название', 'value' =>  set_value("tf[title][{$item->lang}]",isset($tf[$item->lang]->title)?$tf[$item->lang]->title:null))); ?><br>
                   <?php  $data['lang'] = $item->lang; $this->load->view('backend/seo/edit',$data); ?>
                </div>
            <?php endforeach;?>
         </div>
        <label for="url">Адрес категории (url)</label>
        <?php echo form_input(array('name' => 'url', 'required' => '','class' => 'form-control', 'id' => 'url', 'value' => $this->input->post('edit_page') ? set_value('url') : $records->url)); ?><br>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <label>Отображать / Не отображать</label>
        <?php echo form_dropdown('status', array('0'=>'Не отображать','1'=>'Отображать'), $this->input->post('edit_page') ? set_value('status') : $records->status , 'class="form-control"');?><br>
        <?php if($records->img):?>
            <p><img src="uploads/categories_img/<?php echo $records->img;?>" class="img-thumbnail"></p>
        <?php endif;?>
        <input type="file" name="file"/><br>
        <?php echo form_submit(array('name'=>'submit','class'=>'btn btn-success'), 'Сохранить изменения');?>
    </div>
</div><hr>
<?=form_close(); ?>