<?php $this->load->view('backend/product/edit_sub_nav');?>
<?php if ($checked) $c = $checked; else $c = array();?>
<?php echo form_open(current_url());?>
    <div class="row">
        <?php echo show_categories($records,$c); ?>  
    </div>
    <div class="row">
        <hr>
        <div class="col-lg-3">
            <?php echo form_submit(array('name'=>'add','class'=>'btn btn-success'), 'Сохранить');?>
        </div>
    </div> 
<?php echo form_close();?>
<?php
function show_categories($array,$checked = array()) {
    $str = '';
    if (count($array)) {
        foreach ($array as $item) {
            in_array($item['id'], $checked) ? $check = 'checked' : $check = '';
            $str .= '<div class="col-lg-3">';
            $str .= '<div class="checkbox"> <label>';
            $str .= '<input type="checkbox" name="mycheck[]"  value="'.$item['id'].'" '.$check.' />';
            $str .= '<h3>'. $item['title'].'</h3>';
            $str .= '</label></div>' . PHP_EOL;
            if (isset($item['children']) && count($item['children'])) {
                foreach ($item['children'] as $item) {
                    in_array($item['id'], $checked) ? $check = 'checked' : $check = '';
                    $str .= '<div class="checkbox" style="margin-left:20px"> <label>';
                    $str .= '<input type="checkbox" name="mycheck[]"  value="'.$item['id'].'" '.$check.' />';
                    $str .= $item['title'];
                    $str .= '</label></div>' . PHP_EOL;
                }
            }
            $str .= '</div>'. PHP_EOL;
        }
    }
    return $str;
}
?>