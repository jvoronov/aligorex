<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav my_menu" id="side-menu">
            <!--dashboard-->
            <?php $menu_active = $this->uri->rsegment(1) == 'dashboard' ? 'id="menu_active"' : null; ?>
            <li><?php echo anchor('admin/dashboard', '<i class="fa fa-home fa-fw"></i> Главная', $menu_active); ?></li>
            <!--users-->
            <?php $menu_active = $this->uri->rsegment(1) == 'auth' ? 'id="menu_active"' : null; ?>
            <li><?php echo anchor('admin/auth', '<i class="fa fa-users fa-fw"></i> Пользователи', $menu_active); ?></li>
            <!--navigation-->
            <?php $menu_active = $this->uri->rsegment(1) == 'navigation' ? 'id="menu_active"' : null; ?>
            <li><?php echo anchor('admin/navigation/menus', '<i class="fa fa-list-ol fa-fw"></i> Навигация', $menu_active); ?></li>
            <!--categories-->
            <?php $menu_active = $this->uri->rsegment(1) == 'categories' ? 'id="menu_active"' : null; ?>
            <li><?php echo anchor('admin/categories', '<i class="fa fa-align-justify fa-fw"></i> Категории', $menu_active); ?></li>
            <!--pages-->
            <?php $menu_active = $this->uri->rsegment(1) == 'pages' ? 'id="menu_active"' : null; ?>
            <li><?php echo anchor('admin/pages', '<i class="fa fa-files-o fa-fw"></i> Страницы', $menu_active); ?></li>
            <!--templates-->
            <?php $menu_active = $this->uri->rsegment(1) == 'templates' ? 'id="menu_active"' : null; ?>
            <li><?php echo anchor('admin/templates', '<i class="fa fa-code fa-fw"></i> Шаблоны', $menu_active); ?></li>
            <!--blog-->
            <?php $menu_active = $this->uri->rsegment(1) == 'blog' ? 'id="menu_active"' : null; ?>
            <li><?php echo anchor('admin/blog', '<i class="fa fa-calendar-o fa-fw"></i> Блог', $menu_active); ?></li>
            <!--emails-->
            <?php $menu_active = $this->uri->rsegment(1) == 'emails' ? 'id="menu_active"' : null; ?>
            <li><?php echo anchor('admin/emails', '<i class="fa fa-envelope-o fa-fw"></i> Рассылка', $menu_active); ?></li>
            <!--gallery-->
            <?php $menu_active = $this->uri->rsegment(1) == 'gallery' ? 'id="menu_active"' : null; ?>
            <li><?php echo anchor('admin/gallery', '<i class="fa fa-film  fa-fw"></i> Галерея', $menu_active); ?></li>
            <!-- translates -->
            <?php $menu_active = $this->uri->rsegment(1) == 'translates' ? 'id="menu_active"' : null; ?>
            <li><?php echo anchor('admin/translates', '<i class="glyphicon glyphicon-font"></i> ' . lang('b_menu_translations'), $menu_active); ?></li>
            <!-- shop -->
            <?php $active = in_array($this->uri->rsegment(1), array('product', 'filter', 'attributes', 'delivery', 'payment', 'orders')) || in_array($this->uri->rsegment(2), array('image_crud', 'product_category', 'attributes_product')) ? 'active' : null; ?>
            <li class="<?php echo $active ?>">
                <?php echo anchor('#', '<i class="fa fa-money fa-fw"></i> Магазин <span class="fa arrow"></span>'); ?>
                <!-- Products -->
                <?php $menu_active = in_array($this->uri->rsegment(1), array('product')) || in_array($this->uri->rsegment(2), array('image_crud', 'product_category', 'attributes_product')) ? 'id="menu_active"' : null; ?>
                <ul class="nav nav-second-level">
                    <li><?php echo anchor('admin/product', 'Товары', $menu_active); ?></li>
                </ul>
                <!-- attributes -->
                <?php $menu_active = $this->uri->rsegment(1) == 'attributes' ? 'id="menu_active"' : null; ?>
                <ul class="nav nav-second-level">
                    <li><?php echo anchor('admin/attributes', 'Фильтры товаров', $menu_active); ?></li>
                </ul>
                <!-- delivery -->
                <?php $menu_active = $this->uri->rsegment(1) == 'delivery' ? 'id="menu_active"' : null; ?>
                <ul class="nav nav-second-level">
                    <li><?php echo anchor('admin/delivery', 'Доставка', $menu_active); ?></li>
                </ul>
                <!-- payment -->
                <?php $menu_active = $this->uri->rsegment(1) == 'payment' ? 'id="menu_active"' : null; ?>
                <ul class="nav nav-second-level">
                    <li><?php echo anchor('admin/payment', 'Оплата', $menu_active); ?></li>
                </ul>
                <!-- orders -->
                <?php $menu_active = $this->uri->rsegment(1) == 'orders' ? 'id="menu_active"' : null; ?>
                <ul class="nav nav-second-level">
                    <li><?php echo anchor('admin/orders', 'Заказы', $menu_active); ?></li>
                </ul>
            </li>
            <!-- site settings -->
            <?php $active = in_array($this->uri->rsegment(1), array('languages', 'site_settings')) ? 'active' : null; ?>
            <li class="<?php echo $active ?>">
                <?php echo anchor('#', '<i class="fa fa-gears fa-fw"></i> Настройки <span class="fa arrow"></span>'); ?>
                <!-- languages -->
                <?php $menu_active = $this->uri->rsegment(1) == 'languages' ? 'id="menu_active"' : null; ?>
                <ul class="nav nav-second-level">
                    <li><?php echo anchor('admin/languages', 'Языки', $menu_active); ?></li>
                </ul>
                <!-- settings -->
                <?php $menu_active = $this->uri->rsegment(1) == 'site_settings' ? 'id="menu_active"' : null; ?>
                <ul class="nav nav-second-level">
                    <li><?php echo anchor('admin/site_settings', 'Настройки сайта', $menu_active); ?></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>