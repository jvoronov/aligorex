<script>
$(function() {
            $('.sortable').sortable({
            update: function(){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo current_url() ?>',
                    dataType: 'json',
                    data: $(this).sortable('serialize'),
                    success: function(data){
                    }
                });
            }
        });
        

        $('ol.sortable').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            maxLevels: 1
        });
        
        $("#get_items").on('click',function(e){
            $catid = $('#categories option:selected').val();
            e.preventDefault();
            if($catid){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo current_url() ?>',
                    data:{category_id:$catid},
                    success: function(data){
                        $('#orderResult').html(data);
                        $('#orderResult').slideDown();
                        $('.sortable').sortable({
                            update: function(){
                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo current_url() ?>',
                                    dataType: 'json',
                                    data: $(this).sortable('serialize'),
                                    success: function(){
                                    }
                                });
                            }
                        });
                    }
                });
            }
		
	});
});
</script>