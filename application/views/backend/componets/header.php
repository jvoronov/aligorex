<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?=base_url()?>"> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>CMS</title>
    <?php $this->assets->css();?>
    <?php $this->assets->js();?>
    <script> var base = $('base').attr('href'); </script>
</head>
<body>
    