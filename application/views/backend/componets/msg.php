<div class="row">
    <div class="col-lg-12">
    <?php
    $success = $this->session->flashdata('success');
    if ($success):?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $success; ?>
        </div>
    <?php endif; ?>
        <?php
    $error = $this->session->flashdata('error');
    if ($error):?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $error; ?>
        </div>
    <?php endif; ?>
         <?php
    $warning = $this->session->flashdata('warning');
    if ($warning):?>
        <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $warning; ?>
        </div>
    <?php endif; ?>
        <?php if ($this->session->flashdata('errors')): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('errors'); ?>
            </div>
        <?php endif; ?>
    </div>
</div>