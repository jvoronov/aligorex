<nav class="navbar navbar-default">
    <div class="navbar-header">
        <p class="navbar-brand"><?=lang('gallery_manage_pic')?> - <span style="color: green"><?php echo $records->title;?></span></p>
    </div>
</nav>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <?php echo form_open_multipart('', array('id' => 'upload_file','gallery_id'=>$records->id)); ?>
    <div class="col-lg-4">
        <label><?=lang('gallery_pic')?></label>
        <input type="file" name="userfile" id="userfile" class="form-control" /><br>
        <div class="alert" id="msg" style="display: none"></div>
    </div>
     <div class="col-lg-2"><br>
         <?php echo form_submit(array('name' => 'submit', 'class' => 'btn btn-success'), lang('add')); ?>
    </div>
    <?php echo form_close(); ?>
    <?php echo form_open('admin/gallery/add_link', array('id' => 'add_link','gallery_id'=>$records->id)); ?>
    <div class="col-lg-4">
        <label><?=lang('gallery_video_link')?></label>
        <?php echo form_input(array('name' => 'img', 'class' => 'form-control', 'id' => 'link', 'value' => set_value('link'))); ?><br>
        <?php echo form_hidden('gallery_id',  $records->id); ?>
        <?php echo form_hidden('is_video', 1); ?>
        <?php echo form_hidden( 'status', 0); ?>
        <div class="alert" id="msg2" style="display: none"></div>
    </div>
    <div class="col-lg-2">
        <br>
        <?php echo form_submit(array('name' => 'add_link', 'class' => 'btn btn-info'), lang('add'));?>
    </div>
     <?php echo form_close(); ?>
</div>
<div id="files"></div>
<script>
    var gallery_id = $('#upload_file').attr('gallery_id');
    $.get(base + 'admin/gallery/images_control/' +  gallery_id )
        .success(function (data){
            $('#files').html(data);
        });
    $('#upload_file').submit(function(e) {
        e.preventDefault();
        $.ajaxFileUpload({
            url             : base + 'admin/gallery/ajax_upload/' + gallery_id, 
            secureuri       :false,
            fileElementId   :'userfile',
            dataType        : 'json',
            success : function (data,status)
            {
                if(data.status == 'error'){
                    $('#msg').addClass('alert-danger');
                }else{
                    $('#msg').addClass('alert-success');
                    $.get(base + 'admin/gallery/images_control/' + gallery_id)
                        .success(function (data){
                            $('#files').html(data);
                    });
                }
                $('#msg').text(data.msg);
                $('#msg').show('slow');
                $('#msg').fadeOut(5000, function() {
                    $('#msg').remove();
                });
            }
        });
        return false;
    });
     $("#add_link").submit(function(e){
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        var gallery_id = $(this).attr("gallery_id");
        $.ajax({
            url : formURL,
            type: "POST",
            data : postData,
            success:function(data){
              if(data.error == 0){
                  $( "#link" ).val('');
                  $.get(base + 'admin/gallery/images_control/' + gallery_id).success(function (data){$('#files').html(data); });
                   $('#msg2').removeClass('alert-danger');
                  $('#msg2').addClass('alert-success');
              }else{
                  $('#msg2').addClass('alert-danger');
              }
              $('#msg2').text(data.msg);
              $('#msg2').show('slow');
              return false;
            },
        });
        e.preventDefault();
        e.unbind(); 
    });
</script>
    