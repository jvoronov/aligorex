<nav class="navbar navbar-default">
    <div class="navbar-header">
        <p class="navbar-brand"><?=lang('gallery_add')?></p>
    </div>
</nav>
<?php echo form_open_multipart(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active;?>">
                    <a href="#<?php echo $item->lang;?>" data-toggle="tab">
                        <?php echo $item->description;?>
                    </a>
                </li>
          <?php endforeach;?>
        </ul>
        <div class="tab-content">
             <?php foreach ($this->langs as $item):
                  $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                    <div class="tab-pane <?php echo $active;?>" id="<?php echo $item->lang;?>"><br>
                        <label for="title"><?=lang('name')?></label>
                        <?php echo form_input(array('name' => "tf[title][{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => lang('title'), 'value' => set_value("tf[title][{$item->lang}]"))); ?><br>
                        <?php  $data['lang'] = $item->lang; $this->load->view('backend/seo/create',$data); ?>
                    </div>
            <?php  endforeach;?>
             
        </div>
    </div>
    <div class="col-lg-6">
        <label for="url"><?=lang('url')?></label>
        <?php echo form_input(array('name' => 'url', 'class' => 'form-control', 'id' => 'url', 'value' => set_value('url'))); ?><br>
        <label><?=lang('shownot')?></label>
        <?php echo form_dropdown('status', array(0 => 'Не отображать', 1 => 'Отображать'), set_value('status', 0), 'class="form-control"'); ?><br>
        <label><?=lang('gallery_pic')?></label>
        <?php echo form_upload(array('name' => "file", 'class' => 'form-control', 'id' => 'fileInput', 'value' => set_value("file"))); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?php echo form_submit(array('name'=>'submit','class'=>'btn btn-success'), lang('create'));?>
    </div>
</div>
<?php echo form_close(); ?>