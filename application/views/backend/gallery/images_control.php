<div class="row"><hr>
    <?php if($records):?>
       <ol class="sort ">
        <?php foreach($records as $item):?>
            <li id="list_<?php echo $item->id;?>">
                <div>
                     <?php $item->is_video == 0?$img ='uploads/gallery/'.$item->img:$img = youtube_data($item->img)?>
                     <img src="<?php echo $img;?>">
                     <span id="<?php echo $item->id;?>"class="btn btn-xs btn-danger pull-right delete_file_link"><i class="fa fa-trash-o fa-fw"></i></span>
                     <?php $item->status == 1?$active = 'btn-success':$active = 'btn-warning'?>
                     <span id="<?php echo $item->id;?>" class="btn btn-xs pull-right on-off <?php echo $active;?> <?php echo $item->id;?>" style="margin-right:5px;" status="<?php echo $item->status;?>"><i class="fa fa-power-off fa-fw"></i></span>
                     <?php if($item->is_video == 1):?>
                     <a href="<?php echo $item->img?>" target="_blank" class="btn btn-xs pull-right btn-info" style="margin-right:5px;" status="<?php echo $item->status;?>"><i class="fa fa-eye fa-fw"></i></a>
                     <?php endif;?>
                 </div>
             </li>
        <?php endforeach;?>
       </ol> 
     <?php endif;?>
</div>
<script>
     $(".delete_file_link").click(function() {
        var img_id = $(this).attr('id');
        if (confirm(<?php echo lang('gallery_pic_del_confirm')?>)) {
            atr = {
            type: "POST",
            url: base + 'admin/gallery/delete_image/' + img_id, 
            success: function() {
                     $('#list_'+img_id).fadeOut(500, function() {
                         $('#list_'+img_id).remove();
                    });
            }
        };
        $.ajax(atr);
        return false;
        }
     });
      $(".on-off").click(function() {
        var status = $(this).attr('status');
        var oldClass = 'btn-success';
        var NewClass = 'btn-warning';
        var img_id = $(this).attr('id');
        if(status == 0){
            status = 1;
            oldClass = 'btn-warning';
            NewClass = 'btn-success';
        }else{
            status = 0;
        }
        //alert(oldClass);alert(NewClass);
        atr = {
            type: "POST",
            url: base + 'admin/gallery/update_status/' + img_id + '/' + status, 
            success: function() {
                $('.'+img_id).removeClass(oldClass).addClass(NewClass);
                 $('.'+img_id).attr('status',status);
               
            }
        };
        $.ajax(atr);
        return false;
     });
     $('ol.sort').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            maxLevels: 1,
            update : function () {
               $.ajax({
                type: 'post',
                url: base + 'admin/gallery/img_order/', 
                data: $(this).nestedSortable('serialize')
             });             
            }
        });
</script>
