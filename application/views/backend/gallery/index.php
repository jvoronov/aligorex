<nav class="navbar navbar-default">
    <div class="navbar-header">
        <p class="navbar-brand"><?=lang('gallery')?></p>
    </div>
</nav>
<div class="row">
    <div class="col-lg-3">
        <?php echo anchor('admin/gallery/create', '<i class="fa fa-film fa-fw"></i> Создать галерею', 'class="btn btn-success"'); ?>
    </div>
    <div class="col-lg-3">
        <?php echo anchor('admin/gallery/order', '<i class="fa fa-list-ol fa-fw"></i> Сортировка', 'class="btn btn-info"'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12"><hr>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><?=lang('review')?></h4>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <?php if ($records): ?>
                    <thead>
                        <tr class="info">
                            <th class="text-center"><h4><?=lang('name')?></h4></th>
                            <th class="text-center"><h4><?=lang('gallery_pic')?></h4></th>
                            <th class="text-center"><h4><?=lang('display')?></h4></th>
                            <th class="text-center"><h4><?=lang('options')?></h4></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $gallery): ?>
                            <tr class="odd gradeX">
                                <td><strong><?php echo $gallery->title; ?></strong></td>
                                <td class="text-center">
                                    <?php if ($gallery->gallery_img):?>
                                    <img src="uploads/categories_img/<?php echo $gallery->gallery_img;?>" class="img-thumbnail" style="width: 50px">
                                    <?php else: ?>
                                          <b><span class="text-warning"> <?=lang('no_records')?></span></b> 
                                     <?php endif; ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($gallery->status == 0): ?>
                                        <button type="button" class="btn btn-danger btn-sm"> <?=lang('not_show')?> </button>
                                    <?php else: ?>
                                        <button type="button" class="btn btn-success btn-sm"> <?=lang('show')?> </button>
                                    <?php endif; ?> 
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-success btn-sm" href="<?php echo site_url('admin/gallery/edit/' . $gallery->id); ?>" title="<?=lang('edit')?>">
                                        <i class="fa fa-pencil fa-fw"></i>
                                    </a>
                                    <a class="btn btn-info btn-sm" href="<?php echo site_url('admin/gallery/edit_img/' . $gallery->id); ?>" title="<?=lang('gallery_manage_pic')?>">
                                        <i class="fa fa-plus fa-fw"></i>
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="<?php echo site_url('admin/gallery/delete/' . $gallery->id); ?>" title="<?=lang('delete')?>" onclick="return confirm(<?php echo lang('del_confirm')?>)">
                                        <i class="fa fa-trash-o fa-lg"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <?php else: ?>
                            <div class="alert alert-warning"> <?=lang('no_records')?> </div>
                    </tbody>  
                <?php endif; ?>
                </table>
            </div>
        </div>
    </div>
</div>