<nav class="navbar navbar-default">
    <div class="navbar-header">
        <p class="navbar-brand"><?=lang('order')?></p>
    </div>
</nav>
<div class="row">
    <div class="col-lg-12 ">
        <section>
            <div id="orderResult"></div>
            <input type="button" id="save" value="Сохранить" class="btn btn-success" />
        </section>
    </div>
</div>
<script>
$(function() {
    
	$.post('<?php echo site_url('admin/gallery/order_ajax/'); ?>', {}, function(data){
		$('#orderResult').html(data);
                
	});

	$('#save').click(function(){
		oSortable = $('.sortable').nestedSortable('toArray');

		$('#orderResult').slideUp(function(){
			$.post('<?php echo site_url('admin/gallery/order_ajax/'); ?>', { sortable: oSortable }, function(data){
				$('#orderResult').html(data);
				$('#orderResult').slideDown();
			});
		});
		
	});
});
</script>
