<?php $this->load->view('backend/pages/sub_nav'); ?>
<?=form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active;?>"><a href="#<?php echo $item->lang;?>" data-toggle="tab"><?php echo $item->description;?></a></li>
          <?php endforeach;?>
        </ul>
        <div class="tab-content">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                <div class="tab-pane <?php echo $active; ?>" id="<?php echo $item->lang; ?>"><br>
                    <label for="title">Заголовок страницы</label>
                    <?php echo form_input(array('name' => "tf[title][{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название', 'value' => set_value("tf[title][{$item->lang}]"))); ?><br>
                    <label>Описание страницы</label>
                    <?php echo $this->ckeditor->editor("tf[content][{$item->lang}]", html_entity_decode(set_value("tf[content][{$item->lang}]"))); ?><br>
                    <?php  $data['lang'] = $item->lang; $this->load->view('backend/seo/create',$data); ?>
                </div>
            <?php endforeach; ?>
        </div>
        <label for="url">Адрес страницы (url)</label>
        <?php echo form_input(array('name' => 'url', 'required' => '','class' => 'form-control', 'id' => 'url', 'value' => set_value('url'))); ?><br>
        <label>Отображать / Не отображать</label>
        <?php echo form_dropdown('status', array('0'=>'Не отображать','1'=>'Отображать'), '0' , 'class="form-control"');?><br><hr>
    </div>
</div>
<?php if($categories):?>
    <div class="row">
        <div class="col-lg-12 text-center">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <p class="navbar-brand">Добавить в категорию страниц</p>
                </div>
            </nav>
        </div>
    </div>
    <div class="row">
        <?php foreach ($categories as $item):?>
            <div class="col-lg-3">
                <div class="checkbox"> 
                    <label>
                        
                        <input type="checkbox" name="mycheck[]"  value="<?php echo $item['id']?>" <?php echo set_checkbox('mycheck[]',$item['id']); ?>  />
                        <h4><?php echo $item['title']?></h4>
                    </label>
                </div>
                <?php if (isset($item['children']) && count($item['children'])):?>
                    <?php foreach ($item['children'] as $item):?>
                        <div class="checkbox" style="margin-left:20px"> 
                        <label>
                        
                            <input type="checkbox" name="mycheck[]"  value="<?php echo $item['id']?>" <?php echo set_checkbox('mycheck[]',$item['id']); ?>  />
                            <h4><?php echo $item['title']?></h4>
                        </label>
                </div>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
        <?php endforeach;?>
    </div>
<?php endif;?>
<hr>
<div class="row">
    <div class="col-lg-6">
        <?php echo form_hidden('module_id', $this->modules_data->id);?><br>
        <?php echo form_submit(array('name'=>'submit','class'=>'btn btn-success'), 'Создать страницу');?>
    </div>
</div><hr>
<?= form_close(); ?>