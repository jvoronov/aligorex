<?php $this->load->view('backend/languages/sub_nav'); ?>
<?=form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
         <label for="title">Название языка</label>
         <?php echo form_input(array('name' => "title", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название', 'value' =>  set_value("title"))); ?><br>
         <label for="lang">Код языка</label>
         <?php echo form_input(array('name' => "lang", 'class' => 'form-control', 'id' => 'lang', 'placeholder' => 'Введите название', 'value' =>  set_value("lang"))); ?><br>
         <?php echo form_submit(array('name'=>'edit','class'=>'btn btn-success'), 'Добавить язык');?>
    </div>
</div>
<?=form_close();?>