<?php $this->load->view('backend/languages/sub_nav'); ?>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <p class="navbar-brand">Редактирование языка - <span style="color: green"><?php echo $records->description?></span></p>
        </div>
    </div>
</nav>
<?=form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
         <label for="title">Название языка</label>
         <?php echo form_input(array('name' => "title", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название', 'value' => $this->input->post('edit') ? set_value("title") : $records->description)); ?><br>
         <label for="lang">Код языка</label>
         <?php echo form_input(array('name' => "lang", 'class' => 'form-control', 'id' => 'lang', 'placeholder' => 'Введите название', 'value' => $this->input->post('edit') ? set_value("lang") : $records->lang)); ?><br>
         <?php if($records->lang != $this->langs_db->default_language):?>
            <label>Сделать основным:</label>
            <input type="checkbox" name="default"  value="<?php echo $records->id?>" <?php echo set_checkbox('default',$records->id); ?>  /><br>
            <label>Отображать / Не отображать</label>
            <?php echo form_dropdown('status', array('0'=>'Не отображать','1'=>'Отображать'), $records->status , 'class="form-control"');?><br>
         <?php endif;?>
         <?php echo form_submit(array('name'=>'edit','class'=>'btn btn-success'), 'Сохранить изменения');?>
    </div>
</div>
<?=form_close();?>