<?php $this->load->view('backend/emails/sub_nav'); ?>
<?php echo form_open(current_url());?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active; ?>">
                    <a href="#<?php echo $item->lang; ?>" data-toggle="tab">
                        <?php echo $item->description; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="tab-content">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->langs_db->default_language ? $active = 'active' : $active = ''; ?>
                <div class="tab-pane <?php echo $active; ?>" id="<?php echo $item->lang; ?>"><br>
                    <label for="title">Заголовок письма</label>
                    <?php echo form_input(array('name' => "tf[title][{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название', 'value' => set_value("tf[title][{$item->lang}]", $tf[$item->lang]->title))); ?><br>
                    <label for="content">Содержимое письма</label>
                    <?php echo $this->ckeditor->editor("tf[content][{$item->lang}]", html_entity_decode(set_value("tf[content][{$item->lang}]", $tf[$item->lang]->content))); ?><br>
                </div>
        <?php endforeach; ?>
        </div>
    </div>
</div><hr>
<div class="row">
    <div class="col-lg-6">
        <h3>Специальные опции</h3>
        <?php if($tf[$this->site_lang]->options){
            echo  $tf[$this->site_lang]->options;
        }?>
    </div>
</div><hr>
<div class="row">
    <div class="col-lg-6">
        <?php echo form_submit(array('name'=>'submit','class'=>'btn btn-success'), 'Сохранить изменения');?><hr>
    </div>
</div>
<?php echo form_close(); ?>