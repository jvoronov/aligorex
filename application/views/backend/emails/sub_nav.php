<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php echo anchor('admin/emails/index', 'Управление рассылкой почты', 'class="navbar-brand"'); ?>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <?php if( $tf):?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $tf[$this->site_lang]->description;?><span class="caret"></span></a>
                    <?php else:?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Выберите шаблон <span class="caret"></span></a>
                    <?php endif;?>
                    <ul class="dropdown-menu" role="menu">
                        <?php foreach ($tpl as $value): ?>
                            <li><?php echo anchor('admin/emails/index/'.$value->id, $value->description); ?></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>