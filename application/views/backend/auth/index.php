<?php $this->load->view('backend/auth/sub_nav'); ?>
<div class="row">
    <div class="col-lg-12 ">
        <?php
        if ($message):?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 ">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Управление пользователями</h4>
            </div>
            <?php if ($users): ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr class="info">
                                <th class="text-center"><h4><?php echo lang('index_fname_th');?></h4></th>
                                <th class="text-center"><h4><?php echo lang('index_lname_th');?></h4></th>
                                <th class="text-center"><h4><?php echo lang('index_email_th');?></h4></th>
                                <th class="text-center"><h4><?php echo lang('index_groups_th');?></h4></th>
                                <th class="text-center"><h4><?php echo lang('index_status_th');?></h4></th>
                                <th class="text-center"><h4><?php echo lang('index_action_th');?></h4></th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php foreach ($users as $user): ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $user->first_name; ?></td>
                                        <td><?php echo $user->last_name; ?></td>
                                        <td><?php echo $user->email; ?></td>
                                        <td>
                                            <?php foreach ($user->groups as $group): ?>
                                            <span class="label label-primary" style="margin-left: 5px">
                                                <?php echo anchor("admin/auth/edit_group/" . $group->id, $group->name,'style="color:white;"'); ?>
                                             </span>
                                            <?php endforeach ?>
                                        </td>
                                        <td>
                                            <?php 
                                            if($user->active){
                                                echo anchor("admin/auth/deactivate/" . $user->id, lang('index_active_link'),'class="btn btn-success btn-sm"');}
                                            else{
                                                echo anchor("admin/auth/activate/" . $user->id, lang('index_inactive_link'),'class="btn btn-danger btn-sm"');}?>
                                        </td>
                                        <td>
                                             <a class="btn btn-success btn-sm" href="<?php echo site_url('admin/auth/edit_user/'.$user->id);?>" title="Редактировать">
                                                 <i class="fa fa-pencil fa-fw"></i>
                                            </a>
                                            <a class="btn btn-danger btn-sm" href="<?php echo site_url('/admin/auth/delete_user/'.$user->id);?>" title="Удалить" onclick="return confirm('Вы действительно хотите удалить запись?')">
                                            <i class="fa fa-trash-o fa-lg"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                            <div class="alert alert-warning">
                                Польователей нет
                            </div>
                        <?php endif; ?>
                    </tbody>   
                </table>
            </div>
        </div>
    </div>
</div>