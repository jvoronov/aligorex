<?php $this->load->view('backend/auth/sub_nav'); ?>
<div class="row">
    <div class="col-lg-12 ">
        <?php if ($message):?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 ">
        <?php echo form_open("admin/auth/create_group");?>

              <p>
                    <?php echo lang('create_group_name_label', 'group_name');?> <br />
                    <?php echo form_input($group_name);?>
              </p>

              <p>
                    <?php echo lang('create_group_desc_label', 'description');?> <br />
                    <?php echo form_input($description);?>
              </p>

              <p><?php echo form_submit('submit', lang('create_group_submit_btn'),'class="btn btn-success"');?></p>

        <?php echo form_close();?>
    </div>
</div>