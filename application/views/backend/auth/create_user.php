<?php $this->load->view('backend/auth/sub_nav'); ?>
<div class="row">
    <div class="col-lg-12 ">
        <?php if ($message):?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 ">
        <?php echo form_open("admin/auth/create_user");?>

            <p>
                  <?php echo lang('create_user_fname_label', 'first_name');?> <br />
                  <?php echo form_input($first_name);?>
            </p>

            <p>
                  <?php echo lang('create_user_lname_label', 'last_name');?> <br />
                  <?php echo form_input($last_name);?>
            </p>

            <p>
                  <?php echo lang('create_user_company_label', 'company');?> <br />
                  <?php echo form_input($company);?>
            </p>

            <p>
                  <?php echo lang('create_user_email_label', 'email');?> <br />
                  <?php echo form_input($email);?>
            </p>

            <p>
                  <?php echo lang('create_user_phone_label', 'phone');?> <br />
                  <?php echo form_input($phone);?>
            </p>

            <p>
                  <?php echo lang('create_user_password_label', 'password');?> <br />
                  <?php echo form_input($password);?>
            </p>

            <p>
                  <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
                  <?php echo form_input($password_confirm);?>
            </p>


            <p><?php echo form_submit('submit', lang('create_user_submit_btn'),'class="btn btn-success"');?></p>

      <?php echo form_close();?>
    </div>
</div>