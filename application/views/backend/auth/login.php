
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Панель управления сайтом</title>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/login.css');?>" rel="stylesheet">
</head>
<body>
    <div class="container">
        <?php echo form_open(current_url(),array('class' => 'form-signin','role' => 'form'));?>
            <h4 class="form-signin-heading">Вход в панель администратора</h4>
            <?php echo form_input($identity);?>
            <?php echo form_input($password);?>
            <label class="checkbox">
               <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
               <?php echo lang('login_remember_label', 'remember');?>
            </label>
            <?php echo form_submit('submit', lang('login_submit_btn'),'class="btn btn-lg btn-primary btn-block"');?><br>
            <?php if($message):?>
                <div class="alert alert-danger alert-dismissable">
                    <?php echo $message;?>
                </div>
            <?php endif;?>
        <?php echo form_close();?>
                
    </div>
    <script src="<?php echo base_url('assets/js/jquery-1.10.2.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</body>
</html>   