<?php $this->load->view('backend/auth/sub_nav'); ?>
<div class="row">
    <div class="col-lg-12 ">
        <p><b><span class="text-danger"><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></span></b></p>

        <?php echo form_open("admin/auth/deactivate/".$user->id);?>

          <p>
                <?php echo lang('deactivate_confirm_y_label', 'confirm');?>
            <input type="radio" name="confirm" value="yes" checked="checked" />
            <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
            <input type="radio" name="confirm" value="no" />
          </p>

          <?php echo form_hidden($csrf); ?>
          <?php echo form_hidden(array('id'=>$user->id)); ?>

          <p><?php echo form_submit('submit', lang('deactivate_submit_btn'),"class='btn btn-warning'");?></p>

        <?php echo form_close();?>
    </div>
</div>