<?php $this->load->view('backend/attributes/sub_nav'); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h4 class="text-center">Редактирование атрибута <b>"<?php echo $records->title?>"</b></h4>
    </div>
</div><hr>
<?php echo form_open_multipart(current_url()); ?>
<div class="row">
    <div class="col-lg-4">
         <label for="title">Название атрибута</label>
        <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->config->item('site_default_lang') ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active;?>"><a href="#<?php echo $item->lang;?>" data-toggle="tab"><?php echo $item->description;?></a></li>
          <?php endforeach;?>
        </ul>
        <div class="tab-content">
             <?php foreach ($translated_fields as $item):
                  $item->lang == $this->config->item('site_default_lang') ? $active = 'active' : $active = '';?>
                <div class="tab-pane <?php echo $active;?>" id="<?php echo $item->lang;?>"><br>
                    <?php echo form_input(array('name' => "title[{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название', 'value' => $this->input->post('add') ? set_value("title[{$item->lang}]") : $item->title)); ?><br>
                   
                </div>
            <?php endforeach;?>
         </div>
        <label>Изображение</label><br>
        <?php if($records->img):?>
            <p><img src="uploads/categories_img/<?php echo $records->img;?>" class="img-thumbnail"></p>
        <?php endif;?>
        <input type="file" name="file"/><br>
        <label>Отображать / Не отображать</label>
        <?php echo form_dropdown('status', array('0'=>'Не отображать','1'=>'Отображать'), $this->input->post('add') ? set_value('status') : $records->status , 'class="form-control"');?><br>
        <?php echo form_submit(array('name' => 'add', 'class' => 'btn btn-success','value'=>'Сохранить')); ?>
        <a class="btn btn-info" href="<?php echo site_url('/admin/attributes/values/'.$this->uri->segment(4));?>" title="Добавить значения"> Добавить значения</a>
    </div>
</div><br>
<?= form_close(); ?>