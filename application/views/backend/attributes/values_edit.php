<?php $this->load->view('backend/attributes/sub_nav'); ?>
<?=form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
       <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->config->item('site_default_lang') ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active;?>"><a href="#<?php echo $item->lang;?>" data-toggle="tab"><?php echo $item->description;?></a></li>
          <?php endforeach;?>
        </ul>
        <div class="tab-content">
             <?php foreach ($translated_fields as $item):
                  $item->lang == $this->config->item('site_default_lang') ? $active = 'active' : $active = '';?>
                <div class="tab-pane <?php echo $active;?>" id="<?php echo $item->lang;?>"><br>
                    <?php echo form_input(array('name' => "title[{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название', 'value' => $this->input->post('add') ? set_value("title[{$item->lang}]") : $item->title)); ?><br>
                   
                </div>
            <?php endforeach;?>
         </div>
        </div></div>
    <div class="row">
    <div class="col-lg-4">
        <?php echo form_submit(array('name' => 'add', 'class' => 'btn btn-success','value'=>'Редактировать')); ?>
        <a class="btn btn-info" href="<?php echo site_url('/admin/attributes/values/'.$attribute->attributes_id)?>" title="Назад"> Назад</a>
    </div>
        </div>
<br>
<?= form_close(); ?>