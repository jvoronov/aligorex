<?php $this->load->view('backend/attributes/sub_nav'); ?>
<div class="row">
    <div class="col-lg-12 text-center">
        <h4 class="text-center">Управление значениями для атрибута <b style="color:green">"<?php echo $attribute->title?>"</b></h4>
    </div>
</div>
<hr>
<?=form_open(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
       <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->config->item('site_default_lang') ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active;?>"><a href="#<?php echo $item->lang;?>" data-toggle="tab"><?php echo $item->description;?></a></li>
          <?php endforeach;?>
        </ul>
        <div class="tab-content">
             <?php foreach ($this->langs as $item):
                  $item->lang == $this->config->item('site_default_lang') ? $active = 'active' : $active = '';?>
                <div class="tab-pane <?php echo $active;?>" id="<?php echo $item->lang;?>"><br>
                    <?php echo form_input(array('name' => "title[{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название', 'value' => set_value("title[{$item->lang}]"))); ?><br>
                   
                </div>
            <?php  endforeach;?>
        </div>
        <?php echo form_submit(array('name' => 'add', 'class' => 'btn btn-primary','value'=>'Добавить значение')); ?>
    </div>
</div><br>
<?= form_close(); ?>
<div class="row">
        <div class="col-lg-12 ">
           <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Обзор значений атрибута <b style="color:green">"<?php echo $attribute->title?>"</b></h4>
                </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <?php if ($records): ?>
                            <thead>
                                <tr class="info">
                                    <th class="text-center"><h4>Название</h4></th>
                                    <th class="text-center"><h4>Опции</h4></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($records as $record): ?>
                                <tr class="odd gradeX">
                                    <td><strong><?php echo $record->title;?></strong></td>
                                    <td class="text-center">
                                        <a class="btn btn-success btn-sm" href="<?php echo site_url('admin/attributes/values_edit/'.$record->values_id);?>" title="Редактировать">
                                            <i class="fa fa-pencil fa-fw"></i>
                                        </a>
                                        <a class="btn btn-danger btn-sm" href="<?php echo site_url('admin/attributes/values_delete/'.$record->values_id);?>" title="Удалить" onclick="return confirm('Вы действительно хотите удалить запись?')">
                                            <i class="fa fa-trash-o fa-lg"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <?php else: ?>
                                <div class="alert alert-warning">
                                    Значения еще не созданы!
                                </div>
                            <?php endif; ?>
                        </table>
                    </div>
            </div>
        </div>
    </div>