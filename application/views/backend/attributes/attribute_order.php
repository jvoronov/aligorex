<?php $this->load->view('backend/attributes/sub_nav'); ?>
<div class="row">
    <div class="col-lg-12 ">
<section>
	<div id="orderResult"></div>
	<input type="button" id="save" value="Сохранить" class="btn btn-success" />
</section>
</div>
</div>
<script>
$(function() {
    
	$.post('<?php echo site_url('admin/attributes/attribute_order_ajax'); ?>', {}, function(data){
		$('#orderResult').html(data);
                
	});

	$('#save').click(function(){
		oSortable = $('.sortable').nestedSortable('toArray');

		$('#orderResult').slideUp(function(){
			$.post('<?php echo site_url('admin/attributes/attribute_order_ajax'); ?>', { sortable: oSortable }, function(data){
				$('#orderResult').html(data);
				$('#orderResult').slideDown();
			});
		});
		
	});
});
</script>