<?php $this->load->view('backend/product/edit_sub_nav'); ?>
<?php if ($selected_values) $c = $selected_values; else $c = array();?>
<?php echo form_open(current_url());?>
<div class="row">
<?php foreach ($records as $record): ?>
    <div class="col-lg-4">
        <label><?php echo $record->title; ?></label><br>
        <select class="multiselect" multiple="multiple" name="attribute[][<?php echo $record->id; ?>]">
            <?php foreach ($record->values as $value): ?>
                <option  value="<?php echo $value->values_id;?>" <?php if (in_array($value->values_id, $c)) echo "selected";?>>
                       <?php echo $value->title;?>
                    </option>
            <?php endforeach ?>
        </select>
     </div>
<?php endforeach; ?>
</div>
<div class="row">
        <hr>
        <div class="col-lg-3">
            <?php echo form_submit(array('name'=>'add','class'=>'btn btn-success'), 'Сохранить');?>
        </div>
    </div> 
<?php echo form_close();?>
<script type="text/javascript">
  $(document).ready(function() {
    $('.multiselect').multiselect({
      enableCaseInsensitiveFiltering: true
    });
  });
</script>
