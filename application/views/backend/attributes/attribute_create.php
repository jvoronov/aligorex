<?php $this->load->view('backend/attributes/sub_nav'); ?>
<div class="row">
    <div class="col-lg-12 text-center">
        <h3 class="text-center">Добавление нового атрибута</h3>
    </div>
</div>
<hr>
<?php echo form_open_multipart(current_url()); ?>
<div class="row">
    <div class="col-lg-12">
        <?php echo validation_errors( '<div class="alert alert-danger">', '</div>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
         <label for="title">Название атрибута</label>
        <ul class="nav nav-tabs">
            <?php foreach ($this->langs as $item):
                $item->lang == $this->config->item('site_default_lang') ? $active = 'active' : $active = '';?>
                <li class="<?php echo $active;?>"><a href="#<?php echo $item->lang;?>" data-toggle="tab"><?php echo $item->description;?></a></li>
          <?php endforeach;?>
        </ul>
        <div class="tab-content">
             <?php foreach ($this->langs as $item):
                  $item->lang == $this->config->item('site_default_lang') ? $active = 'active' : $active = '';?>
                <div class="tab-pane <?php echo $active;?>" id="<?php echo $item->lang;?>"><br>
                    <?php echo form_input(array('name' => "title[{$item->lang}]", 'class' => 'form-control', 'id' => 'title', 'placeholder' => 'Введите название', 'value' => set_value("title[{$item->lang}]"))); ?><br>
                   
                </div>
            <?php  endforeach;?>
        </div><br>
        <label>Изображение</label>
        <input type="file" name="file"/><br>
        <label>Отображать / Не отображать</label>
        <?php echo form_dropdown('status', array('0'=>'Не отображать','1'=>'Отображать'), '0' , 'class="form-control"');?><br>
        <?php echo form_submit(array('name' => 'add', 'class' => 'btn btn-success','value'=>'Сохранить')); ?>
    </div>
        
</div><br>
<?php echo form_close(); ?>