<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <p class="navbar-brand">Фильтры товаров</p>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php 
                        if($this->uri->segment(2) == 'attributes' && $this->uri->segment(3) == 'index')
                            $active = 'class="active"';
                        else
                           $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/attributes/index');?>">Обзор</a></li>
                 <?php
                        if($this->uri->segment(2) == 'attributes' && $this->uri->segment(3) == 'attribute_create')
                            $active = 'class="active"';
                        
                        else
                           $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/attributes/attribute_create');?>">Добавить</a></li>
                <?php
                        if($this->uri->segment(2) == 'attributes' && $this->uri->segment(3) == 'attribute_order')
                            $active = 'class="active"';
                        
                        else
                           $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/attributes/attribute_order');?>">Сортировать</a></li>
            </ul>
            <form class="navbar-form navbar-right" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Поиск" disabled>
                </div>
                <button type="submit" class="btn btn-default" disabled>Найти</button>
            </form>
        </div>
    </div>
</nav>