<?php
if($records)
    echo get_ol($records);
else
    echo '<div class="alert alert-warning">Атрибуты еще не созданы!</div>';
function get_ol ($array, $child = FALSE)
{
	$str = '';
	
	if (count($array)) {
		$str .= '<ol class="sortable">';
		
		foreach ($array as $item) {
                    if ($item['status'] == 0)
                        $disabled = "<span style='color:red;float:right'> Внимание атрибут отключен и не показывается!</span>";
                    else {
                         $disabled = "";
                    }
			$str .= '<li id="list_' . $item['id'] .'">';
			$str .= '<div>' . $item['title'].$disabled .'</div>'; 
			
			$str .= '</li>' . PHP_EOL;
		}
		
		$str .= '</ol>' . PHP_EOL;
	}
	
	return $str;
}
?>

<script>
$(function() {

        $('ol.sortable').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            maxLevels: 1
        });

    });
</script>