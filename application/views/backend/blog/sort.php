<?php $this->load->view('backend/pages/sub_nav'); ?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <?php 
            $sorted = $this->session->flashdata('sorted');
            if($sorted):?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $sorted;?>
            </div>
            <?php endif;?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            <select class="form-control" name="category" id="categories">   
                <option></option>
                <?php 
                foreach($categories as $item){
                    ?>
                    <option value="<?=$item['id']?>"><?=$item['title']?></option>
                <?php
                foreach($item['children'] as $child){
                    ?>
                    <option value="<?=$child['id']?>"><?=$child['title']?></option>
                    <?php
                }
                }
                ?>
            </select>
        </div>
        <div class="col-lg-2">
            <input type="button" name="get_items" id="get_items" value="Показать" class="btn btn-info" />
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
        <div id="orderResult">
        </div>
            </div>
    </div>
</div>
<?php $this->load->view('backend/componets/sortable_js') ?>
   