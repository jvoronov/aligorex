<?php $this->load->view('backend/blog/sub_nav'); ?>
<div class="row">
        <div class="col-lg-12 ">
           <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Обзор записей блога</h4>
                </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <?php if($records):?>
                            <thead>
                                <tr class="info">
                                    <th class="text-center"><h4>Заголовок записи</h4></th>
                                    <th class="text-center"><h4>Отображаение</h4></th>
                                    <th class="text-center"><h4>Создана</h4></th>
                                    <th class="text-center"><h4>Редактирована</h4></th>
                                    <th class="text-center"><h4>Опции</h4></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($records as $record): ?>
                                <tr class="odd gradeX">
                                    <td><strong><?php echo $record->title;?></strong></td>
                                    <td class="text-center">
                                        <?php if($record->status == 0):?>
                                           <button type="button" class="btn btn-danger btn-sm"> Не отображается </button>
                                        <?php else:?>
                                            <button type="button" class="btn btn-success btn-sm"> Отображаетася </button>
                                        <?php endif;?> 
                                    </td>
                                    <td><?php echo $record->created;?></td>
                                    <td><?php echo $record->modified;?></td>
                                    <td class="text-center">
                                        <a class="btn btn-success btn-sm" href="<?php echo site_url('admin/blog/edit/'.$record->id);?>" title="Редактировать">
                                            <i class="fa fa-pencil fa-fw"></i>
                                        </a>
                                        <a class="btn btn-danger btn-sm" href="<?php echo site_url('admin/blog/delete/'.$record->id);?>" title="Удалить" onclick="return confirm('Вы действительно хотите удалить запись?')">
                                            <i class="fa fa-trash-o fa-lg"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <?php else: ?>
                                <div class="alert alert-warning">
                                    Записи блога еще не созданы!
                                </div>
                           <?php endif;?>
                        </table>
                    </div>
            </div>
        </div>
    </div>
<div class="row">
    <div class="col-lg-12">
        <?php echo $pagination; ?>
    </div>
</div