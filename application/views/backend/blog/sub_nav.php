<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="glyphicon glyphicon-chevron-down"></span>
            </button>
            <p class="navbar-brand">Управление блогом сайта</p>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php $this->uri->segment(3) == 'index' ? $active = 'class="active"' : $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/blog/index');?>">Обзор</a></li>
                <?php $this->uri->segment(3) == 'create' ? $active = 'class="active"' : $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/blog/create');?>">Добавить запись</a></li>
                 <?php $this->uri->segment(3) == 'sort' ? $active = 'class="active"' : $active = '';?>
                <li <?php echo $active?>><a href="<?php echo base_url('admin/blog/sort');?>">Сортировка</a></li>
            </ul>
            <form class="navbar-form navbar-right" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Поиск" disabled>
                </div>
                <button type="submit" class="btn btn-default" disabled>Найти</button>
            </form>
        </div>
    </div>
</nav>