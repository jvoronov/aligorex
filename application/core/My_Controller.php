<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Base_Controller extends CI_Controller {

    public $data = array();
    public $site_lang;
    public $tpl;
    public $langs = array();

    public function __construct() {
        parent::__construct();
        $this->load->library(array('assets', 'settings_lib'));
        $this->langs = $this->lang->languages_data;
        $this->site_lang = $this->get_site_lang();
        $this->load->helper(array('url', 'form', 'language', 'func'));
        $this->assets->add(array('css' => array('css/bootstrap.min.css'), 'js' => array('js/jquery-1.10.2.js', 'js/bootstrap.min.js')));
        $this->lang->load('from_db', $this->site_lang);
        //$this->output->enable_profiler(true);
    }

    public function get_site_lang() {
        $exteptions = array('admin', FALSE);
        $uri_segment = $this->uri->segment(1);
        return in_array($uri_segment, $exteptions) || !in_array($uri_segment, $this->lang->languages) ?  $this->lang->def_lang : $uri_segment;
    }

}

class Frontend_Controller extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('cart', 'ion_auth', 'seo_lib'));
        $this->assets->add(array('css' => array('css/frontend.css',)));
        $this->load->model('navigation_model', 'navigation', TRUE);
        $this->data['settings'] = $this->settings_lib->get_settings('site_main');
        $this->data['seo'] = $this->seo_lib->make_seo();
        $this->tpl = 'frontend/tpl';
        method_exists($this->router->class,$this->router->method)? $this->data['main'] = 'frontend/' . $this->router->class . '/' . $this->router->method: redirect(site_url('404'));
    }

}

class Backend_Controller extends Base_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'session', 'pagination', 'ion_auth',));
        $exception_uris = array('admin/auth/login', 'admin/auth/logout');
        if (in_array(uri_string(), $exception_uris) == FALSE) {
            if (!$this->ion_auth->is_admin())
                redirect('admin/auth/login');
        }
        $this->langs = $this->lang->all_languages_data;
        $this->site_lang = $this->lang->def_lang;
        $this->lang->load('backend');
        $this->config->load('routs_db', TRUE);
        $this->assets->add(array('css' => array('third_party/font-awesome/css/font-awesome.min.css', 'css/sb-admin.css', 'css/backend.css',), 'js' => array('js/jquery.metisMenu.js', 'js/sb-admin.js')));
        $this->tpl = 'backend/tpl';
        $this->data['main'] = 'backend/' . $this->router->class . '/' . $this->router->method;
    }

}
