<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails_model extends BF_Model {
    
    protected $table_name = 'emails';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    
    protected $validation_rules = array(
    );
    
    protected $translated_vr = array(
        array('tf[title]', 'Заголовок письма на языке', 'required|min_length[3]|max_length[200]|xss_clean'),
        array('tf[content]', 'Содержимое письма на языке', 'required|min_length[3]'),
    );
    
    public function record_exist($id) {
        if($this->find_by('id', $id) == FALSE){
            return false;
        }
        return TRUE;
    }
    
    public function get_template_settings($key) {
        $this->db->select('content,title,key');
        $this->db->join($this->translate_table, "{$this->table_name}.id = {$this->translate_table}.table_id",'left');
        $q = $this->db->get_where($this->table_name, array('key' => $key,'lang'=>$this->site_lang)); 
        return $q->row_object();
    }
}