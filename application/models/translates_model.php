<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Translates_model extends BF_Model {

    protected $table_name = 'translates';
    protected $return_insert_id = TRUE;
    protected $return_type = 'object';
    protected $soft_deletes = FALSE;
    protected $set_created = FALSE;
    protected $set_modified = FALSE;
    protected $validation_rules = array(
        array('field' => 'nt[key]', 'label' => 'Уникальный ключ', 'rules' => 'required|trim|callback_vaidate_key|min_length[3]|max_length[250]|is_unique[translates.key]|xss_clean'),
    );
    protected $translated_vr = array(
        array('tf[value]', 'перевод на языке', 'required|xss_clean'),
    );

    public function prep_data($post_data) {
        $data['key'] = $post_data['key'];
        return $data;
    }

    public function prep_data_t($table_id, $post_data) {
        $insert = array();
        foreach ($post_data as $key => $value) {
            foreach ($value as $k => $v) {
                $insert[$k]['table_id'] = $table_id;
                $insert[$k]['lang'] = $k;
                $insert[$k][$key] = $v;
            }
        }
        return array_values($insert);
    }

    public function insert_t(array $data) {
        $this->db->insert_batch($this->translate_table, $data);
    }

    /* public function get_all_translates() {
      $this->db->select('t.*,(SELECT GROUP_CONCAT(tt.value)FROM translates_translate tt  WHERE t.id = tt.table_id) AS val,(SELECT GROUP_CONCAT(tt.lang) FROM translates_translate tt WHERE t.id = tt.table_id) AS lang');
      // $this->db->limit(1);
      $q = $this->db->get('translates t ');
      return $q->result_object();
      } */

    public function get_all_translates($limit, $offset) {
        $this->db->select('t.*,tt.value,tt.lang');
        if ($word = $this->input->post('word')) {
            $this->db->join('translates_translate tt', "t.id = tt.table_id", 'left');
            $this->db->like('tt.value', $word);
        } else {
            $this->db->join('translates_translate tt', "t.id = tt.table_id and tt.lang = '$this->site_lang'", 'left');
        }
        $this->db->limit($limit);
        $this->db->offset($offset);
        $this->db->order_by('t.id', 'desc');
        $q = $this->db->get('translates t ');
        return $q->result_object();
    }

    public function get_single_translate($id) {
        //$this->db->select('t.*,tt.value,tt.lang');
        $this->db->select('t.id t_id,t.key,tt.value,tt.lang,tt.id tt_id');
        $this->db->join('translates_translate tt', "t.id = tt.table_id", 'left');
        $q = $this->db->get_where('translates t', array('t.id' => $id));
        return return_assoc_array($q->result_object(), 'lang');
    }

    public function files_to_db($data) {
        foreach ($data as $key => $value) {
            $id = $this->insert(array('key' => $key));
            foreach ($value as $k => $v) {
                $this->db->insert('translates_translate', array('lang' => $k, 'value' => $v, 'table_id' => $id));
            }
        }
    }

}

/*
 * 
 SELECT t.*,(SELECT GROUP_CONCAT(tt.value)FROM translates_translate tt  WHERE t.id = tt.table_id) AS val,(SELECT GROUP_CONCAT(tt.lang) FROM translates_translate tt WHERE t.id = tt.table_id) AS lang
FROM translates t 
 */