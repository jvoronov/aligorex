<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Seo_model extends BF_Model {

    protected $table_name = 'seo';
    protected $return_insert_id = TRUE;
    protected $return_type = 'object';
    protected $soft_deletes = FALSE;
    protected $set_created = FALSE;
    protected $set_modified = FALSE;
    protected $redirect = 'admin';
    protected $validation_rules = array(
    );
    
    protected $translated_vr =  array(
        array('seo[title]', 'SEO title', 'xss_clean'),
        array('seo[description]', 'SEO description', 'xss_clean'),
        array('seo[keywords]', 'SEO description', 'xss_clean'),
    );

    public function save_seo($item_id, $module_id, $data) {
        $id = $this->select('id')->find_by(array('item_id' => $item_id, 'modules_id' => $module_id));
        if ($id){
            $this->update_translate($data, $id->id);
        }else{
            $this->insert_translate(array('item_id' => $item_id, 'modules_id' => $module_id), $data);
        }
    }
    
    public function get_frontend_seo($item_id, $module_id) {
        $this->db->select('title,description,keywords');
        $this->db->join($this->translate_table, 'id = table_id','left');
        $q = $this->db->get_where($this->table_name, array('item_id'=>$item_id,'modules_id'=>$module_id,'lang'=>  $this->site_lang));
        return $q->row_object();
    }
    
    

}
