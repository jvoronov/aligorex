<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Values_model extends BF_Model {
    
    protected $table_name = 'values';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    protected $redirect = 'admin';
    
    public function update_translate($id,array $title_data,$filed){
        foreach ($title_data as $k => $v){
            $this->db->where('table_id', $id);
            $this->db->where('lang', $k);
            $this->db->update($this->translate_table, array($filed=>$v)); 
        }
    }
    
    public function insert_translate($id,array $data,$filed){
        foreach ($data as $k => $v){
            $this->db->insert($this->translate_table, array($filed=>$v,'lang'=>$k,'table_id'=>$id)); 
        }
    }
    
    public function insert_empty(){
       $this->db->query("INSERT INTO `values` (`primary_key`) VALUES (NULL)");
       return  $this->db->insert_id();
        
    }
}