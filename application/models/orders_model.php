<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends BF_Model {
    
    protected $table_name = 'orders';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $date_format  = 'datetime';
    protected $set_created  = TRUE;
    protected $created_field    = 'created';
    protected $set_modified     = FALSE;
    protected $soft_deletes = TRUE;
    protected $deleted_field    = 'deleted';
    protected $redirect = 'admin';
    
    protected $validation_rules = array(
         array(
            'field' => 'name',
            'label' => 'Имя/Фамилия Покупателя',
            'rules' => 'required|max_length[140]|xss_clean'
        ),
        array(
            'field' => 'phone',
            'label' => 'Телефон',
            'rules' => 'required|xss_clean'
        ),
        array(
            'field' => 'email',
            'label' => 'Электронная почта',
            'rules' => 'required|valid_email|xss_clean'
        ),
        array(
            'field' => 'payment_id',
            'label' => 'Вид оплаты',
            'rules' => 'required|xss_clean'
        ),
    );
    
    public function prep_data($post_data) {
        $data = parent::prep_data($post_data);
        if (!empty($post_data['name'])) {
            $data['name'] = $post_data['name'];
        }
        if (!empty($post_data['phone'])) {
            $data['phone'] = $post_data['phone'];
        }
        if (!empty($post_data['email'])) {
            $data['email'] = $post_data['email'];
        }
        $data['sum'] = $this->cart->total(); 
        $data['status'] = 0;
        $data['new'] = 1;
        return $data;
    }
    
    public function insert_order($data){
        
        $this->orders->insert($data);
    }
}