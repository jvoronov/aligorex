<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_model extends BF_Model {
    
    protected $table_name = 'settings';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    protected $redirect = 'admin';
    
    protected $validation_rules = array(
    );
    
    public function get_all_values($settings_name){
        return $this->find_all_by_t(array('name'=>$settings_name));
    }
    
    public function update_settings($settings_name,array $settings){
        $result = $this->find_by('name',$settings_name);
        if($result){
            foreach ($settings as $key => $value) {
                $this->db->where('table_id', $result->id);
                $this->db->where('lang', $key);
                $this->db->update($this->translate_table, array('value'=>$value)); 
            }
            return true;
        }
        return false;
    }
}