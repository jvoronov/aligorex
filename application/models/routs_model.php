<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Routs_model extends BF_Model {
    
    protected $table_name = 'routs';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
}