<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Templates_model extends BF_Model {
    
    protected $table_name = 'templates';
    protected $date_format  = 'datetime';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $soft_deletes = FALSE;
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    
    protected $redirect = 'admin';
    
    protected $validation_rules = array(
        array(
            'field' => 'title',
            'label' => 'Название шаблона',
            'rules' => 'trim|required|min_length[3]|max_length[200]|xss_clean'
        ),
        array(
            'field' => 'path',
            'label' => 'Путь до шаблона',
            'rules' => 'trim|required|min_length[3]|max_length[200]|xss_clean'
        ),
    );
    
    public function prep_data($post_data) {
        $data = parent::prep_data($post_data);

        if (!empty($post_data['title'])) {
            $data['title'] = $post_data['title'];
        }
        if (!empty($post_data['url'])) {
            $data['url'] = $post_data['url'];
        }
        if (!empty($post_data['path'])) {
            $data['path'] = $post_data['path'];
        }
        if (!empty($post_data['status'])) {
            $data['status'] = $post_data['status'];
        }
        return $data;
    }
    
   
}
