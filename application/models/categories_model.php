<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends BF_Model {
    
    protected $table_name = 'categories';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    
    protected $soft_deletes = TRUE;
    protected $deleted_field = 'deleted';
    
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    
    protected $validation_rules = array(
    );
    protected $translated_vr = array(
        array('tf[title]','Название категории на языке','max_length[200]|xss_clean'),
    );
    
     public function prep_data($post_data) {
        $data = parent::prep_data($post_data);

        if (!empty($post_data['url'])) {
            $data['url'] = $post_data['url'];
        }
        if (!empty($post_data['status'])) {
            $data['status'] = $post_data['status'];
        }
        if (!empty($post_data['module_id'])) {
            $data['module_id'] = $post_data['module_id'];
        }
        if (!empty($post_data['img'])) {
            $data['img'] = $post_data['img'];
        }
        return $data;
    }
    
     public function get_nested($module_id,$for = 'backend') {
         $array = array();
        if($for == 'backend')
            $pages = $this->db->where('deleted', 0)
                              ->where("module_id",  $module_id)
                              ->where("{$this->translate_table}.lang",  $this->site_lang)
                              ->join($this->translate_table, "{$this->translate_table}.table_id = {$this->table_name}.id", 'left')
                              ->order_by("order", "asc")
                              ->get($this->table_name)
                              ->result_array();
        else
            $pages = $this->db->select('routs.rout,navigation_translate.title,navigation.p_id,navigation.id')
                              ->where('deleted', 0)
                              ->where('status', 1)
                              ->where("{$this->translate_table}.lang",  $this->site_lang)
                              ->join($this->translate_table, "{$this->translate_table}.table_id = {$this->table_name}.id", 'left')
                              ->join('routs', 'routs.id = navigation.rout_id','left')
                              ->order_by("order", "asc")
                              ->get($this->table_name)
                              ->result_array();
        foreach ($pages as $page) {
            if (!$page['p_id']) {
                $array[$page['id']] = $page;
            } else {
                $array[$page['p_id']]['children'][] = $page;
            }
        }
       //return $array = null ;
       return $array;
    }

    public function save_order($pages) {
        if (count($pages)) {
            foreach ($pages as $order => $page) {
                if ($page['item_id'] != '') {
                    $data = array('p_id' => (int) $page['parent_id'], 'order' => $order);
                    $this->db->set($data)->where('id', $page['item_id'])->update('categories');
                }
            }
        }
    }
    
    public function count_products_by_category($category_id) {
        //$this->db->select('category_id');
        $this->db->from('product_category');
        $this->db->where('product_category.category_id', $category_id);
        $this->db->where('products.deleted', 0);
        $this->db->where('products.status', 1);
        $this->db->join('products', 'products.id = product_category.product_id');
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function get_records_in_category_2($cat_id){
        $q = $this->db->query("SELECT pages_categories.categories_id, pages_translate.title,routs.rout
                                FROM pages
                                JOIN pages_translate ON ( pages_translate.table_id = pages.id ) 
                                JOIN pages_categories ON ( pages_categories.pages_id = pages.id ) 
                                JOIN routs ON ( pages.rout_id = routs.id )
                                where pages.status = 1
                                and pages.deleted = 0
                                and pages_translate.lang = '$this->site_lang'
                                and pages_categories.categories_id in($cat_id)");
        
        return $q->result_object();
    }
    
    
     
    
    
    public function get_records_in_category($cat_id){
        //$join_record_category = $module_data->name.'_'.$this->table_name;
        //$join_record_table = $module_data->name;
        //$this->db->select('categories.id,routs.rout,categories.title,categories.p_id,categories.url');
        //$this->db->from($this->table_name);
        //$this->db->where('categories.module_id', $module_data->id);
        //$this->db->join($join_record_category, "$join_record_table.id = $join_record_category.$module_data->name_id");
        
        $q = $this->db->query("SELECT pages_translate.title,routs.rout
                                FROM pages
                                JOIN pages_translate ON ( pages_translate.table_id = pages.id ) 
                                JOIN pages_categories ON ( pages_categories.pages_id = pages.id ) 
                                JOIN categories ON ( categories.id = pages_categories.categories_id )
                                JOIN routs ON ( pages.rout_id = routs.id )
                                where pages.status = 1
                                and pages.deleted = 0
                                and pages_translate.lang = '$this->site_lang'
                                and categories.id = '$cat_id'");
       return $q->result_object();
    }
    
   
}