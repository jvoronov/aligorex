<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Navigation_model extends BF_Model {
    
    protected $table_name = 'navigation';
    protected $date_format  = 'datetime';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    
    protected $soft_deletes = TRUE;
    protected $deleted_field = 'deleted';
    
    protected $set_created  = TRUE;
    protected $created_field    = 'created';

    protected $set_modified     = TRUE;
    protected $modified_field   = 'modified';

    protected $validation_rules = array(
        array(
            'field' => 'category_id',
            'label' => 'Связать страницу',
            'rules' => 'required'
        ),
    );
    public function __construct() {
        
        parent::__construct();
    }
    
    public function update_translate($id,array $title_data){
        foreach ($title_data as $k => $v){
            $this->db->where('table_id', $id);
            $this->db->where('lang', $k);
            $this->db->update($this->translate_table, array('title'=>$v)); 
        }
    }
    
    public function insert_translate($id,array $title_data){
        foreach ($title_data as $k => $v){
            $this->db->insert($this->translate_table, array('title'=>$v,'lang'=>$k,'table_id'=>$id)); 
        }
    }

    public function prep_data($post_data) {
            $data = parent::prep_data($post_data);

            if (!empty($post_data['category_id'])) {
                $data['rout_id'] = $post_data['category_id'];
            }
            if (!empty($post_data['status'])) {
                $data['status'] = $post_data['status'];
            }
            if (!empty($post_data['menus_id'])) {
                $data['menus_id'] = $post_data['menus_id'];
            }
            return $data;
    }
    
    public function get_nested($for = 'backend',$nav_id = false) {
        if($for == 'backend')
            $pages = $this->db->where('deleted', 0)
                              ->where("{$this->translate_table}.lang",  $this->site_lang)
                              ->where('menus_id',$nav_id)
                              ->join($this->translate_table, "{$this->translate_table}.table_id = {$this->table_name}.id", 'left')
                              ->order_by("order", "asc")
                              ->get('navigation')
                              ->result_array();
        else
            $pages = $this->db->select('routs.rout,navigation_translate.title,navigation.p_id,navigation.id')
                              ->where('deleted', 0)
                              ->where('status', 1)
                              ->where("{$this->translate_table}.lang",  $this->site_lang)
                              ->join($this->translate_table, "{$this->translate_table}.table_id = {$this->table_name}.id", 'left')
                              ->join('routs', 'routs.id = navigation.rout_id','left')
                              ->order_by("order", "asc")
                              ->get('navigation')
                              ->result_array();

        $array = array();
        foreach ($pages as $page) {
            if (!$page['p_id']) {
                $array[$page['id']] = $page;
            } else {
                $array[$page['p_id']]['children'][] = $page;
            }
        }
        return $array;
    }

    public function save_order($pages) {
        if (count($pages)) {
            foreach ($pages as $order => $page) {
                if ($page['item_id'] != '') {
                    $data = array('p_id' => (int) $page['parent_id'], 'order' => $order);
                    $this->db->set($data)->where('id', $page['item_id'])->update('navigation');
                }
            }
        }
    }
    
    public function new_menu($name) {
        $this->db->insert('menus',array('name'=>$name));
    }
    
    public function all_menus() {
        $this->table_name = 'menus';
        return $this->find_all();
    }
    
    public function menu_data($menus_id) {
        $q = $this->db->get_where('menus',array('id'=>$menus_id));
        return $q->row();
    }
    
    public function build_menu($menu_name) {
        $this->db->select('n.id id,n.p_id p_id,nt.title,r.rout');
        $this->db->join('navigation n', "m.id = n.menus_id", 'left');
        $this->db->join('navigation_translate nt', "n.id = nt.table_id", 'left');
        $this->db->join('routs r', "r.id = n.rout_id", 'left');
        $q = $this->db->get_where('menus m', array('m.name' => $menu_name, 'nt.lang' => $this->site_lang));
        $result = $q->result_array();
        $array = array();
        if ($result) {
            foreach ($result as $page) {
                if (!$page['p_id']) {
                    $array[$page['id']] = $page;
                } else {
                    $array[$page['p_id']]['children'][] = $page;
                }
            }
            return $array;
        } else {
            return $array;
        }
    }
}