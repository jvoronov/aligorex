<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_category_model extends BF_Model {
    
    protected $table_name = 'product_category';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $soft_deletes = FALSE;
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    protected $validation_rules = array();
    
    public function save_product_category($product_id, array $categories_array)
    {
        $this->db->where('product_id', $product_id);
        $this->db->delete($this->table_name);
        foreach ($categories_array as $value) {
            $data = array(
               'product_id' => $product_id,
               'category_id' => $value
            );
            $this->db->insert($this->table_name, $data);
        }
    }
    
    public function get_product_categories($product_id){
       $categories = $this->find_all_by('product_id', $product_id);
       if(is_array($categories)){
           $cat = array();
           foreach ($categories as $value) {
               $cat[] = $value->category_id;
           }
           return $cat;
       }else{
           return FALSE;
       }
    }
}