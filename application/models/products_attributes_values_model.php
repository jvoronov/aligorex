<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_attributes_values_model extends BF_Model {
    
    protected $table_name = 'products_attributes_values';
    protected $return_insert_id = FALSE;
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    
    public function insert_all($products_id,$values_array) {
        
        $this->db->where('products_id', $products_id);
        $this->db->delete($this->table_name);
        
        $this->db->insert_batch($this->table_name, $values_array);
    }
    
    public function get_selected_values($products_id){
       $this->db->select('values_id');
       $this->db->where('products_id', $products_id);
       $q =$this->db->get($this->table_name);
       $selected = $q->result_object();
       $array = array();
       foreach ($selected as $item){
            $array[] = $item->values_id;
       }
       return $array;
    }
}