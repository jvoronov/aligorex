<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Languages_model extends BF_Model {
    
    protected $table_name = 'languages';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    protected $validation_rules = array(
        array(
            'field' => 'title',
            'label' => 'Название языка',
            'rules' => 'required|trim|min_length[2]|max_length[20]|xss_clean'
        ),
        array(
            'field' => 'lang',
            'label' => 'Код языка',
            'rules' => 'required|trim|exact_length[2]|xss_clean'
        ),
    );
    
    public function get_active_languages(){
        $query = $this->db->where('status',1)->order_by('default','desc')->get($this->table_name);
        $result = $query->result();
        $array = array();
        foreach ($result as $key => $value) {
            $array[$value->lang] = $value->lang;
        }
        return $array;
    }
    
    public function set_default_lang($id){
        foreach ($this->find_all() as $item){
            $this->db->where('id', $item->id);
            $this->db->update($this->table_name, array('default'=>0));
        }
         $this->db->where('id', $id);
         $this->db->update($this->table_name, array('default'=>1));
    }
    
    public function prep_data($post_data) {
        $data = parent::prep_data($post_data);

        if (!empty($post_data['title'])) {
            $data['description'] = $post_data['title'];
        }
        if (!empty($post_data['status'])) {
            $data['status'] = $post_data['status'];
        }
        if (!empty($post_data['lang'])) {
            $data['lang'] = $post_data['lang'];
        }
        if (!empty($post_data['default'])) {
            $data['default'] = $post_data['default'];
        }
        return $data;
    }
    
    public function cache_routes()
    {
       $langs = implode("|", $this->get_active_languages());
       $data = "<?php".PHP_EOL;
       $data .='$route["^('.$langs.')/(.+)$"] = "$2";'.PHP_EOL;
       $data .='$route["^('.$langs.')$"] = $route["default_controller"];';
       $this->load->helper('file');
       write_file(APPPATH."cache/routes.php", $data,'w');
    }
    
    public function get_translate_tables(){
        $database = $this->db->database;
        $database_like = 'Tables_in_'.$database.' (%_translate)';
        $q = $this->db->query("SHOW TABLES FROM $database LIKE  '%_translate'");
        $result = $q->result_array();
        $array = array();
        foreach ($result as $item){
            $array[] = $item[$database_like];
        }
        return $array;
    }
    
    public function insert_new_lang_fields($lang_code){
        $tables = $this->get_translate_tables();
        foreach ($tables as $item){
        $this->db->query("INSERT INTO $item (table_id,lang) 
        SELECT table_id,'$lang_code' FROM $item WHERE lang = 'ru'");}
        
    }
}
