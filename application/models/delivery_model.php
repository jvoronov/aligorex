<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Delivery_model extends BF_Model {
    
    protected $table_name = 'delivery';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    protected $redirect = 'admin';
    
    protected $validation_rules = array(
         array(
            'field' => 'price',
            'label' => 'Цена доставки',
            'rules' => 'required|decimal|min_length[4]|max_length[14]|xss_clean'
        ),
    );
    
    public function prep_data($post_data) {
        $data = parent::prep_data($post_data);
        if (!empty($post_data['status'])) {
            $data['status'] = $post_data['status'];
        }
        if (!empty($post_data['price'])) {
            $data['price'] = $post_data['price'];
        }
         if (!empty($post_data['show_adress'])) {
            $data['show_adress'] = $post_data['show_adress'];
        }
        return $data;
    }
    
    public function insert_translate($id,array $data,$filed){
        foreach ($data as $k => $v){
            $this->db->insert($this->translate_table, array($filed=>$v,'lang'=>$k,'table_id'=>$id)); 
        }
    }
    
    public function update_translate($id,array $title_data,$filed){
        foreach ($title_data as $k => $v){
            $this->db->where('table_id', $id);
            $this->db->where('lang', $k);
            $this->db->update($this->translate_table, array($filed=>$v)); 
        }
    }
}