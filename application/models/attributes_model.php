<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attributes_model extends BF_Model {
    
    protected $table_name = 'attributes';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    protected $redirect = 'admin';
    
    protected $validation_rules = array(
    );
    
    public function prep_data($post_data) {
        $data = parent::prep_data($post_data);
        if (!empty($post_data['status'])) {
            $data['status'] = $post_data['status'];
        }
        if (!empty($post_data['img'])) {
            $data['img'] = $post_data['img'];
        }
        return $data;
    }
    
    public function insert_translate($id,array $data,$filed){
        foreach ($data as $k => $v){
            $this->db->insert($this->translate_table, array($filed=>$v,'lang'=>$k,'table_id'=>$id)); 
        }
    }
    
    public function update_translate($id,array $title_data,$filed){
        foreach ($title_data as $k => $v){
            $this->db->where('table_id', $id);
            $this->db->where('lang', $k);
            $this->db->update($this->translate_table, array($filed=>$v)); 
        }
    }
    
    public function get_attributes_values($attributes_id){
       $q = $this->db->query("select `values`.title,`values`.id
                                from attributes
                                left join attributes_values on attributes.id = attributes_values.attributes_id
                                left join `values` on `values`.id = attributes_values.values_id
                                where attributes.id = '$attributes_id'
                                order by `values`.title");
       return $q->result_object();
    }
    
    public function get_selected_values($attributes_id){
        $q = $this->db->query("select `values`.id
                                from attributes
                                left join attributes_values on attributes.id = attributes_values.attributes_id
                                left join `values` on `values`.id = attributes_values.values_id
                                where attributes.id = '$attributes_id'");
       $selected = $q->result_object();
       $array = array();
       foreach ($selected as $item){
            $array[] = $item->id;
       }
       return $array;
    }

    public function save_order($pages) {
        if (count($pages)) {
            foreach ($pages as $order => $page) {
                if ($page['item_id'] != '') {
                    $data = array('order' => $order);
                    $this->db->set($data)->where('id', $page['item_id'])->update('attributes');
                }
            }
        }
    }
}