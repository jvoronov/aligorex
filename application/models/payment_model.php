<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends BF_Model {
    
    protected $table_name = 'payment';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    protected $redirect = 'admin';
    
    protected $validation_rules = array(
    );
    
    public function prep_data($post_data) {
        $data = parent::prep_data($post_data);
        if (!empty($post_data['status'])) {
            $data['status'] = $post_data['status'];
        }
        return $data;
    }
    
    public function insert_translate($id,array $data,$filed){
        foreach ($data as $k => $v){
            $this->db->insert($this->translate_table, array($filed=>$v,'lang'=>$k,'table_id'=>$id)); 
        }
    }
    
    public function update_translate($id,array $title_data,$filed){
        foreach ($title_data as $k => $v){
            $this->db->where('table_id', $id);
            $this->db->where('lang', $k);
            $this->db->update($this->translate_table, array($filed=>$v)); 
        }
    }
}