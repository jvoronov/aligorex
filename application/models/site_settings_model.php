<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_settings_model extends BF_Model {
    
    protected $table_name = '';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    protected $translated_vr =  array(
        array('site_title', 'Название сайта на языке', 'required|min_length[3]|max_length[50]|xss_clean'),
        array('site_email', 'Почтовый адрес на языке', 'required|min_length[3]|valid_email|xss_clean'),
        array('site_phone', 'Телефон на языке', 'required|min_length[3]|max_length[50]|is_natural_no_zero|xss_clean'),
        array('site_adress', 'Адрес на языке', 'required|min_length[3]|max_length[150]|xss_clean'),
        array('site_description', 'Описание сайта на языке', 'required|min_length[3]|max_length[500]|xss_clean'),
        array('site_keywords', 'Ключевые слова сайта на языке', 'required|min_length[3]|max_length[500]|xss_clean'),
    );
    protected $validation_rules = array();
    
    public function __construct() {
        parent::__construct();
        $this->set_vr();
    }
}