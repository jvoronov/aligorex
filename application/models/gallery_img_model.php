<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery_img_model extends BF_Model {

    protected $table_name = 'gallery_img';
    protected $return_insert_id = TRUE;
    protected $return_type = 'object';
    protected $soft_deletes = FALSE;
    protected $set_created = FALSE;
    protected $set_modified = FALSE;
    protected $validation_rules = array(
        array('field' => 'img', 'label' => 'Ссылка', 'rules' => 'required|callback__check_youtube|xss_clean'),
    );
    
    public function get_images() {
        return $this->select('img')->order_by('id','desc')->limit(20)->find_all_by(array('is_video'=>0,'status'=>1));
    }
}
