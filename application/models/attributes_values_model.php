<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attributes_values_model extends BF_Model {
    
    protected $table_name = 'attributes_values';
    protected $return_insert_id = TRUE;
    protected $key  = 'values_id';
    protected $return_type      = 'object';
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    
    public function insert_all($attribute_id,$values_array) {
        $data = array();
        $i = 0;
        foreach ($values_array as $item){
            $data[$i]['attributes_id'] = $attribute_id;
            $data[$i]['values_id'] = $item;
            $i++;
        }
        $this->db->insert_batch($this->table_name, $data);
    }
    
    public function save_attributes_values($attributes_id, array $values,$filed = 'title')
    {
        $this->db->insert($this->table_name, array('attributes_id'=>$attributes_id));
        $id = $this->db->insert_id();
        foreach ($values as $k => $v){
            $this->db->insert($this->translate_table, array($filed=>$v,'lang'=>$k,'table_id'=>$id)); 
        }
    }
    
    public function insert_translate($id,array $data,$filed){
        foreach ($data as $k => $v){
            $this->db->insert($this->translate_table, array($filed=>$v,'lang'=>$k,'table_id'=>$id)); 
        }
    }
    
    public function find_all_values($attributes_id){
          $this->db->join($this->translate_table, "{$this->translate_table}.table_id = {$this->table_name}.values_id", 'left');
          return $this->find_all_by(array('attributes_id'=>$attributes_id,'lang'=>'ru'));
    }
    
    public function find_value($field, $value, $type=''){
          $this->db->join($this->translate_table, "{$this->translate_table}.table_id = {$this->table_name}.values_id", 'left');
          $this->db->where("{$this->translate_table}.lang",  $this->site_lang);
          return $this->find_all_by(array('values_id'=>$value));
    }
    
    public function find_all_by_t($array) {

        $this->db->join($this->translate_table, "{$this->translate_table}.table_id = {$this->table_name}.values_id", 'left');
        return $this->find_all_by($array);
    }

    public function update_translate($id, array $title_data, $filed) {
        foreach ($title_data as $k => $v) {
            $this->db->where('table_id', $id);
            $this->db->where('lang', $k);
            $this->db->update($this->translate_table, array($filed => $v));
        }
    }

}