<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_img_model extends BF_Model {
    
    protected $table_name = 'products_img';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    
    protected $soft_deletes = FALSE;
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;

     public function delete_img(array $images_id)
    {
        foreach ($images_id as $id)
        {
            $this->db->delete($this->table_name, array('id' => $id)); 
        
        }
    }
    
    public function update_img_title(array $title)
    {
        foreach ($title as $key => $value)
        {
            $this->db->where('id', $key);
            $this->db->update($this->table_name, array('title' => $value)); 
        }
        
    }
}