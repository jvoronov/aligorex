<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pages_model extends BF_Model {

    protected $table_name = 'pages';
    protected $date_format = 'datetime';
    protected $return_insert_id = TRUE;
    protected $return_type = 'object';
    protected $soft_deletes = TRUE;
    protected $deleted_field = 'deleted';
    protected $set_created = TRUE;
    protected $created_field = 'created';
    protected $set_modified = TRUE;
    protected $modified_field = 'modified';
    protected $validation_rules = array(
    );
    protected $translated_vr = array(
        array('tf[title]', 'Название страницы на языке', 'required|min_length[3]|max_length[200]|xss_clean'),
        array('tf[content]', 'Содержимое страницы на языке', 'required|min_length[3]'),
    );

    public function prep_data($post_data) {
        $data = parent::prep_data($post_data);

        if (!empty($post_data['url'])) {
            $data['url'] = $post_data['url'];
        }
        if (!empty($post_data['status'])) {
            $data['status'] = $post_data['status'];
        }
        if (!empty($post_data['module_id'])) {
            $data['module_id'] = $post_data['module_id'];
        }
        return $data;
    }

    public function page_in_category($id) {
        $this->db->join('pages_categories pc', "pc.pages_id = {$this->table_name}.id", 'left');
        $this->db->where(array("pc.pages_id"=>$id,'pages.status'=>'1'));
        return $this->find($id);
    }
    
    public function pages_in_category(){
        $this->db->join('pages_categories pc', "pc.pages_id = {$this->table_name}.id", 'left');
        $this->db->join('pages_translate pt', "pt.table_id = {$this->table_name}.id and pt.lang='$this->site_lang'", 'left');
        $this->db->order_by('ord','ASC');
        $q = $this->db->get_where($this->table_name,array('pages.status'=>'1', 'pc.categories_id'=>$this->input->post('category_id')));
        return $q->result_object();
    }

    public function get_parent_category_by_page_url($page_url) {
        //$this->db->select();
        $this->db->join('pages_categories', "pages_categories.pages_id = {$this->table_name}.id", 'left');
        $this->db->join('categories', "categories.id = pages_categories.categories_id", 'left');
        //$this->db->where("pages.url",  $page_url);
        return $this->find_by("pages.url", $page_url);
    }

    function get_page() {
        return $this->pages->find_by_t('url', $this->uri->rsegment(3));
    }

    function get_pages() {
        $this->db->select('c.id, p.url,pt.title,pt.content,pt.short_content,r.rout');
        $this->db->join('pages_categories pc', 'pc.categories_id =  c.id');
        $this->db->join($this->table_name . ' p', 'p.id = pc.pages_id');
        $this->db->join($this->translate_table . ' pt', 'pt.table_id = p.id');
        $this->db->join('routs r', 'p.rout_id = r.id');
        $q = $this->db->get_where('categories c', array('c.url' => $this->uri->rsegment(3), 'pt.lang' => $this->site_lang));
        return $q->result_object();
    }
    
    function save_order(){
        foreach($this->input->post('list') as $k=>$v){
            $this->db->where('id', $v);
            $this->db->update($this->table_name, array('ord'=>$k)); 
        }
    }

}
