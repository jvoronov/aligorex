<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery_model extends BF_Model {

    protected $table_name = 'gallery';
    protected $return_insert_id = TRUE;
    protected $return_type = 'object';
    protected $soft_deletes = FALSE;
    protected $set_created = FALSE;
    protected $set_modified = FALSE;
    protected $validation_rules = array(
        //array('field' => 'url','label' => 'Адрес галереи','rules' => 'required|min_length[2]|max_length[50]|callback__check_url|xss_clean'),
        array('field' => 'status', 'label' => 'отображение', 'rules' => 'required|xss_clean'),
    );
    protected $translated_vr = array(
        array('tf[title]', 'Название галереи на языке', 'required|min_length[3]|max_length[200]|xss_clean'),
    );

    public function prep_data($post_data) {
        $data = parent::prep_data($post_data);

        if (!empty($post_data['url'])) {
            $data['url'] = $post_data['url'];
        }
        if (!empty($post_data['status'])) {
            $data['status'] = $post_data['status'];
        }
        if (!empty($post_data['gallery_img'])) {
            $data['gallery_img'] = $post_data['gallery_img'];
        }
        return $data;
    }
    
     public function save_order($pages) {
        if (count($pages)) {
            foreach ($pages as $order => $page) {
                if ($page['item_id'] != '') {
                    $data = array('order' => $order);
                    $this->db->set($data)->where('id', $page['item_id'])->update('gallery');
                }
            }
        }
    }
    
    public function get_nested() {
        return $this->db->where("{$this->translate_table}.lang", $this->site_lang)
                        ->join($this->translate_table, "{$this->translate_table}.table_id = {$this->table_name}.id", 'left')
                        ->order_by("order", "asc")
                        ->get($this->table_name)
                        ->result_array();
    }
    
    public function save_image($gallery_id,$image_name) {
        $data = array(
            'gallery_id'=>$gallery_id,
            'img'=>$image_name,
            'status'=>1,
            'is_video'=>0,
        );
        $this->db->insert('gallery_img', $data); 
         return $this->db->insert_id();
    }
    
    public function get_images($gallery_id) {
        $this->db->select();
        $q = $this->db->order_by('order')->get_where('gallery_img', array('gallery_id' => $gallery_id)); 
        return $q->result_object();
    }
    
    public function delete_image($id) {
        $file = $this->db->select()->from('gallery_img')->where('id', $id)->get()->row();
        if (!$this->db->where('id', $id)->delete('gallery_img')){
            return FALSE;
        }
        if(!$file->is_video){
            $filestring = APPPATH.'../uploads/gallery/'.$file->img;
            $file_arr = explode('.', $file->img);
            $filestring2 = APPPATH.'../uploads/gallery/'.$file_arr[0].'_thumb.'.$file_arr[1];
            unlink ($filestring); 
            unlink ($filestring2); 
        }
        return TRUE;
    }
    
    public function update_image_order($data) {
        $i = 1;
        foreach ($data as $key => $value) {
            $list2[$i]['id'] = $key;
            $list2[$i]['order'] = $i;
            $i++;
        }
        $this->db->update_batch('gallery_img', $list2, 'id'); 
    }
    
    public function update_image_status($id,$status) {
        $this->db->update('gallery_img',  array('status' => $status), array('id' => $id));
    }
    
    public function save_link($data) {
        $this->db->insert('gallery_img',$data);
    }
    
    public function make_front_galleries() {
         $this->db->select('id,url,gallery_img,title');
         $this->db->join('gallery_translate gt', 'g.id = gt.table_id', 'left');
         $this->db->order_by('order');
         $q = $this->db->get_where('gallery g', array('g.status' => 1, 'gt.lang' => $this->site_lang));
         return $q->result_object();
    }
    
    public function get_gallery($url) {
        $this->db->select('gi.img,gi.is_video');
        $this->db->join('gallery_img gi', 'gi.gallery_id =  g.id','left');
        $this->db->order_by('gi.order');
        $q = $this->db->get_where('gallery g', array('g.url' => $url, 'gi.status' => 1));
        return $q->result_object();
    }
}
