<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages_categories_model extends BF_Model {
    
    protected $table_name = 'pages_categories';
    protected $return_insert_id = TRUE;
    protected $return_type      = 'object';
    protected $soft_deletes = FALSE;
    protected $set_created  = FALSE;
    protected $set_modified     = FALSE;
    protected $validation_rules = array();
    
    public function save_to_category($product_id,$categories_array)
    {
        $this->db->where('pages_id', $product_id);
        $this->db->delete($this->table_name);
        if(!is_array($categories_array)) return;
        foreach ($categories_array as $value) {
            $data = array(
               'pages_id' => $product_id,
               'categories_id' => $value
            );
            $this->db->insert($this->table_name, $data);
        }
    }
    
    public function get_categories($pages_id){
       $categories = $this->find_all_by('pages_id', $pages_id);
       if(is_array($categories)){
           $cat = array();
           foreach ($categories as $value) {
               $cat[] = $value->categories_id;
           }
           return $cat;
       }else{
           return FALSE;
       }
    }
}