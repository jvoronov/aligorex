<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends BF_Model {

    protected $table_name = 'products';
    protected $date_format = 'datetime';
    protected $return_insert_id = TRUE;
    protected $return_type = 'object';
    protected $soft_deletes = TRUE;
    protected $deleted_field = 'deleted';
    protected $set_created = TRUE;
    protected $created_field = 'created';
    protected $set_modified = TRUE;
    protected $modified_field = 'modified';
    protected $redirect = 'admin';
    protected $validation_rules = array(
        array(
            'field' => 'price',
            'label' => 'Цена недвижимости',
            'rules' => 'min_length[1]|max_length[11]|numeric'
        ),
        array(
            'field' => 'status',
            'label' => 'Отображать/Не отображать',
            'rules' => 'required'
        ),
    );
    protected $translated_vr = array(
        array('tf[title]', 'Название товара на языке', 'required|min_length[3]|max_length[200]|xss_clean'),
        array('tf[content]', 'Описание товара на языке', 'required|min_length[3]'),
    );

    public function prep_data($post_data) {
        $data = parent::prep_data($post_data);
        if (!empty($post_data['url'])) {
            $data['url'] = $post_data['url'];
        }
        if (!empty($post_data['status'])) {
            $data['status'] = $post_data['status'];
        }
        if (!empty($post_data['price'])) {
            $data['price'] = $post_data['price'];
        }
        return $data;
    }

    public function get_products_by_category($category) {
        $q = $this->db->query("select products.title, products.url, products_img.thumbnail, products_img.img
                                from products
                                join product_category on (products.id = product_category.product_id)
                                join products_img on (products.id = products_img.product_id)
                                join categories on (categories.id = product_category.category_id)
                                where products.status = 1
                                and products.deleted = 0
                                and  categories.url = '$category'
                                group by products.title");
        return $q->result_object();
    }

    public function get_product_images($product_url) {
        $q = $this->db->query("select products_img.thumbnail, products_img.img,products_img.title
                                from products
                                join products_img on (products.id = products_img.product_id)
                                where products.status = 1
                                and products.deleted = 0
                                and products.url = '$product_url'");
        return $q->result_object();
    }

    public function get_products_by_filter($filters) {
        unset($filters['add']);
        $query = 'SELECT DISTINCT `p`.* FROM `products` AS `p` ';
        foreach ($filters as $key => $value) {
            $ppv = 'ppv' . $key;
            $query .= "INNER JOIN `products_attributes_values` AS `$ppv` ON (p.id = $ppv.products_id)";
            $query .= "WHERE(";
            if (count($value) > 1) {
                $i = 0;
                foreach ($value as $k => $v) {
                    $query .= "(`$ppv`.`attributes_id` = '$key' AND `$ppv`.`values_id` = '$v')";
                    $query .= "OR";
                }
            } else {
                foreach ($value as $k => $v) {
                    $query .= "(`$ppv`.`attributes_id` = '$key' AND `$ppv`.`values_id` = '$v')";
                }
            }
            $query .= ")";
        }

        print_r($query);
        exit;
    }

    public function get_products_by_filter2($filters, $category) {
        unset($filters['add']);

        $sql = 'SELECT DISTINCT `p`.title,p.url,products_img.thumbnail, products_img.img
				FROM `products` AS `p` 
                                join product_category on (p.id = product_category.product_id)
                                join products_img on (p.id = products_img.product_id)
                                join categories as cat on  (cat.id = product_category.category_id)';
        $i = 1;
        foreach ($filters as $prop => $value) {
            $sql_properties[] = "INNER JOIN `products_attributes_values` AS `ppv{$i}` ON (p.id = ppv{$i}.products_id)";
            //$values = explode(',', $value);
            foreach ($value as $val) {
                $sql_vals[$i][] = "(`ppv{$i}`.`attributes_id` = '{$prop}' AND `ppv{$i}`.`values_id` = '{$val}')";
            }
            $sql_values[] = '(' . implode(' OR ', $sql_vals[$i]) . ')';
            $i++;
        }
        $sql .= implode("\n", $sql_properties);
        $sql .= ' WHERE ';
        $sql .= implode(' AND ', $sql_values);
        $sql .= ' AND ';
        $sql .= ' p.status = 1 ';
        $sql .= ' and p.deleted = 0 ';
        $sql .= " and cat.url = '$category' ";
        $sql .= ' group by p.title ';
        //print_r($sql);exit;
        $q = $this->db->query($sql);
        return $q->result_object();
    }

    public function get_product_filters($product_url) {
        $q = $this->db->query("select values.title vt,attributes.title at
                                from products
                                join products_attributes_values on (products.id = products_attributes_values.products_id)
                                join attributes on (attributes.id = products_attributes_values.attributes_id)
                                join `values` on (values.id = products_attributes_values.values_id)
                                where products.status = 1
                                and products.deleted = 0
                                and products.url = '$product_url'");
        return $q->result_object();
    }

    public function get_products_for_index_page($limit = 6) {
        $q = $this->db->query("select products.title, products.url, products_img.thumbnail, products_img.img
                                from products
                                join product_category on (products.id = product_category.product_id)
                                join products_img on (products.id = products_img.product_id)
                                join categories on (categories.id = product_category.category_id)
                                where products.status = 1
                                and products.deleted = 0
                                group by products.title limit $limit");
        return $q->result_object();
    }

}
