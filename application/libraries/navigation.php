<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Navigation {
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->model('navigation_model','nav',TRUE);
    }
    
    public function get_menu(){
         return $this->CI->nav->get_nested('frontend');
    }
}