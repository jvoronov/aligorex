<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emailer {
    
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->model('emails_model','emails_model',TRUE);
    }
    
    public function get_template_settings($template) {
        return $this->CI->emails_model->get_template_settings($template);
    }
    
    public function make_message($email_key, array $replace_array) {
        $template_data = $this->get_template_settings($email_key);
        if($template_data){
            $search = array_keys($replace_array);
            $replace = array_values($replace_array);
            $message = array();
            $message['message'] = str_replace($search, $replace, $template_data->content);
            $message['title'] = $template_data->title;
            return $message;
        }else{
            die('wrong email key');
        }
    }

}