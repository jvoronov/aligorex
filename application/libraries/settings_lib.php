<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_lib{
    
    protected $ci;
    
    public function __construct(){
        $this->ci = &get_instance();
        $this->ci->load->model('settings_model','settings');
    }
    
    public function get_settings($name){
        $result = $this->get_all_values($name);
        //if($result){
            $array = array();
            foreach ($result as $k => $v) {
                $array[$k] = json_decode($v->value);
            }
            return $array;
       // }
    }
    
    public function get_all_values($name){
        $result = $this->ci->settings->get_all_values($name);
        if($result)
            return return_assoc_array($result,'lang');
         die('Wrong settings name '.$name);
    }
    
    public function get_settings_value($name,$value){
        $result = $this->get_settings($name);
        if(in_array($this->ci->site_lang, $this->ci->langs_db->active_languages))
            return $result[$this->ci->site_lang]->$value;
    }
    
    public function update_settings($settings_name,array $settings){
        $result = $this->ci->settings->update_settings($settings_name,$settings);
        if(!$result)
            die('Wrong settings name '.$settings_name);
    }
    
    public function prepare_data(array $post){
        unset($post['submit']);
        $array = array();
        foreach ($post as $key => $value) {
            foreach ($value as $k =>$v){
                $array[$k][$key] = $v;
            }
        }
        foreach ($array as $key => $value) {
            $array[$key] = json_encode($value);
        }
        return $array;
    }
    
}
