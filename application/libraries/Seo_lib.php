<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Seo_lib {

    protected $ci;
    protected $default = array();
    protected $lang;

    public function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->model('seo_model', 'seo');
        $this->ci->load->library('settings_lib');
        $this->default = $this->ci->settings_lib->get_settings('site_main');
        $this->lang = $this->ci->site_lang;
    }

    public function get_records($item_id, $module_id) {
        return $this->ci->seo->select('keywords,description,title seo_title,lang seo_lang')->find_all_by_t(array('item_id' => $item_id, 'modules_id' => $module_id));
    }

    public function save_seo($item_id, $module_id, $data) {
        $this->ci->seo->save_seo($item_id, $module_id, $data);
    }
    //return validation rules
    public function get_validation_rules(){
        return $this->ci->seo->get_validation_rules('insert');
    }
    
    public function get_seo_data($item_id, $module_id) {
        return $this->ci->seo->get_frontend_seo($item_id, $module_id);
    }
    
    public function make_html($value,$switch = 'title') {
        switch ($switch) {
            case 'title': $return = "<title>".strip_tags($value)."</title>". PHP_EOL; break;
            case 'description': $return = "<meta name='description' content='".strip_tags($value)."'>". PHP_EOL; break;
            case 'keywords': $return = "<meta name='keywords' content='".strip_tags($value)."'>". PHP_EOL; break;
        }
        return $return;
    }
    
    public function make_seo( array $new_val = null) {
        if ($new_val != null) {
            $data = $this->get_seo_data($new_val[0], $new_val[1]);
            if ($data) {
                if ($data->title) {
                    $seo = $this->make_html($data->title, 'title');
                } else {
                    $seo = $this->make_html($this->default[$this->lang]->site_title, 'title');
                }
                if ($data->description) {
                    $seo .= $this->make_html($data->description, 'description');
                } else {
                    $seo .= $this->make_html($this->default[$this->lang]->site_description, 'description');
                }
                if ($data->keywords) {
                    $seo .= $this->make_html($data->keywords, 'keywords');
                } else {
                    $seo .= $this->make_html($this->default[$this->lang]->site_keywords, 'keywords');
                }
            } else {
                return $this->make_seo();
            }
        } else {
            $seo = $this->make_html($this->default[$this->lang]->site_title, 'title');
            $seo .= $this->make_html($this->default[$this->lang]->site_description, 'description');
            $seo .= $this->make_html($this->default[$this->lang]->site_keywords, 'keywords');
        }
        return $seo;
    }

    public function custom_seo($title, $description = false, $keywords = false) {
        $seo = $this->make_html($title,'title');
        if ($description) {
            $seo .=  $this->make_html($description,'description');
        } else {
            $seo .=  $this->make_html($this->default[$this->lang]->site_description,'description');
        }
        if ($keywords) {
             $seo .=  $this->make_html($keywords,'keywords');
        } else {
            $seo .=  $this->make_html($this->default[$this->lang]->site_keywords,'keywords');
        }
        return $seo;
    }

}
