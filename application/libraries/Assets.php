<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assets {
    
    public $css = array();
    public $js = array();
    
    public function __construct(){
    }       
    
    public function add(){
        $args = func_get_args();
        if(empty($args))
            die('args cannot be empty');
        $args = $args[0];
        
        $k = array_keys($args);
        foreach($k as $key){
            $this->{$key} = array_merge((array) $this->{$key}, (array)$args[$key]);
        }
    }
    
    public function css()
    {
        foreach($this->css as $css){
            echo "<link href='".base_url()."assets/$css' rel='stylesheet'>\n";
        }
    }
    
    public function js()
    {
        foreach($this->js as $js){
            echo "<script src='".base_url()."assets/$js' ></script>\n";
        }
    }
}