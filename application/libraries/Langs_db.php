<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Langs_db {
    
    public $active_languages = array();
    public $default_language;
    
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->model('languages_model','languages',TRUE);
        $this->active_languages = $this->get_active_languages();
        $this->default_language = $this->get_default_language();
    }
    
    public function get_active_languages(){
        //return assoc config with languages
        return $this->CI->lang->languages;
    }
    
    public function get_default_language(){
        $def_lang = array_keys($this->active_languages);
        return $def_lang[0];
    }
    
    public function languages_data(){
        //return all languages data from db
        return  $this->CI->languages->where('status',1)->order_by('default','desc')->find_all();
    }
}