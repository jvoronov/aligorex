<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['image_library'] = 'gd2';
$config['create_thumb'] = FALSE;
$config['maintain_ratio'] = TRUE;
$config['overwrite'] = TRUE;
$config['width'] = 449;
$config['height'] = 204;

