<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config = array(
    //routs for pages controller
    'pages_main' => 'pages/index',
    'pages' => 'pages/page/',
    //routs for deleted pages
    'error404' => '404',
    //routs for blog controller
    'blog_main' => 'blog/index',
    'blog' => 'blog/article/',
     //routs for categories
    'categories_main' => 'categories',
    'pages_categories' => 'pages/category/',
    'blog_categories' => 'blog/category/',
     //routs for templates
    'templates_main' => 'template',
    'templates' => 'pages/template/',
    //routs for gallery
    'gallery_main' => 'gallery/index',
    'gallery' => 'gallery/show/',
);
