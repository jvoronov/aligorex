<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['allowed_types'] = 'jpg|png|jpeg';
$config['max_size']	= '0';
$config['max_width']  = '0';
$config['max_height']  = '0';
$config['encrypt_name'] = TRUE;
