<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config = array(
    'language' => 'ru',
    'width' => '',
    'height' => '300px',
    'filebrowserImageUploadUrl' => base_url().'admin/file_upload/ckupload',
    'toolbar' => array(
        array( 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ),
        array( 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ),
        array( 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ),
        array( 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ),
        array( 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ),
        array( 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'),
        array( 'Link', 'Unlink', 'Anchor' ),
        array( 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ),
        array(  'Styles', 'Format', 'Font', 'FontSize' ),
        array(  'TextColor', 'BGColor' ),
        array(  'Maximize', 'ShowBlocks' ),
    )
); 