<?php defined('BASEPATH') OR exit('No direct script access allowed');
/* twitter bootstrap overrides */
$config['full_tag_open'] = '<div class="text-center"><ul class="pagination">';
$config['full_tag_close'] = '</ul></div><!--/pagination-->';

$config['first_link'] = '&laquo; На первую';
$config['first_tag_open'] = '<li class="page">';
$config['first_tag_close'] = '</li>';

$config['last_link'] = 'На последнюю  &raquo;';
$config['last_tag_open'] = '<li class="page">';
$config['last_tag_close'] = '</li>';

$config['next_link'] = '>';
$config['next_tag_open'] = '<li class="page">';
$config['next_tag_close'] = '</li>';

$config['prev_link'] = '<';
$config['prev_tag_open'] = '<li class="page">';
$config['prev_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li class="active"><a href="'.current_url().'">';
$config['cur_tag_close'] = '</a></li>';

$config['num_tag_open'] = '<li class="page">';
$config['num_tag_close'] = '</li>';

$config['per_page'] = 20;
$config['uri_segment'] = 4;