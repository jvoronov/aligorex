<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//site config
$config['site_title'] = 'Aligorex IT';
$config['site_phone'] = '+3717777777';
$config['site_email'] = 'aligorex@aligorex.com';
$config['site_default_lang'] = 'ru';